package com.javarush.task.task08.task0823;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/* 
Омовение Рамы
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        String result = "";

        for (int i = 0; i < s.length(); i++) {
            String str;

            if (i == 0) {
                if (s.charAt(i) != ' ') {
                    str = s.substring(i, i + 1).toUpperCase();
                } else {
                    str = String.valueOf(s.charAt(i));
                }
            } else {
                if (s.charAt(i - 1) == ' ' && s.charAt(i) != ' ') {
                    str = s.substring(i, i + 1).toUpperCase();
                } else {
                    str = String.valueOf(s.charAt(i));
                }
            }

            result += str;
        }

        System.out.println(result);

        //напишите тут ваш код
    }
}
