package com.javarush.task.task08.task0813;

import java.util.Set;
import java.util.HashSet;

/* 
20 слов на букву «Л»
*/

public class Solution {
    public static Set<String> createSet() {
        //напишите тут ваш код
        Set<String> s = new HashSet<>();

        s.add("Ла");
        s.add("Лб");
        s.add("Лв");
        s.add("Лг");
        s.add("Лд");
        s.add("Ле");
        s.add("Лё");
        s.add("Лж");
        s.add("Лз");
        s.add("Ли");
        s.add("Лй");
        s.add("Лк");
        s.add("Лл");
        s.add("Лм");
        s.add("Лн");
        s.add("Ло");
        s.add("Лп");
        s.add("Лр");
        s.add("Лс");
        s.add("Лт");
            
        return s;
    }

    public static void main(String[] args) {

    }
}
