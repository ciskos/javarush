package com.javarush.task.task08.task0828;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/* 
Номер месяца
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        //напишите тут ваш код
        HashMap<Integer, String> monthes = new HashMap<>();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String month = br.readLine();
        Calendar c = Calendar.getInstance();

        monthes.put(1, "January");
        monthes.put(2, "February");
        monthes.put(3, "March");
        monthes.put(4, "April");
        monthes.put(5, "May");
        monthes.put(6, "June");
        monthes.put(7, "July");
        monthes.put(8, "August");
        monthes.put(9, "September");
        monthes.put(10, "October");
        monthes.put(11, "November");
        monthes.put(12, "December");

        for (Map.Entry<Integer, String> entry : monthes.entrySet()) {
            if (entry.getValue().equals(month)) {
                System.out.println(month + " is the " + entry.getKey() + " month");
            }
        }
    }
}
