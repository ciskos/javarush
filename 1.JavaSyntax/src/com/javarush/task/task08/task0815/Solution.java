package com.javarush.task.task08.task0815;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

/* 
Перепись населения
*/

public class Solution {
    public static HashMap<String, String> createMap() {
        //напишите тут ваш код
        HashMap<String, String> hm = new HashMap<>();
        
        for (int i = 0; i < 10; i++) {
            hm.put("Surname" + i, "Name" + i);
        }
        
        return hm;
    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name) {
        //напишите тут ваш код
        int counter = 0;
        for (Entry pair : map.entrySet()) {
            if (pair.getValue().equals(name)) counter++;
        }
        return counter;
    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String lastName) {
        //напишите тут ваш код
        int counter = 0;
        for (Entry pair : map.entrySet()) {
            if (pair.getKey().equals(lastName)) counter++;
        }
        return counter;
    }

    public static void main(String[] args) {
        // System.out.println(getCountTheSameFirstName(createMap(), "Name1"));
        // System.out.println(getCountTheSameLastName(createMap(), "Surname1"));
    }
}
