package com.javarush.task.task08.task0816;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Calendar;

/* 
Добрая Зинаида и летние каникулы
*/

public class Solution {
    public static HashMap<String, Date> createMap() throws ParseException {
        DateFormat df = new SimpleDateFormat("MMMMM d yyyy", Locale.ENGLISH);
        HashMap<String, Date> map = new HashMap<String, Date>();
        map.put("Stallone", df.parse("JUNE 1 1980"));

        //напишите тут ваш код
        for (int i = 0; i < 9; i++) {
            map.put("Name" + i, df.parse("January 1 1990"));
        }

        return map;
    }

    public static void removeAllSummerPeople(HashMap<String, Date> map) {
        //напишите тут ваш код
        String month[] = {"Jun", "Jul", "Aug"};
        Iterator<Map.Entry<String, Date>> mapI = map.entrySet().iterator();
        while (mapI.hasNext()) {
            Map.Entry<String, Date> pair = mapI.next();
            Calendar c = Calendar.getInstance();
            c.setTime(pair.getValue());
            if (c.get(2) == 5 || c.get(2) == 6 || c.get(2) == 7) {
                mapI.remove();
            }
        }
    }

    public static void main(String[] args) {

    }
}
