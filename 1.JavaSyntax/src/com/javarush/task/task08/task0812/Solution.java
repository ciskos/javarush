package com.javarush.task.task08.task0812;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/* 
Cамая длинная последовательность
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        //напишите тут ваш код
        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> ali = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            ali.add(sc.nextInt());
        }

		int countCurrent = 1;
		int countPrevious = 0;
		int previous = ali.get(0);

		for (int i = 1; i <= ali.size(); i++) {
//			System.out.println("i=" + i + " ali=" + ali.get(i) + " boolean=" + (previous == ali.get(i)));
			if (i == ali.size()) {
				// System.out.println("TUT");
				if (countPrevious < countCurrent) {
					countPrevious = countCurrent;
				}

				// System.out.println("out");
				System.out.println(countPrevious);
				return;
			}

			if (previous == ali.get(i)) {
				countCurrent++;
				// System.out.println("countCurrent=" + countCurrent);
			} else {
				if (countPrevious <= countCurrent) {
					countPrevious = countCurrent;
					countCurrent = 1;
				}
			}

			previous = ali.get(i);
		}
    }
}