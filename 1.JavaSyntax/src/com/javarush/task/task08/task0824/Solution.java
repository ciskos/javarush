package com.javarush.task.task08.task0824;

/* 
Собираем семейство
*/

import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        ArrayList<Human> c = new ArrayList<>();
        Human child1 = new Human("Child1", true, 10, c);
        Human child2 = new Human("Child2", true, 10, c);
        Human child3 = new Human("Child3", true, 10, c);
        ArrayList<Human> childs = new ArrayList<>();
        childs.add(child1);
        childs.add(child2);
        childs.add(child3);
        Human father = new Human("Father", true, 30, childs);
        Human mother = new Human("Mother", false, 30, childs);
        ArrayList<Human> parentsFather = new ArrayList<>();
        ArrayList<Human> parentsMother = new ArrayList<>();
        parentsFather.add(father);
        parentsMother.add(mother);
        Human gf1 = new Human("GF1", true, 60, parentsFather);
        Human gm1 = new Human("GM1", false, 60, parentsMother);
        Human gf2 = new Human("GF2", true, 60, parentsFather);
        Human gm2 = new Human("GM2", false, 60, parentsMother);

        System.out.println(gf1.toString());
        System.out.println(gf2.toString());
        System.out.println(gm1.toString());
        System.out.println(gm2.toString());
        System.out.println(father.toString());
        System.out.println(mother.toString());
        System.out.println(child1.toString());
        System.out.println(child2.toString());
        System.out.println(child3.toString());
    }

    public static class Human {
        //напишите тут ваш код
        String name;
        boolean sex;
        int age;
        List<Human> children = new ArrayList<>();

        public Human(String name, boolean sex, int age, ArrayList<Human> children) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = children;
        }

        public String toString() {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0) {
                text += ", дети: " + this.children.get(0).name;

                for (int i = 1; i < childCount; i++) {
                    Human child = this.children.get(i);
                    text += ", " + child.name;
                }
            }
            return text;
        }
    }

}
