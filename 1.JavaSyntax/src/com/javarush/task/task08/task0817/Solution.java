package com.javarush.task.task08.task0817;

import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;

import java.util.*;

/* 
Нам повторы не нужны
*/

public class Solution {
    public static HashMap<String, String> createMap() {
        //напишите тут ваш код
        HashMap<String, String> hmss = new HashMap<>();

//        for (int i = 0; i < 10; i++) {
//            hmss.put("Surname" + i, "Name" + i);
//        }
        hmss.put("Surname", "Name");
        hmss.put("Surname0", "Name");
        hmss.put("Surname1", "Name1");
        hmss.put("Surname2", "Name2");
        hmss.put("Surname3", "Name3");
        hmss.put("Surname4", "Name4");
        hmss.put("Surname5", "Name5");
        hmss.put("Surname6", "Name6");
        hmss.put("Surname7", "Name7");
        hmss.put("Surname8", "Name8");

        return hmss;
    }

    public static void removeTheFirstNameDuplicates(Map<String, String> map) {
        //напишите тут ваш код
        ArrayList<String> alsV = new ArrayList<>();
        int counter = 0;

        Iterator<Map.Entry<String, String>> mapI = map.entrySet().iterator();
        while (mapI.hasNext()) {
            Iterator<Map.Entry<String, String>> mapI2 = map.entrySet().iterator();
            String mapIV = mapI.next().getValue();

            while (mapI2.hasNext()) {
                if (mapIV.equals(mapI2.next().getValue())) {
                    counter++;
                }
            }

            if (counter > 1) {
                alsV.add(mapIV);
            }
            counter = 0;
        }

        HashSet<String> hs = new HashSet<>();
        for (String s : alsV) {
//            System.out.println(s);
            hs.add(s);
        }

        for (String s : hs) {
//            System.out.println(s);
            removeItemFromMapByValue(map, s);
        }

//        for (Map.Entry<String, String> entry : map.entrySet()) {
//            System.out.println(entry.getKey() + " " + entry.getValue());
//        }
    }

    public static void removeItemFromMapByValue(Map<String, String> map, String value) {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair : copy.entrySet()) {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }

    public static void main(String[] args) {
//        HashMap<String, String> hmss =
//        removeTheFirstNameDuplicates(createMap());
//        System.out.println(createMap());

//        for (Map.Entry<String, String> entry : hmss.entrySet()) {
//            System.out.println(entry.getKey() + " " + entry.getValue());
//        }
    }
}
