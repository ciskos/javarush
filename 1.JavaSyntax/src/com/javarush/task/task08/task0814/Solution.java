package com.javarush.task.task08.task0814;

import java.util.HashSet;
import java.util.Set;
import java.util.Iterator;

/* 
Больше 10? Вы нам не подходите
*/

public class Solution {
    public static HashSet<Integer> createSet() {
        // напишите тут ваш код
        HashSet<Integer> hsi = new HashSet<>();
        
        for (int i = 0; i < 20; i++) {
            hsi.add(i);
        }

        return hsi;
    }

    public static HashSet<Integer> removeAllNumbersGreaterThan10(HashSet<Integer> set) {
        // напишите тут ваш код
        // Map copiedMap = Collections.synchronizedMap(Map);
        HashSet<Integer> hsi = new HashSet<>();

        hsi.addAll(set);
//        System.out.println(hsi);
        Iterator<Integer> hsiI = hsi.iterator();
        while (hsiI.hasNext()) {
        	int i = hsiI.next();
            if (i > 10) hsiI.remove();
        }

        return hsi;
    }

    public static void main(String[] args) {
//    	removeAllNumbersGreaterThan10(createSet());
    // 	System.out.println(removeAllNumbersGreaterThan10(createSet()));
    }
}
