package com.javarush.task.task08.task0818;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* 
Только для богачей
*/

public class Solution {
    public static HashMap<String, Integer> createMap() {
        //напишите тут ваш код
        HashMap<String, Integer> hmss = new HashMap<>();

        hmss.put("Surname", 100);
        hmss.put("Surname0", 200);
        hmss.put("Surname1", 300);
        hmss.put("Surname2", 400);
        hmss.put("Surname3", 500);
        hmss.put("Surname4", 600);
        hmss.put("Surname5", 700);
        hmss.put("Surname6", 800);
        hmss.put("Surname7", 900);
        hmss.put("Surname8", 1000);

        return hmss;

    }

    public static void removeItemFromMap(HashMap<String, Integer> map) {
        //напишите тут ваш код
        Iterator<Map.Entry<String, Integer>> mapI = map.entrySet().iterator();

        while (mapI.hasNext()) {
//            String s = mapI.next().getKey();
            Integer i = mapI.next().getValue();
            if (i < 500) {
                mapI.remove();
            }
        }

//        for (Map.Entry<String, Integer> entry : map.entrySet()) {
//            System.out.println(entry.getKey() + " " + entry.getValue());
//        }
    }

    public static void main(String[] args) {
//        removeItemFromMap(createMap());
    }
}