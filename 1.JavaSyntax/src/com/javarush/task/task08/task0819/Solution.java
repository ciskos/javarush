package com.javarush.task.task08.task0819;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* 
Set из котов
*/

public class Solution {
    public static void main(String[] args) {
        Set<Cat> cats = createCats();

        //напишите тут ваш код. step 3 - пункт 3
        Iterator<Cat> catsI = cats.iterator();

        if (catsI.hasNext()) {
            catsI.next();
            catsI.remove();
        }

        printCats(cats);
    }

    public static Set<Cat> createCats() {
        //напишите тут ваш код. step 2 - пункт 2
        Set<Cat> sC = new HashSet<>();

        Cat cat1 = new Cat();
        Cat cat2 = new Cat();
        Cat cat3 = new Cat();

        sC.add(cat1);
        sC.add(cat2);
        sC.add(cat3);

//        for (Cat c : sC) {
//            System.out.println(c);
//        }

        return sC;
    }

    public static void printCats(Set<Cat> cats) {
        // step 4 - пункт 4
        Iterator<Cat> catsI = cats.iterator();
        while (catsI.hasNext()) {
            System.out.println(catsI.next());
        }
    }

    // step 1 - пункт 1
    public static class Cat {

    }
}
