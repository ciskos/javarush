package com.javarush.task.task10.task1013;

/* 
Конструкторы класса Human
*/

public class Solution {
    public static void main(String[] args) {
    }

    public static class Human {
        // Напишите тут ваши переменные и конструкторы
        private String name;
        private String surname;
        private int age;
        private boolean sex;
        private boolean dog;
        private boolean cat;

        // 1
        public Human() {
            this.name = "Name";
            this.surname = "Surname";
            this.age = 0;
            this.sex = false;
            this.dog = false;
            this.cat = false;
        }

        // 2
        public Human(String name) {
            this.name = name;
            this.surname = "Surname";
            this.age = 0;
            this.sex = false;
            this.dog = false;
            this.cat = false;
        }

        // 3
        public Human(String name, String surname) {
            this.name = name;
            this.surname = surname;
            this.age = 0;
            this.sex = false;
            this.dog = false;
            this.cat = false;
        }

        // 4
        public Human(String name, String surname, int age) {
            this.name = name;
            this.surname = surname;
            this.age = age;
            this.sex = false;
            this.dog = false;
            this.cat = false;
        }

        // 5
        public Human(String name, String surname, int age, boolean sex) {
            this.name = name;
            this.surname = surname;
            this.age = age;
            this.sex = sex;
            this.dog = false;
            this.cat = false;
        }

        // 6
        public Human(String name, String surname, int age, boolean sex, boolean dog) {
            this.name = name;
            this.surname = surname;
            this.age = age;
            this.sex = sex;
            this.dog = dog;
            this.cat = false;
        }

        // 7
        public Human(String name, String surname, int age, boolean sex, boolean dog, boolean cat) {
            this.name = name;
            this.surname = surname;
            this.age = age;
            this.sex = sex;
            this.dog = dog;
            this.cat = cat;
        }

        // 8
        public Human(int age, boolean sex, boolean dog, boolean cat) {
            this.name = "Name";
            this.surname = "Surname";
            this.age = age;
            this.sex = sex;
            this.dog = dog;
            this.cat = cat;
        }

        // 9
        public Human(boolean sex, boolean dog, boolean cat) {
            this.name = "Name";
            this.surname = "Surname";
            this.age = 0;
            this.sex = sex;
            this.dog = dog;
            this.cat = cat;
        }

        // 10
        public Human(boolean dog, boolean cat) {
            this.name = "Name";
            this.surname = "Surname";
            this.age = 0;
            this.sex = false;
            this.dog = dog;
            this.cat = cat;
        }
    }
}
