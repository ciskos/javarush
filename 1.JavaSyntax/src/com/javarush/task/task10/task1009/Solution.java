package com.javarush.task.task10.task1009;

/* 
Правильный ответ: d = 5.5
*/

public class Solution {
    public static void main(String[] args) {
//        int a = 5;
//        int b = 4;
//        int c = 3;
//        int e = 2;
//        double d = a + b / c / e;
        int a = 5;
        int b = 4;
        int c = 3;
        int e = 2;
        double d = a + b / c / (double) e;

//        System.out.println("a " + a);
//        System.out.println("b " + b);
//        System.out.println("c " + c);
//        System.out.println("e " + e);
        System.out.println(d);
    }
}
