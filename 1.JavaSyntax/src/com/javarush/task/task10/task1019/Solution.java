package com.javarush.task.task10.task1019;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/* 
Функциональности маловато!
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        HashMap<String, Integer> hm = new HashMap<>();

        while (true) {
            String read = reader.readLine();
            if (read.isEmpty()) break;
            int id = Integer.parseInt(read);
            String name = reader.readLine();
            hm.put(name, id);
        }

        for (Map.Entry<String, Integer> entry : hm.entrySet()) {
            System.out.println(entry.getValue() + " " + entry.getKey());
        }
    }
}
