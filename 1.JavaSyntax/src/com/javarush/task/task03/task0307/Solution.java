package com.javarush.task.task03.task0307;

import com.javarush.task.task03.task0307.*;
/* 
Привет StarCraft!
*/

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        Zerg zerg1 = new Zerg();
        Zerg zerg2 = new Zerg();
        Zerg zerg3 = new Zerg();
        Zerg zerg4 = new Zerg();
        Zerg zerg5 = new Zerg();
        Protoss protos1 = new Protoss();
        Protoss protos2 = new Protoss();
        Protoss protos3 = new Protoss();
        Terran terran1 = new Terran();
        Terran terran2 = new Terran();
        Terran terran3 = new Terran();
        Terran terran4 = new Terran();
        
        zerg1.name = "zerg1Name";
        zerg2.name = "zerg2Name";
        zerg3.name = "zerg3Name";
        zerg4.name = "zerg4Name";
        zerg5.name = "zerg5Name";
        
        protos1.name = "protos1Name";
        protos2.name = "protos2Name";
        protos3.name = "protos3Name";
        
        terran1.name = "terran1Name";
        terran2.name = "terran2Name";
        terran3.name = "terran3Name";
        terran4.name = "terran4Name";
    }

    public static class Zerg {
        public String name;
    }

    public static class Protoss {
        public String name;
    }

    public static class Terran {
        public String name;
    }
}
