package com.javarush.task.task03.task0314;

/* 
Таблица умножения
*/

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        int row = 1;
        int col = 1;
        int count = 10;
        
        while (row <= count) {
            while (col <= count) {
                System.out.print(row * col + " ");
                col++;
            }
            row++;
            col = 1;
            System.out.println();
        }
    }
}
