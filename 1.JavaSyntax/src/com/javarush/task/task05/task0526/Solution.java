package com.javarush.task.task05.task0526;

/* 
Мужчина и женщина
*/

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        Man man1 = new Man("Name1", 31, "Address1");
        Man man2 = new Man("Name2", 32, "Address2");
        Woman woman1 = new Woman("Name3", 33, "Address3");
        Woman woman2 = new Woman("Name4", 34, "Address4");
        
        System.out.println(man1.toString());
        System.out.println(man2.toString());
        System.out.println(woman1.toString());
        System.out.println(woman2.toString());
    }

    //напишите тут ваш код
    public static class Man {
        String name;
        int age;
        String address;
        
        Man(String name, int age, String address) {
            this.name = name;
            this.age = age;
            this.address = address;
        }
        
        public String toString() {
            return (this.name + " " + this.age + " " + this.address);
        }
    }
    
    public static class Woman {
        String name;
        int age;
        String address;
        
        public Woman(String name, int age, String address) {
            this.name = name;
            this.age = age;
            this.address = address;
        }
        
        public String toString() {
            return (this.name + " " + this.age + " " + this.address);
        }
    }
}
