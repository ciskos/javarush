package com.javarush.task.task05.task0507;

/* 
Среднее арифметическое
*/

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        Scanner sc = new Scanner(System.in);
        double summ = 0;
        int count = 0;
        
        while (true) {
            double number = sc.nextDouble();
            if (number == -1) {
                System.out.println(summ / count);
                break;
            }
            summ += number;
            count++;
        }
        
    }
}

