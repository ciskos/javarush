package com.javarush.task.task05.task0510;

/* 
Кошкоинициация
*/

public class Cat {
    //напишите тут ваш код
    String name;
    int age;
    int weight;
    String address;
    String color;

    public static void main(String[] args) {

    }
    
    public void initialize(String name) {
        this.name = name;
        this.age = 5;
        this.weight = 3;
        this.color = "black";
    }
    
    public void initialize(String name, int weight, int age) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.color = "black";
    }
    
    public void initialize(String name, int age) {
        this.name = null;
        this.age = 1;
        this.weight = 1;
        this.color = null;
    }
    
    public void initialize(int weight, String color) {
        this.weight = weight;
        this.color = color;
        this.age = 1;
    }
    
    public void initialize(int weight, String color, String address) {
        this.weight = weight;
        this.color = color;
        this.address = address;
        this.age = 1;
    }
    
}
