package com.javarush.task.task01.task0138;

/* 
Любимое стихотворение
*/

public class Solution {
    public static void main(String[] args) {
        System.out.println("Мое любимое стихотворение:");
        //напишите тут ваш код
        System.out.println("О сколько нам открытий чудных");
        System.out.println("Готовит просвященья дух");
        System.out.println("И опыт - сын ошибок трудных");
        System.out.println("И генний - парадоксов друг");
        System.out.println("И случай - бог изобретатель.");
    }
}