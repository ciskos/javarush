package com.javarush.task.task07.task0711;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Удалить и вставить
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> als = new ArrayList<>();
        
        for (int i = 0; i < 5; i++) {
            als.add(br.readLine());
        }
        
        for (int i = 0; i < 13; i++) {
            String tmp = als.get(als.size() - 1);
            als.remove(als.size() - 1);
            als.add(0, tmp);
        }
        
        for (String s : als) {
            System.out.println(s);
        }
    }
}
