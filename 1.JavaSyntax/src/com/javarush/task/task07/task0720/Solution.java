package com.javarush.task.task07.task0720;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

/* 
Перестановочка подоспела
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> ali = new ArrayList<>();
        int n = Integer.parseInt(reader.readLine());
        int m = Integer.parseInt(reader.readLine());
        //напишите тут ваш код
        
        for (int i = 0; i < n; i++) {
            ali.add(reader.readLine());
        }
        
        for (int i = 0; i < m; i++) {
            ali.add(ali.get(i));
        }
        
        Iterator<String> aliI = ali.iterator();
        for (int i = 0; i < m; i++) {
            if (aliI.hasNext()) {
                aliI.next();
                // String s = aliI.next();
                // ali.add(s);
                aliI.remove();
                // System.out.println(s);
            }
        }
        
        for (String s : ali) {
            System.out.println(s);
        }
    }
}
