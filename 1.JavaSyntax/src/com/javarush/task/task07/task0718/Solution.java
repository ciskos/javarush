package com.javarush.task.task07.task0718;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Проверка на упорядоченность
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        //напишите тут ваш код
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> als = new ArrayList<>();
        
        for (int i = 0; i < 10; i++) {
            als.add(br.readLine());
        }
        
        int length = als.get(0).length();
        // System.out.println(length);
        for (int i = 1; i < als.size(); i++) {
            // System.out.println(als.get(i).length());
            // System.out.println(i + " " + als.get(i) + " " + (length <= als.get(i).length()) + " " + als.get(i).length());
            if (length >= als.get(i).length()) {
                System.out.println(i);
                break;
            } else {
                length = als.get(i).length();
            }
        }
    }
}

