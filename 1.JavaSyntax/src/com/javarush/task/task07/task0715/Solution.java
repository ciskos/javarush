package com.javarush.task.task07.task0715;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Продолжаем мыть раму
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        ArrayList<String> als = new ArrayList<>();
        
        als.add("мама");
        als.add("мыла");
        als.add("раму");
        
        for (int i = 1; i < als.size() + 1; i += 2) {
            als.add(i, "именно");
        }

        for (String s : als) {
            System.out.println(s);
        }
    }
}
