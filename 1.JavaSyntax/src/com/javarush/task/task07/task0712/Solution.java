package com.javarush.task.task07.task0712;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Самые-самые
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> als = new ArrayList<>();
        int shortest = 0;
        int longest = 0;
        
        for (int i = 0; i < 10; i++) {
            als.add(br.readLine());
        }

        shortest = als.get(0).length();
        longest = als.get(0).length();
        for (int i = 0; i < als.size(); i++) {
            if (shortest > als.get(i).length()) shortest = als.get(i).length();
            if (longest < als.get(i).length()) longest = als.get(i).length();
        }
        
        for (int i = 0; i < als.size(); i++) {
            if (als.get(i).length() == shortest) {
                System.out.println(als.get(i));
                break;
            } else if (als.get(i).length() == longest) {
                System.out.println(als.get(i));
                break;
            }
        }
    }
}
