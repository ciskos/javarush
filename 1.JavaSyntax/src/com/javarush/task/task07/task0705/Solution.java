package com.javarush.task.task07.task0705;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Один большой массив и два маленьких
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int bigArr[] = new int[20];
        int smallArr1[] = new int[10];
        int smallArr2[] = new int[10];
        
        for (int i = 0; i < bigArr.length; i++) {
            bigArr[i] = Integer.parseInt(br.readLine());
            
            if (i <= 9) {
                smallArr1[i] = bigArr[i];
            } else {
                smallArr2[i - 10] = bigArr[i];
                System.out.println(smallArr2[i - 10]);
            }
        }
    }
}
