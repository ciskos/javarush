package com.javarush.task.task07.task0707;

import java.util.ArrayList;

/* 
Что за список такой?
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        ArrayList<String> al = new ArrayList<>();
        
        al.add("1");
        al.add("2");
        al.add("3");
        al.add("4");
        al.add("5");
        
        System.out.println(al.size());
        
        for (String s : al) {
            System.out.println(s);
        }
    }
}
