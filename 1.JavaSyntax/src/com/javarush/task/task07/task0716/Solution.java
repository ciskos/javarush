package com.javarush.task.task07.task0716;

import java.util.ArrayList;

/* 
Р или Л
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        ArrayList<String> list = new ArrayList<String>();
        list.add("роза"); // 0
        list.add("лоза"); // 1
        list.add("лира"); // 2
        list = fix(list);

        for (String s : list) {
            System.out.println(s);
        }
    }

    public static ArrayList<String> fix(ArrayList<String> list) {
        //напишите тут ваш код
    	ArrayList<String> als = new ArrayList<>();
        
        for (int i = 0; i < list.size(); i++) {
            String s = list.get(i);
//            System.out.println(i + " " +s);
            boolean r = false;
            boolean l = false;
            for (int j = 0; j < s.length(); j++) {
                // System.out.println(s.charAt(j) == 'р');
                if (s.charAt(j) == 'р') {
//                	System.out.println("r true");
                    r = true;
                }
                
                if (s.charAt(j) == 'л') {
//                	System.out.println("l true");
                    l = true;
                }
//                System.out.println("r " + r + " l " + l);
            }
            
            if (!r && l) {
//            	System.out.println("adding " + list.get(i));
                als.add(list.get(i));
                als.add(list.get(i));
            } else if (r && !l) {
//            	System.out.println("removing " + list.get(i));
            } else {
            	als.add(list.get(i));
            }
        }
        
        return als;
    }
}