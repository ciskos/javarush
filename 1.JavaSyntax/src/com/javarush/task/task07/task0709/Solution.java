package com.javarush.task.task07.task0709;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Выражаемся покороче
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> strings = new ArrayList<>();
        int length = 0;
        
        for (int i = 0; i < 5; i++) {
            strings.add(br.readLine());
        }
        
        length = strings.get(0).length();
        for (int i = 0; i < 5; i++) {
            if (strings.get(i).length() < length) {
                length = strings.get(i).length();
            }
        }
        
        for (String s : strings) {
            if (s.length() == length) {
                System.out.println(s);
            }
        }
    }
}
