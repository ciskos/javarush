package com.javarush.task.task09.task0921;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Метод в try..catch
*/

public class Solution {
    public static void main(String[] args) {
        readData();
    }

    public static void readData() {
        //напишите тут ваш код
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Integer> ali = new ArrayList<>();

        try {
            while (true) {
                ali.add(Integer.parseInt(br.readLine()));
            }
        } catch (NumberFormatException | IOException e) {
            e.printStackTrace();
        }

        for (Integer i : ali) {
            System.out.println(i);
        }
    }
}
