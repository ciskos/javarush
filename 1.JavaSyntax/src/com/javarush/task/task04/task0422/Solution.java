package com.javarush.task.task04.task0422;

/* 
18+
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        Scanner sc = new Scanner(System.in);
        
        String name = sc.nextLine();
        int age = sc.nextInt();
        int ageLine = 18;
        
        if (age < ageLine) {
            System.out.println("Подрасти еще");
        }
    }
}
