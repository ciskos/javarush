package com.javarush.task.task04.task0419;

/* 
Максимум четырех чисел
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        Scanner sc = new Scanner(System.in);
        
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        int d = sc.nextInt();
        int first;
        int second;
        
        if (a > b) {
            first = a;
        } else {
            first = b;
        }
        
        if (c > d) {
            second = c;
        } else {
            second = d;
        }
        
        if (a == b & b == c & c == d) {
            System.out.println(a);
        } else if (first > second) {
            System.out.println(first);
        } else {
            System.out.println(second);
        }
    }
}
