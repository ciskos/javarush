package com.javarush.task.task04.task0427;

/* 
Описываем числа
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        Scanner sc = new Scanner(System.in);
        
        int a = sc.nextInt();
        // int a = 9;
        boolean even = a % 2 == 0;
        int count = 0;
        String msg = "";
        
        if (a >= 1 && a <= 999) {
            while (a > 0) {
                count++;
                a /= 10;
            }
            
            if (even) {
                msg = "четное ";
            } else {
                msg = "нечетное ";
            }
            
            switch(count) {
                case 1:
                    msg += "однозначное ";
                    break;
                case 2:
                    msg += "двузначное ";
                    break;
                case 3:
                    msg += "трехзначное ";
                    break;
            }
            
            // System.out.println(even);
            // System.out.println(count);
            System.out.println(msg + "число");
        }

    }
}
