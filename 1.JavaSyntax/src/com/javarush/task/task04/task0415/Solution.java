package com.javarush.task.task04.task0415;

/* 
Правило треугольника
*/

import java.io.*;
import java.util.Scanner;
// import java.lang.Math;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        Scanner sc = new Scanner(System.in);
        
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        int varC = a + b;
        int varB = a + c;
        int varA = b + c;

        // if (Math.sqrt(varC) > Math.sqrt(c)) {
        //     System.out.println("Треугольник существует.");
        // } else if (Math.sqrt(varB) > Math.sqrt(b)) {
        //     System.out.println("Треугольник существует.");
        // } else if (Math.sqrt(varA) > Math.sqrt(a)) {
        //     System.out.println("Треугольник существует.");
        // } else {
        //     System.out.println("Треугольник не существует.");
        // }
        // if (varC > c) {
        //     System.out.println("Треугольник существует.");
        // } else if (varB > b) {
        //     System.out.println("Треугольник существует.");
        // } else if (varA > a) {
        //     System.out.println("Треугольник существует.");
        // } else {
        //     System.out.println("Треугольник не существует.");
        // }
        
        if (varC <= c || varB <= b || varA <= a) {
            System.out.println("Треугольник не существует.");
        } else {
            System.out.println("Треугольник существует.");
        }

    }
}