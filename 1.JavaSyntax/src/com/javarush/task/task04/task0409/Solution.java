package com.javarush.task.task04.task0409;

/* 
Ближайшее к 10
*/

public class Solution {
    public static void main(String[] args) {
        displayClosestToTen(8, 11);
        displayClosestToTen(7, 14);
        displayClosestToTen(-7, 14);
    }

    public static void displayClosestToTen(int a, int b) {
        // напишите тут ваш код
        // int absA = abs(a);
        // int absB = abs(b);
        int ten = 10;
        int closestA = abs(ten - a);
        int closestB = abs(ten - b);

        if (closestA < closestB) {
            System.out.println(a);
        } else if (closestA > closestB) {
            System.out.println(b);
        } else {
            System.out.println(a);
        }

    }

    public static int abs(int a) {
        if (a < 0) {
            return -a;
        } else {
            return a;
        }
    }
}