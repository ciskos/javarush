package com.javarush.task.task04.task0416;

/* 
Переходим дорогу вслепую
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        Scanner sc = new Scanner(System.in);
        
        double t = sc.nextDouble();
        
        double mod = t % 5;

        if ((mod >= 0 & mod < 3) | mod == 5) {
            System.out.println("зелёный");
        } else if ((mod >= 3 & mod < 4)) {
            System.out.println("жёлтый");
        } else if (mod >= 4 & mod < 5) {
            System.out.println("красный");
        }

    }
}