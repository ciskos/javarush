package com.javarush.task.task04.task0442;


/* 
Суммирование
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        Scanner sc = new Scanner(System.in);
        int number = 0;
        int summ = 0;
        
        while (true) {
            number = sc.nextInt();
            summ += number;
            
            if (number == -1) {
                System.out.println(summ);
                break;
            }
        }
    }
}
