package com.javarush.task.task04.task0441;


/* 
Как-то средненько
*/
import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        // 3 2 1    2
        // 4 4 3    4
        // 5 5 5    5
        // 3 4 3    3
        Scanner sc = new Scanner(System.in);
        
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        
        // int a = 4;
        // int b = 4;
        // int c = 3;
        
        int mid = 0;
        
        if ((a < b && a > c) || (a > b && a < c)) {
            mid = a;
        } else if ((b < a && b > c) || (b < c && b > a)) {
            mid = b;
        } else if ((c < a && c > b) || (c < b && c > a)) {
            mid = c;
        } else if (a == b && b == c) {
            mid = a;
        } else if (a == b && a != c) {
            mid = a;
        } else if (b == c && a != b) {
            mid = b;
        } else if (a == c && b != c) {
            mid = c;
        }

        System.out.println(mid);
    }
}
