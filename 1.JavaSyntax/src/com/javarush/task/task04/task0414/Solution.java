package com.javarush.task.task04.task0414;

/* 
Количество дней в году
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        Scanner sc = new Scanner(System.in);
        
        int year = sc.nextInt();
        int fourHundred = 400;
        int oneHundred = 100;
        int four = 4;
        
        // System.out.println(year % fourHundred);
        // System.out.println(year % oneHundred);
        // System.out.println(year % fourHundred);
        
        if (year % fourHundred == 0) {
            System.out.println("количество дней в году: 366");
        } else if (year % oneHundred == 0) {
            System.out.println("количество дней в году: 365");
        } else if (year % four == 0) {
            System.out.println("количество дней в году: 366");
        } else {
            System.out.println("количество дней в году: 365");
        }
    }
}