package com.javarush.task.task04.task0417;

/* 
Существует ли пара?
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
        Scanner sc = new Scanner(System.in);
        
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();

        if (a == b & b == c) {
            System.out.print(a + " ");
            System.out.print(b + " ");
            System.out.print(c + " ");
        } else if (a == b) {
            System.out.print(a + " ");
            System.out.print(b + " ");
        } else if (a == c) {
            System.out.print(a + " ");
            System.out.print(c + " ");
        } else if (b == c) {
            System.out.print(b + " ");
            System.out.print(c + " ");
        }
    }
}