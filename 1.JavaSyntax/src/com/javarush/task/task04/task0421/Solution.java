package com.javarush.task.task04.task0421;

/* 
Настя или Настя?
*/

import java.io.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {
	public String name;

    public static void main(String[] args) throws Exception {
        //напишите тут ваш код
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Solution t1 = new Solution(); 
		Solution t2 = new Solution();
        
		t1.name = br.readLine();
		t2.name = br.readLine();

        if (t1.name.equals(t2.name)) {
            System.out.println("Имена идентичны");
        } else if (t1.name.length() == t2.name.length()) {
            System.out.println("Длины имен равны");
        }
    }
    
	public boolean equals(Solution t) {
    	// TODO Auto-generated method stub
    	//    	return super.equals(obj);
    	return this.name == t.name;
	}
}
