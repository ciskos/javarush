package com.javarush.task.task12.task1217;

/* 
Лететь, бежать и плыть
*/

public class Solution {
    public static void main(String[] args) {

    }

//add interfaces here - добавь интерфейсы тут
    public interface CanFly {
        public String getName();
    }
    public interface CanRun {
        public String getName();
    }
    public interface CanSwim {
        public String getName();
    }

}
