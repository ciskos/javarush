package com.javarush.task.task17.task1710;

import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/* 
CRUD
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) {
        //start here - начни тут
//        String[] args = {"-c", "Миронов", "м", "15/04/1990"};

        if (args.length != 0) {
            String key = args[0];
            int id = 0;
            String name = null;
            String sex = null;
            String bd = null;
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            Date bDate = null;

            switch (key) {
                case "-c":
                    name = args[1];
                    sex = args[2];
                    bd = args[3];

                    try {
                        bDate = sdf.parse(bd);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    createPerson(name, sex, bDate);
                    break;
                case "-u":
                    id = Integer.parseInt(args[1]);
                    name = args[2];
                    sex = args[3];
                    bd = args[4];

                    try {
                        bDate = sdf.parse(bd);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    updatePerson(id, name, sex, bDate);
                    break;
                case "-d":
                    id = Integer.parseInt(args[1]);
                    deletePerson(id);
                    break;
                case "-i":
                    id = Integer.parseInt(args[1]);
                    showInfo(id);
                    break;
                default:
                    System.out.println("No such option");
            }
        }
    }

    public static void createPerson(String name, String sex, Date bd) {
        if (sex.equals("ж")) {
            allPeople.add(Person.createFemale(name, bd));
        } else if (sex.equals("м")) {
            allPeople.add(Person.createMale(name, bd));
        } else {
            System.out.println("No such sex");
            return;
        }

        System.out.println((allPeople.size() - 1));
    }

    public static void updatePerson(int id, String name, String sex, Date bd) {
        Sex s = null;
        if (sex.equals("ж")) {
            s = Sex.FEMALE;
        } else if (sex.equals("м")) {
            s = Sex.MALE;
        } else {
            System.out.println("No such sex");
            return;
        }
        allPeople.get(id).setName(name);
        allPeople.get(id).setSex(s);
        allPeople.get(id).setBirthDate(bd);
    }

    public static void deletePerson(int id) {
        allPeople.get(id).setName(null);
        allPeople.get(id).setSex(null);
        allPeople.get(id).setBirthDate(null);
    }

    public static void showInfo(int id) {
        String name = allPeople.get(id).getName();
        Sex sex = allPeople.get(id).getSex();
        Date bd = allPeople.get(id).getBirthDate();
        String sx = null;
        Calendar c = Calendar.getInstance();
        c.setTime(bd);
        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);

        if (sex == Sex.FEMALE) {
            sx = "ж";
        } else if (sex == Sex.MALE) {
            sx = "м";
        } else {
            System.out.println("No such sex");
        }

        System.out.println(name + " " + sx + " " + day + "-" + takeMonth(month) + "-" + year);
    }

    public static String takeMonth(int m) {
        String month = "";
        switch (m) {
            case 0:
                month = "Jan";
                break;
            case 1:
                month = "Feb";
                break;
            case 2:
                month = "Mar";
                break;
            case 3:
                month = "Apr";
                break;
            case 4:
                month = "May";
                break;
            case 5:
                month = "Jun";
                break;
            case 6:
                month = "Jul";
                break;
            case 7:
                month = "Aug";
                break;
            case 8:
                month = "Sep";
                break;
            case 9:
                month = "Oct";
                break;
            case 10:
                month = "Nov";
                break;
            case 11:
                month = "Dec";
                break;
        }
        return month;
    }
}
