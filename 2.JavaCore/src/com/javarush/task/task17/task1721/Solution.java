package com.javarush.task.task17.task1721;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/* 
Транзакционность
*/

/*
 * 1. Класс Solution должен содержать public static поле allLines типа List<String>.
 * 2. Класс Solution должен содержать public static поле forRemoveLines типа List<String>.
 * 3. Класс Solution должен содержать public void метод joinData() который может бросать исключение CorruptedDataException.
 * 4. Программа должна считывать c консоли имена двух файлов.
 * 5. Программа должна считывать построчно данные из первого файла в список allLines.
 * 6. Программа должна считывать построчно данные из второго файла в список forRemoveLines.
 * 7. Метод joinData должен удалить в списке allLines все строки из списка forRemoveLines, если в allLines содержаться
 * все строки из списка forRemoveLines.
 * 8. Метод joinData должен очистить список allLines и выбросить исключение CorruptedDataException, если в allLines не
 * содержаться все строки из списка forRemoveLines.
 * 9. Метод joinData должен вызываться в main.
 * */
public class Solution {
    public static List<String> allLines = new ArrayList<String>();
    public static List<String> forRemoveLines = new ArrayList<String>();
    private static String firstFile = null;
    private static String secondFile = null;


    public static void main(String[] args) {
        Solution sol = new Solution();

        try {
            getFileNames();
            readFileToList(allLines, firstFile);
            readFileToList(forRemoveLines, secondFile);
            sol.joinData();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readFileToList(List<String> list, String file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        Scanner sc = new Scanner(fis);

        while (sc.hasNextLine()) {
            list.add(sc.nextLine());
        }

        fis.close();
        sc.close();
    }

    public static void getFileNames() throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        firstFile = br.readLine();
        secondFile = br.readLine();

        isr.close();
        br.close();
    }

    public void joinData() throws CorruptedDataException {
        if (allLines.containsAll(forRemoveLines)) {
            for (int i = 0; i < forRemoveLines.size(); i++) {
                for (int j = 0; j < allLines.size(); j++) {
                    if (forRemoveLines.get(i).equals(allLines.get(j))) {
                        allLines.remove(j);
                    }
                }
            }
        } else {
            allLines.clear();
            throw new CorruptedDataException();
        }
    }
}
