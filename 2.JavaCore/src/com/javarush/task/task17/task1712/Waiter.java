package com.javarush.task.task17.task1712;

public class Waiter implements Runnable {
    public boolean continueWorking = true;

    @Override
    public void run() {
        Manager manager = Manager.getInstance();

        while (continueWorking || needToGetOrders()) {
            if (needToGetOrders()) {       //относим готовый заказ
                bringOrder();
            } else {                       //берем новый заказ
                takeOrder();
            }

            try {
                Thread.sleep(100);  //walking to the next table
            } catch (InterruptedException e) {
            }
        }
    }

    public void bringOrder() {
        Dishes dishes = Manager.getInstance().getDishesQueue().poll();
        System.out.println("Официант отнес заказ для стола №" + dishes.getTableNumber());
    }

    public void takeOrder() {
        Table table = Manager.getInstance().getNextTable();
        Order order = table.getOrder();
        System.out.println("Получен заказ от стола №" + order.getTableNumber());
        Manager.getInstance().getOrderQueue().add(order);
    }

    private boolean needToGetOrders() {
        return !Manager.getInstance().getDishesQueue().isEmpty();
    }
}
