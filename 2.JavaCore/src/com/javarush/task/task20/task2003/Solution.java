package com.javarush.task.task20.task2003;

import java.io.*;
import java.util.Map;
import java.util.HashMap;
import java.util.Properties;

/* 
Знакомство с properties
*/
public class Solution {
    public static Map<String, String> properties = new HashMap<>();

    public void fillInPropertiesMap() {
        //implement this method - реализуйте этот метод
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        String file = null;

        try {
            file = br.readLine();
//            file = ".properties";
            FileInputStream fis = new FileInputStream(file);
            load(fis);

            br.close();
            isr.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void save(OutputStream outputStream) throws Exception {
        //implement this method - реализуйте этот метод
        if (!properties.isEmpty()) {
            Properties p = new Properties();
            p.putAll(properties);
            p.store(outputStream, null);
        }
    }

    public void load(InputStream inputStream) throws Exception {
        //implement this method - реализуйте этот метод
        Properties p = new Properties();
        p.load(inputStream);

        for (String key : p.stringPropertyNames()) {
            properties.put(key, p.getProperty(key));
        }
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        String file = ".properties";
        properties.put("1", "one");
        properties.put("2", "two");
        properties.put("3", "three");
        try {
            OutputStream os = new FileOutputStream(file);
            s.save(os);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        s.fillInPropertiesMap();
    }
}
