package com.javarush.task.task20.task2022;

import java.io.*;

/* 
Переопределение сериализации в потоке
*/
public class Solution implements Serializable, AutoCloseable {
    // 1 Установить transient
    transient private FileOutputStream stream;
    private String fileName;

    public Solution(String fileName) throws FileNotFoundException {
        this.fileName = fileName;
        // 5 Инициализировать stream
        this.stream = new FileOutputStream(fileName);
    }

    public void writeObject(String string) throws IOException {
        stream.write(string.getBytes());
        stream.write("\n".getBytes());
        stream.flush();
    }

    // 2 Не использовать close
    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
//        out.close();
    }

    // 3 Не использовать close
    // 4 Инициализировать stream новым значением
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        stream = new FileOutputStream(fileName, true);
//        in.close();
    }

    @Override
    public void close() throws Exception {
        System.out.println("Closing everything!");
        stream.close();
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        // Не проверяется валидатором
        String file = "file_task2022.txt";
//        String data = "data for task 2022";
        Solution s = new Solution(file);
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);

//        s.writeObject(data);
        oos.writeObject(s);

        FileInputStream fis = new FileInputStream("file_task2022.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);

        Solution s1 = (Solution)ois.readObject();
        System.out.println("data " + s1.fileName);
    }
}
