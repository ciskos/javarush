package com.javarush.task.task20.task2002;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* 
Читаем и пишем в файл: JavaRush
*/
public class Solution {
    public static void main(String[] args) {
        //you can find your_file_name.tmp in your TMP directory or adjust outputStream/inputStream
        // according to your file's actual location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream
        // в соответствии с путем к вашему реальному файлу
        try {
//            File yourFile = File.createTempFile("your_file_name", null);
            String yourFile = "yourFile.txt";
            OutputStream outputStream = new FileOutputStream(yourFile);
            InputStream inputStream = new FileInputStream(yourFile);

            JavaRush javaRush = new JavaRush();
            //initialize users field for the javaRush object here -
            // инициализируйте поле users для объекта javaRush тут
            User user0 = new User();
            User user1 = new User();

            user0.setFirstName("FirstName");
            user0.setLastName("LastName");
            user0.setBirthDate(new Date());
            user0.setMale(true);
            user0.setCountry(User.Country.OTHER);

            user1.setFirstName("FirstName1");
            user1.setLastName("LastName1");
            user1.setBirthDate(new Date());
            user1.setMale(false);
            user1.setCountry(User.Country.OTHER);

            javaRush.users.add(user0);
            javaRush.users.add(user1);
            javaRush.save(outputStream);
            outputStream.flush();

            JavaRush loadedObject = new JavaRush();
            loadedObject.load(inputStream);
            //here check that the codeGym object is equal to the loadedObject object -
            // проверьте тут, что javaRush и loadedObject равны
            System.out.println(javaRush.equals(loadedObject));
//            for (User u : javaRush.users) {
//                System.out.println(u.getFirstName() + " " + u.getLastName() + " " + u.getBirthDate() + " " + u.isMale() + " " + u.getCountry());
//            }

//            for (User u : loadedObject.users) {
//                System.out.println(u.getFirstName() + " " + u.getLastName() + " " + u.getBirthDate() + " " + u.isMale() + " " + u.getCountry());
//            }

            outputStream.close();
            inputStream.close();

        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Oops, something is wrong with my file");
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Oops, something is wrong with the writeObject/readObject method");
        }
    }

    public static class JavaRush {
        public List<User> users = new ArrayList<>();

        public void save(OutputStream outputStream) throws Exception {
            //implement this method - реализуйте этот метод
            PrintWriter pw = new PrintWriter(outputStream);
            String isUsersEmpty = users.isEmpty() ? "yes" : "no";

            pw.println(isUsersEmpty);
            pw.flush();

            if (!users.isEmpty()) {
                for (User u : users) {
                    pw.println(u.getFirstName());
                    pw.println(u.getLastName());
                    pw.println(u.getBirthDate().getTime());
                    pw.println(u.isMale());
                    pw.println(u.getCountry());
                }
            }
            pw.close();
        }

        public void load(InputStream inputStream) throws Exception {
            //implement this method - реализуйте этот метод
            InputStreamReader isr = new InputStreamReader(inputStream);
            BufferedReader br = new BufferedReader(isr);

            String isUsersEmpty = br.readLine();

            if (isUsersEmpty.equals("no")) {
                String firstName;
                while ((firstName = br.readLine()) != null) {
                    User u = new User();
                    String lastName = br.readLine();
                    String bDate = br.readLine();
                    Boolean sex = Boolean.valueOf(br.readLine());
                    String country = br.readLine();

                    u.setFirstName(firstName);
                    u.setLastName(lastName);
                    u.setBirthDate(new Date(Long.parseLong(bDate)));
                    u.setMale(sex);
                    u.setCountry(User.Country.valueOf(country));

                    users.add(u);
                }
            }

            br.close();
            isr.close();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            JavaRush javaRush = (JavaRush) o;

            return users != null ? users.equals(javaRush.users) : javaRush.users == null;

        }

        @Override
        public int hashCode() {
            return users != null ? users.hashCode() : 0;
        }
    }
}
