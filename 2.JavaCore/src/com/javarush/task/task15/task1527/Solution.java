package com.javarush.task.task15.task1527;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Парсер реквестов
*/

public class Solution {
    public static void main(String[] args) {
        //add your code here
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String input = null;

        try {
            input = br.readLine();
//            input = "http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo";
//            input = "http://javarush.ru/alpha/index.html?obj=3.14&name=Amigo";
            String[] part1 = input.split("[?]");
            String[] part2 = part1[1].split("[&]");

            for (String s : part2) {
                String[] str = s.split("[=]");
                System.out.print(str[0] + " ");
            }

            System.out.println();
            for (String s : part2) {
                String[] str = s.split("[=]");

                if (str[0].equals("obj")) {
                    try {
                        alert(Double.parseDouble(str[1]));
                    } catch (NumberFormatException e) {
                        alert(str[1]);
                    }
                }
//                System.out.println(str[0]);
            }

            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void alert(double value) {
        System.out.println("double: " + value);
    }

    public static void alert(String value) {
        System.out.println("String: " + value);
    }
}
