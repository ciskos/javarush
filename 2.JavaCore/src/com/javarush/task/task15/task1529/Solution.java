package com.javarush.task.task15.task1529;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Осваивание статического блока
*/

public class Solution {
    public static CanFly result;

    static {
        //add your code here - добавьте код тут
        reset();
    }

    public static void main(String[] args) {
    }

    public static void reset() {
        //add your code here - добавьте код тут
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String input = null;
        int passengers;

        try {
            input = br.readLine();
            if (input.equals("helicopter")) {
                result = new Helicopter();
            } else if (input.equals("plane")) {
                passengers = Integer.parseInt(br.readLine());
                result = new Plane(passengers);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
