package com.javarush.task.task15.task1505;

// Изменено JavaRush-ем
//import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;

import java.util.ArrayList;
import java.util.List;

/* 
ООП - исправь ошибки в наследовании
*/

public class Solution {

    public static void main(String[] args) {
        printlnFingers();
        printlnBodyParts();
        printlnLivingParts();
    }

    private static void printlnLivingParts() {
        System.out.println(new BodyPart("Рука").containsBones());
    }

    private static void printlnBodyParts() {
        List<BodyPart> bodyParts = new ArrayList<BodyPart>(5);
        bodyParts.add(new BodyPart("Рука"));
        bodyParts.add(new BodyPart("Нога"));
        bodyParts.add(new BodyPart("Голова"));
        bodyParts.add(new BodyPart("Тело"));
        System.out.println(bodyParts.toString());
    }

    private static void printlnFingers() {
        List<Finger> fingers = new ArrayList<Finger>(5);
        fingers.add(new Finger("Большой", true));
        fingers.add(new Finger("Указательный", true));
        fingers.add(new Finger("Средний", true));
        fingers.add(new Finger("Безымянный", false));
        fingers.add(new Finger("Мизинец", true));
        System.out.println(fingers.toString());
    }

    public static interface LivingPart {
        Object containsBones();
    }

    public static class BodyPart implements LivingPart {
        private String name;
//        private boolean containBone;

//        public BodyPart(String name, boolean containBone) {
        public BodyPart(String name) {
            this.name = name;
//            this.containBone = containBone;
        }

        public Object containsBones() {
//            String yes = "Yes";
//            String no = "No";
//
//            if (!this.containBone) {
//                return no;
//            }

            return "Yes";
        }

        public String toString() {
            String contains = name + " содержит кости";
            String containsNot = name + " не содержит кости";
            return containsBones().equals("Yes") ? contains : containsNot;
        }
    }

    public static class Finger extends BodyPart {
        private boolean isArtificial;

        public Finger(String name, boolean isArtificial) {
            super(name);
            this.isArtificial = isArtificial;
        }

        public Object containsBones() {
            Object obj = super.containsBones();
            String yes = "Yes";
            String no = "No";
            return obj.equals(yes) && !isArtificial ? yes : no;
        }
    }

}

