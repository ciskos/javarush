package com.javarush.task.task15.task1519;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/* 
Разные методы для разных типов
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        //напиште тут ваш код
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String input = null;

        input = br.readLine();
        while (!input.equals("exit")) {
            try {
                if (Integer.parseInt(input) <= 0 || Integer.parseInt(input) >= 128) {
                    print(Integer.parseInt(input));
                } else if (Short.parseShort(input) > 0 && Short.parseShort(input) < 128) {
                    print(Short.parseShort(input));
                }
            } catch (NumberFormatException e) {
                try {
                    print(Double.parseDouble(input));
                } catch (NumberFormatException e1) {
                    print(input);
                }
            }
            input = br.readLine();
        }


    }

    public static void print(Double value) {
        System.out.println("Это тип Double, значение " + value);
    }

    public static void print(String value) {
        System.out.println("Это тип String, значение " + value);
    }

    public static void print(short value) {
        System.out.println("Это тип short, значение " + value);
    }

    public static void print(Integer value) {
        System.out.println("Это тип Integer, значение " + value);
    }
}
