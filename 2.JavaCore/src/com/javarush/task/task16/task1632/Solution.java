package com.javarush.task.task16.task1632;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static List<Thread> threads = new ArrayList<>(5);

    static {
        threads.add(new T1());
        threads.add(new T2());
        threads.add(new T3());
        threads.add(new T4());
        threads.add(new T5());
    }

    public static void main(String[] args) {
    }

    public static class T1 extends Thread {
        @Override
        public void run() {
            while (true) {}
        }
    }

    public static class T2 extends Thread {
        @Override
        public void run() {
            try {
                throw new InterruptedException();
            } catch (InterruptedException e) {
                System.out.println("InterruptedException");
            }
        }
    }

    public static class T3 extends Thread {
        @Override
        public void run() {
            while (true) {
                try {
                    System.out.println("Ура");
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class T4 extends Thread implements Message {

        @Override
        public void showWarning() {
            interrupt();
        }

        @Override
        public void run() {
            while (!isInterrupted()) {}
        }
    }

    public static class T5 extends Thread {
        @Override
        public void run() {
            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(isr);
            String s = "";
            int sum = 0;

            try {
                s = br.readLine();
                while (true) {
                    if (s.equals("N")) break;
                    sum += Integer.parseInt(s);
                    s = br.readLine();
                }
                System.out.println(sum);
                isr.close();
                br.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}