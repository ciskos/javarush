package com.javarush.task.task13.task1319;

import java.io.*;

/* 
Писатель в файл с консоли
*/

public class Solution {
    public static void main(String[] args) {
        // напишите тут ваш код
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = null;
        FileOutputStream fos;
//        StringBuilder sb = new StringBuilder();
        String file = null;
        String input = null;
        String stop = "exit";

        try {
            file = br.readLine();
            if (file.isEmpty()) return;
            bw = new BufferedWriter(new FileWriter(file));
        } catch (IOException e) {
            e.printStackTrace();
        }

//        System.out.println(file);
        do {
            try {
                input = br.readLine();
                if (input.isEmpty()) break;
//                fos = new FileOutputStream(file);
                bw.write(input);
                bw.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
//            System.out.println(input);
        } while (!input.equals(stop));

        try {
            br.close();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
