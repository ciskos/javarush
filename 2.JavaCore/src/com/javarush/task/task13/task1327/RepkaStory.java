package com.javarush.task.task13.task1327;

import java.util.List;

public class RepkaStory {
    static void tell(List<Person> items) {
        Person first;
        Person second;
        first = items.get(items.size() - 1);
//        System.out.println(first.getName());
        for (int i = items.size() - 2; i >= 0; i--) {
            second = items.get(i);
            first.pull(second);
            first = items.get(i);
//            System.out.println(second.getName());
        }
    }
}
