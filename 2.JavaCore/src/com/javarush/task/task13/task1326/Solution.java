package com.javarush.task.task13.task1326;

/* 
Сортировка четных чисел из файла
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        // напишите тут ваш код
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Integer> ali = new ArrayList<>();
        Scanner sc;

        try {
            FileInputStream fis = new FileInputStream(br.readLine());
            sc = new Scanner(fis);

            while (sc.hasNextInt()) {
                ali.add(Integer.parseInt(sc.next()));
            }

            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Integer[] arr = ali.toArray(new Integer[ali.size()]);
        Arrays.sort(arr);

        for (Integer i : arr) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
    }
}
