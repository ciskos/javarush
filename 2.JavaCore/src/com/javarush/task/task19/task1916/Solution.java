package com.javarush.task.task19.task1916;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Отслеживаем изменения
*/

public class Solution {
    public static List<LineItem> lines = new ArrayList<LineItem>();

    public static void main(String[] args) {

        try (
                InputStreamReader isr = new InputStreamReader(System.in);
                BufferedReader br = new BufferedReader(isr);
            ) {

            String file1 = br.readLine();
            String file2 = br.readLine();
//            String file1 = "file1.txt";
//            String file2 = "file2.txt";
            FileReader fr1 = new FileReader(file1);
            FileReader fr2 = new FileReader(file2);
            BufferedReader br1 = new BufferedReader(fr1);
            BufferedReader br2 = new BufferedReader(fr2);
            ArrayList<String> als1 = new ArrayList<>();
            ArrayList<String> als2 = new ArrayList<>();
            String str1 = "";
            String str2 = "";

            while ((str1 = br1.readLine()) != null) {
                als1.add(str1);
            }

            while ((str2 = br2.readLine()) != null) {
                als2.add(str2);
            }

            for (int i = 0, j = 0; i < als1.size() | j < als2.size();) {
                String s1 = i >= als1.size() ? "" : als1.get(i);
                String s2 = j >= als2.size() ? "" : als2.get(j);
                int nextI = i + 1;
                int nextJ = j + 1;
                String nextS1 = nextI >= als1.size() ? "" : als1.get(nextI);
                String nextS2 = nextJ >= als2.size() ? "" : als2.get(nextJ);

                if (s1.equals(s2)) {
                    lines.add(new LineItem(Type.SAME, s1));
                    i++;
                    j++;
                } else if (s1.equals(nextS2)) {
                    lines.add(new LineItem(Type.ADDED, s2));
                    j++;
                } else if (nextS1.equals(s2)) {
                    lines.add(new LineItem(Type.REMOVED, s1));
                    i++;
                }
            }

            br1.close();
            br2.close();
            fr2.close();
            fr1.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static enum Type {
        ADDED,        //добавлена новая строка
        REMOVED,      //удалена строка
        SAME          //без изменений
    }

    public static class LineItem {
        public Type type;
        public String line;

        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }
    }
}
