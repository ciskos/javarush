package com.javarush.task.task19.task1908;

/* 
Выделяем числа
*/

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) {


        try (
                InputStreamReader isr = new InputStreamReader(System.in);
                BufferedReader br = new BufferedReader(isr);
        ) {
            String file1 = br.readLine();
            String file2 = br.readLine();
//            String file1 = "digit.txt";
//            String file2 = "digitOut.txt";
            String s = "";

            FileReader fr = new FileReader(file1);
            BufferedReader brFR = new BufferedReader(fr);
            Pattern pattern = Pattern.compile("\\b\\d+\\b");

            while (brFR.ready()) {
                s += (char)brFR.read();
            }

//            System.out.println(s);

            Matcher matcher = pattern.matcher(s);
            FileWriter fw = new FileWriter(file2);
            BufferedWriter bwFW = new BufferedWriter(fw);

            while (matcher.find()) {
//                System.out.println(s.substring(matcher.start(), matcher.end()));
                bwFW.write(s.substring(matcher.start(), matcher.end()) + " ");
            }

            bwFW.close();
            brFR.close();
            fw.close();
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
