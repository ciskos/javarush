package com.javarush.task.task19.task1904;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

/* 
И еще один адаптер
*/

public class Solution {

    public static void main(String[] args) {
//        File file = new File("person.txt");
//        FileInputStream fis = null;
//        try {
//            fis = new FileInputStream(file);
//            Scanner sc = new Scanner(fis);
//            PersonScannerAdapter psa = new PersonScannerAdapter(sc);
//
//            while (sc.hasNextLine()) {
//                Person p = psa.read();
//                System.out.println(p);
//            }
//
//            fis.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    public static class PersonScannerAdapter implements PersonScanner {
        private Scanner fileScanner;

        public PersonScannerAdapter(Scanner fileScanner) {
            this.fileScanner = fileScanner;
        }

        @Override
        public Person read() throws IOException {
            String s = this.fileScanner.nextLine();
            String[] personData = s.split("\\s");
            String firstName = personData[1];
            String middleName = personData[2];
            String lastName = personData[0];
            String date = personData[3] + " " + personData[4] + " " + personData[5];
            SimpleDateFormat sdf = new SimpleDateFormat("dd MM yyyy");
            Date bd = null;

            try {
                bd = sdf.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return new Person(firstName, middleName, lastName, bd);
        }

        @Override
        public void close() throws IOException {
            this.fileScanner.close();
        }
    }
}
