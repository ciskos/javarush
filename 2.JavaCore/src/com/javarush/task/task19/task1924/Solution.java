package com.javarush.task.task19.task1924;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* 
Замена чисел
*/

public class Solution {
    public static Map<Integer, String> map = new HashMap<Integer, String>();

    static {
        map.put(0, "ноль");
        map.put(1, "один");
        map.put(2, "два");
        map.put(3, "три");
        map.put(4, "четыре");
        map.put(5, "пять");
        map.put(6, "шесть");
        map.put(7, "семь");
        map.put(8, "восемь");
        map.put(9, "девять");
        map.put(10, "десять");
        map.put(11, "одиннадцать");
        map.put(12, "двенадцать");
    }

    public static void main(String[] args) {
        String file;
        try (
                InputStreamReader isr = new InputStreamReader(System.in);
                BufferedReader br = new BufferedReader(isr);
            ) {
            file = br.readLine();
//            file = "digitsToWords.txt";
            FileReader fr = new FileReader(file);
            BufferedReader brFR = new BufferedReader(fr);

            String s = "";
            while (brFR.ready()) {
                s += brFR.readLine();
                s += "\n";
            }

            Iterator<Map.Entry<Integer, String>> mapI = map.entrySet().iterator();

            while (mapI.hasNext()) {
                Map.Entry<Integer, String> next = mapI.next();
                Integer k = next.getKey();
                String v = next.getValue();
                Pattern p = Pattern.compile("\\b" + k + "\\b");

                s = s.replaceAll(p.pattern(), v);
            }
            System.out.println(s);

            brFR.close();
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
