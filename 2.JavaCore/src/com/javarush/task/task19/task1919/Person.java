package com.javarush.task.task19.task1919;

public class Person {
    private String name;
    private double sallary;

    public Person(String name, double sallary) {
        this.name = name;
        this.sallary = sallary;
    }

    public String getName() {
        return name;
    }

    public double getSallary() {
        return sallary;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSallary(double sallary) {
        this.sallary = sallary;
    }
}
