package com.javarush.task.task19.task1919;

/* 
Считаем зарплаты
*/

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeSet;

public class Solution {
    public static void main(String[] args) {
//        String[] args = {"sallary.txt"};

        try (
                FileReader fr = new FileReader(args[0]);
                BufferedReader brFR = new BufferedReader(fr);
            ) {
            String s;
            ArrayList<Person> alP = new ArrayList<>();
            TreeSet<String> hss = new TreeSet<>();

            while (( s = brFR.readLine()) != null) {
                String[] str = s.split(" ");
                alP.add(new Person(str[0].trim(), Double.parseDouble(str[1].trim())));
            }

            for (Person p : alP) {
                hss.add(p.getName());
            }

            for (String str : hss) {
                double sallary = 0.0;

                for (Person p : alP) {
                String name = p.getName();
                    if (str.equals(name)) {
                        sallary += p.getSallary();
                    }
                }

                System.out.println(str + " " + sallary);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
