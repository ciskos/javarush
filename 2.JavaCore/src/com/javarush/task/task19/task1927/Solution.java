package com.javarush.task.task19.task1927;

/* 
Контекстная реклама
*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream consoleStream = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(baos);

        System.setOut(stream);
        testString.printSomething();
        System.setOut(consoleStream);

        String[] s = baos.toString().split("\n");
        int i = 0;
        int countRow = 0;
        while (i < s.length) {
            if (countRow == 2) {
                System.out.println("JavaRush - курсы Java онлайн");
                countRow = 0;
            } else {
                System.out.println(s[i]);
                countRow++;
                i++;
            }
        }

    }

    public static class TestString {
        public void printSomething() {
            System.out.println("first");
            System.out.println("second");
            System.out.println("third");
            System.out.println("fourth");
            System.out.println("fifth");
        }
    }
}
