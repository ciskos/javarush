package com.javarush.task.task19.task1923;

/* 
Слова с цифрами
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) {
        String file0 = args[0];
        String file1 = args[1];
//        String file0 = "wordsWithDigits.txt";
//        String file1 = "wordsWithDigitsOut.txt";


        try (
                FileReader fr = new FileReader(file0);
                BufferedReader brFR = new BufferedReader(fr);
                FileWriter fw = new FileWriter(file1);
            ) {

            String s;
            while ((s = brFR.readLine()) != null) {
                String[] str = s.split(" ");
                for (int i = 0; i < str.length; i++) {
                    Pattern p = Pattern.compile(".*\\d+.*");
                    Matcher m = p.matcher(str[i].trim());

                    while (m.find()) {
//                        System.out.println("(" + str[i] + ")");
                        fw.write(str[i].trim() + " ");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
