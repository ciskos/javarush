package com.javarush.task.task19.task1922;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Ищем нужные строки
*/

public class Solution {
    public static List<String> words = new ArrayList<String>();

    static {
        words.add("файл");
        words.add("вид");
        words.add("В");
    }

    public static void main(String[] args) {
        try (
                InputStreamReader isr = new InputStreamReader(System.in);
                BufferedReader br = new BufferedReader(isr);
        ) {
            String file = br.readLine();
//            String file = "words.txt";
            FileReader fr = new FileReader(file);
            BufferedReader brFR = new BufferedReader(fr);

            String s;
            while ((s = brFR.readLine()) != null) {
                String[] str = s.split(" ");
                int countWords = 0;
                int numberOfWords = 2;
                for (int i = 0; i < words.size(); i++) {
                    for (int j = 0; j < str.length; j++) {
                        if (words.get(i).equals(str[j])) {
                            countWords++;
                        }
                    }
                }
                if (countWords == numberOfWords) {
                    System.out.println(s);
                }
                countWords = 0;
            }

            brFR.close();
            fr.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
