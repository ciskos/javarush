package com.javarush.task.task19.task1925;

/* 
Длинные слова
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) {
        String file0 = args[0];
        String file1 = args[1];
//        String file0 = "6LetterWords.txt";
//        String file1 = "6LetterWordsOut.txt";

        try (
                FileReader fr = new FileReader(file0);
                BufferedReader brFR = new BufferedReader(fr);
                FileWriter fw = new FileWriter(file1);
            ) {
            ArrayList<String> words = new ArrayList<>();

            String s;
            while ((s = brFR.readLine()) != null) {
                String[] str = s.split(" ");
                for (int i = 0; i < str.length; i++) {
                    if (str[i].length() > 6) {
                        words.add(str[i]);
                    }
                }
            }

            for (int i = 0; i < words.size(); i++) {
                if (i < (words.size() - 1)) {
                    fw.write(words.get(i) + ",");
                } else {
                    fw.write(words.get(i));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
