package com.javarush.task.task19.task1906;

/* 
Четные символы
*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String file1;
        String file2;

        try {
            file1 = br.readLine();
            file2 = br.readLine();
//            file1 = "text.txt";
//            file2 = "textOut.txt";

            String s = "";
            ArrayList<Character> alc = new ArrayList<>();

            FileReader fr = new FileReader(file1);
            while (fr.ready()) {
                alc.add((char) fr.read());
            }

            FileWriter fw = new FileWriter(file2);
            for (int i = 0; i < alc.size(); i++) {
                if (i % 2 != 0) {
                    fw.write(alc.get(i));
                }
            }

            fw.close();
            fr.close();
            br.close();
            isr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
