package com.javarush.task.task19.task1918;

/* 
Знакомство с тегами
*/

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String file;
//        String[] args = {"tag"};

        try {
            file = br.readLine();
//            file = "html2.txt";
            FileReader fr = new FileReader(file);
            BufferedReader brFR = new BufferedReader(fr);
            String s = "";

            while (brFR.ready()) {
                s += (char)brFR.read();
            }

            Document html = Jsoup.parse(s, "", Parser.xmlParser());
            Elements elements = html.select(args[0]);

            for (Element e : elements) {
                System.out.println(e);
            }

            brFR.close();
            fr.close();
            br.close();
            isr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
