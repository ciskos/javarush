package com.javarush.task.task19.task1909;

/* 
Замена знаков
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) {

        try (
            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(isr);
            ) {
            String file1 = br.readLine();
            String file2 = br.readLine();
//            String file1 = "dots.txt";
//            String file2 = "dotsOut.txt";
            String s = "";
            FileReader fr = new FileReader(file1);
            BufferedReader brFR = new BufferedReader(fr);

            while (brFR.ready()) {
                s += (char) brFR.read();
            }

            s = s.replaceAll("\\.", "!");
            FileWriter fw = new FileWriter(file2);
            BufferedWriter bwFW = new BufferedWriter(fw);

            bwFW.write(s);

            bwFW.close();
            brFR.close();
            fw.close();
            fr.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
