package com.javarush.task.task19.task1920;

/* 
Самый богатый
*/

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/*public class Solution {
    public static void main(String[] args) throws IOException {
        FileReader f = new FileReader("sallary2.txt");

        Map<String, Double> human = new HashMap<>();
        String name = "";
        Double zp = 0.0;
        double max = 0.0;
        BufferedReader reader = new BufferedReader(f);
        while (true) {
            String s = reader.readLine();
            if (s == null) break;
            name = s.split(" ")[0];
            zp = Double.parseDouble(s.split(" ")[1]);
            if (max < zp) {
                max = zp;
            }
            if(human.containsKey(name)){
                double plzp = human.get(name) + zp;
                System.out.println(human.get(name));
                if (max < plzp) {
                    max = plzp;
                }
                human.put(name, plzp);
            }else
                human.put(name, zp);
        }

        for (Map.Entry<String, Double> entry : human.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        reader.close();
        f.close();

        List<String> nameM = new ArrayList<>();
        for (Map.Entry<String, Double> par : human.entrySet()){
            if(par.getValue() == max)
                nameM.add(par.getKey());
        }

        Collections.sort(nameM);
        for(String s : nameM){
            System.out.println(s);
        }
    }
}*/

public class Solution {
    public static void main(String[] args) {
//        String[] args = {"sallary2.txt"};

        try (
                FileReader fr = new FileReader(args[0]);
                BufferedReader brFR = new BufferedReader(fr);
        ) {
            String s;
            ArrayList<Person> alP = new ArrayList<>();
            TreeMap<String, Double> tmSD = new TreeMap<>();

            while (( s = brFR.readLine()) != null) {
                String[] str = s.split(" ");
                alP.add(new Person(str[0].trim(), Double.parseDouble(str[1].trim())));
                tmSD.put(str[0].trim(), 0.0);
            }

            for (int i = 0; i < alP.size(); i++) {
                Person p = alP.get(i);
                Iterator<Map.Entry<String, Double>> hmiI = tmSD.entrySet().iterator();
                while (hmiI.hasNext()) {
                    Map.Entry<String, Double> next = hmiI.next();
                    String k = next.getKey();
                    Double v = next.getValue();

                    if (k.equals(p.getName())) {
                        tmSD.replace(k, v + p.getSallary());
                    }
                }
            }

            Double maxSallary = Collections.max(tmSD.values());

            for (Map.Entry<String, Double> entry : tmSD.entrySet()) {
                String k = entry.getKey();
                Double v = entry.getValue();
                if (v.equals(maxSallary)) {
                    System.out.println(k);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
