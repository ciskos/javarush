package com.javarush.task.task19.task1921;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* 
Хуан Хуанович
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) {
//        String[] args = {"people.txt"};

        try (
                FileReader fr = new FileReader(args[0]);
                BufferedReader brFR = new BufferedReader(fr);
        ) {
            String s;

            while (( s = brFR.readLine()) != null) {
                Pattern p = Pattern.compile("(.+)\\s(\\d+)\\s(\\d+)\\s(\\d+)");
                Matcher m = p.matcher(s);
                while (m.find()) {
                    String name = m.group(1);
                    String day = m.group(2);
                    String month = m.group(3);
                    String year = m.group(4);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd MM yyyy");
                    Date bd = sdf.parse(day + " " + month + " " + year);

                    PEOPLE.add(new Person(name, bd));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
