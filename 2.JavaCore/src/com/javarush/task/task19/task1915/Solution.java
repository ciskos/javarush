package com.javarush.task.task19.task1915;

/* 
Дублируем текст
*/

import java.io.*;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream consoleStream = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(baos);

        System.setOut(stream);
        testString.printSomething();
        System.setOut(consoleStream);

        System.out.println(baos.toString());

        String fileOutput;



        try (
            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(isr);
        ) {
            fileOutput = br.readLine();
//            fileOutput = "doubling.txt";

            FileOutputStream fos = new FileOutputStream(fileOutput);

            fos.write(baos.toString().getBytes());

            fos.close();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("it's a text for testing");
        }
    }
}

