package com.javarush.task.task19.task1914;

/* 
Решаем пример
*/

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream consoleStream = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(baos);

        System.setOut(stream);
        testString.printSomething();
        System.setOut(consoleStream);

        Pattern p = Pattern.compile("(\\d+)\\s([\\+\\-\\*])\\s(\\d+)");
        String s = baos.toString();
        Matcher m = p.matcher(s);

        m.find();
        Integer a = Integer.parseInt(m.group(1));
        Integer b = Integer.parseInt(m.group(3));
        Integer c;

        switch (m.group(2)) {
            case "+":
                c = a + b;
                System.out.println(a + " + " + b + " = " + c);
                break;
            case "-":
                c = a - b;
                System.out.println(a + " - " + b + " = " + c);
                break;
            case "*":
                c = a * b;
                System.out.println(a + " * " + b + " = " + c);
                break;
        }

        try {
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("3 + 6 = ");
        }
    }
}

