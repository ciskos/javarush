package com.javarush.task.task19.task1903;

/* 
Адаптация нескольких интерфейсов
*/

import java.util.HashMap;
import java.util.Map;

public class Solution {
    public static Map<String, String> countries = new HashMap<String, String>();

    static {
        countries.put("UA", "Ukraine");
        countries.put("RU", "Russia");
        countries.put("CA", "Canada");
    }

    public static void main(String[] args) {

    }

    public static class IncomeDataAdapter implements Customer, Contact {
        private IncomeData data;

        public IncomeDataAdapter(IncomeData data) {
            this.data = data;
        }

        @Override
        public String getCompanyName() {
            return this.data.getCompany();
        }

        @Override
        public String getCountryName() {
            return countries.get(this.data.getCountryCode());
        }

        @Override
        public String getName() {
            String name = this.data.getContactFirstName();
            String lastName = this.data.getContactLastName();
            return lastName + ", " + name;
        }

        @Override
        public String getPhoneNumber() {
            String countryCode = String.valueOf(this.data.getCountryPhoneCode());
            String countryPhoneNumber = String.valueOf(this.data.getPhoneNumber());
            String phoneCode = "";
            String phoneNumber = "";
            String phoneFirstPart = "";
            String phoneSecondPart = "";
            String phoneThirdPart = "";
            while (countryPhoneNumber.length() < 10){
                countryPhoneNumber = "0" + countryPhoneNumber;
            }
            phoneCode = countryPhoneNumber.substring(0, 3);
            phoneFirstPart = countryPhoneNumber.substring(3, 6);
            phoneSecondPart = countryPhoneNumber.substring(6, 8);
            phoneThirdPart = countryPhoneNumber.substring(8, 10);

            return "+" + countryCode + "(" + phoneCode + ")" + phoneFirstPart + "-" + phoneSecondPart + "-" + phoneThirdPart;
//            return ("+" + countryCode + phoneNumber);
        }
    }


    public static interface IncomeData {
        String getCountryCode();        //For example: UA

        String getCompany();            //For example: JavaRush Ltd.

        String getContactFirstName();   //For example: Ivan

        String getContactLastName();    //For example: Ivanov

        int getCountryPhoneCode();      //For example: 38

        int getPhoneNumber();           //For example: 501234567
    }

    public static interface Customer {
        String getCompanyName();        //For example: JavaRush Ltd.

        String getCountryName();        //For example: Ukraine
    }

    public static interface Contact {
        String getName();               //For example: Ivanov, Ivan

        String getPhoneNumber();        //For example: +38(050)123-45-67
    }
}