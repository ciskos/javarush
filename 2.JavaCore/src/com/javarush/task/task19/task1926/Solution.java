package com.javarush.task.task19.task1926;

/* 
Перевертыши
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) {
        try (
                InputStreamReader isr = new InputStreamReader(System.in);
                BufferedReader br = new BufferedReader(isr);
            ) {
            String file = br.readLine();
//            String file = "reverse.txt";
            FileReader fr = new FileReader(file);
            BufferedReader brFR = new BufferedReader(fr);

            String s;
            while ((s = brFR.readLine()) != null) {
                System.out.println(new StringBuilder(s).reverse());
            }

            brFR.close();
            fr.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
