package com.javarush.task.task18.task1817;

/* 
Пробелы
*/

import java.io.FileInputStream;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class Solution {
    public static void main(String[] args) {
//        String[] args = {"a.txt"};
        double spaces = 0.0;
        double symbols = 0.0;
        Locale currentLocale = Locale.getDefault();
        DecimalFormatSymbols dfs = new DecimalFormatSymbols(currentLocale);
        dfs.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#.##", dfs);
        df.setRoundingMode(RoundingMode.CEILING);
//        NumberFormat format = new DecimalFormat("0.00");
//        format.setRoundingMode(RoundingMode.CEILING);


        try {
            FileInputStream fis = new FileInputStream(args[0]);

//            symbols = fis.available();
            while (fis.available() > 0) {
                int symbol = fis.read();
                if ((char)symbol == ' ') {
                    spaces++;
                    symbols++;
                } else {
                    symbols++;
                }
            }

            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        System.out.printf("spaces %.2f\n", spaces);
//        System.out.printf("symbols %.2f\n", symbols);
//        System.out.println(currentLocale);
        double result = (double) spaces / symbols * 100;
//        System.out.printf("%.2f ", result);
        System.out.println(df.format(result));
//        System.out.println(format.format(result));
    }
}
