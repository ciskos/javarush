package com.javarush.task.task18.task1820;

/* 
Округление чисел
*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String fileInput = null;
        String fileOutput = null;

        try {
            fileInput = br.readLine();
            fileOutput = br.readLine();

            FileInputStream fis = new FileInputStream(fileInput);
            FileOutputStream fos = new FileOutputStream(fileOutput);
            BufferedReader fileInputStream = new BufferedReader(new InputStreamReader(fis));
            BufferedWriter fileOutputStream = new BufferedWriter(new OutputStreamWriter(fos));

            Scanner sc = new Scanner(fileInputStream);
            while (sc.hasNextLine()) {
                Float f = Float.valueOf(sc.next());
                fileOutputStream.write(Math.round(f) + " ");
            }

            fileOutputStream.close();
            fileInputStream.close();
            fos.close();
            fis.close();
            br.close();
            isr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
