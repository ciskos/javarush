package com.javarush.task.task18.task1825;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/* 
Собираем файл
*/

public class Solution {
    public static void main(String[] args) {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String file = null;
        String stopWord = "end";
        ArrayList<String> listFiles = new ArrayList<>();

        readFiles(br, stopWord, listFiles);
        Collections.sort(listFiles);
        writeTo(listFiles);

        try {
            br.close();
            isr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeTo(ArrayList<String> listFiles) {
        String[] fileNameParts = listFiles.get(0).split("\\.");
        try {
            FileOutputStream fos = new FileOutputStream(fileNameParts[0] + "." + fileNameParts[1], true);

            for (int i = 0; i < listFiles.size(); i++) {
                FileInputStream fis = new FileInputStream(listFiles.get(i));

                byte[] buffer = new byte[fis.available()];

                if (fis.available() > 0) {
                    int count1 = fis.read(buffer);
                    fos.write(buffer, 0, count1);
                }
                fis.close();
            }
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readFiles(BufferedReader br, String stopWord, ArrayList<String> listFiles) {
        String file;
        try {
            file = br.readLine();
            while (!file.equals(stopWord)) {
                listFiles.add(file);
                file = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
