package com.javarush.task.task18.task1819;

/* 
Объединение файлов
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Solution {
    public static void main(String[] args) {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String file1 = null;
        String file2 = null;
        ArrayList<Byte> alb = new ArrayList<>();

        try {
            file1 = br.readLine();
            file2 = br.readLine();

            FileInputStream fis1 = new FileInputStream(file1);
            FileInputStream fis2 = new FileInputStream(file2);

            while (fis2.available() > 0) {
                alb.add((byte) fis2.read());
            }

            while (fis1.available() > 0) {
                alb.add((byte) fis1.read());
            }
            FileOutputStream fos1 = new FileOutputStream(file1);

            fos1.flush();
            for (Byte b : alb) {
                fos1.write(b);
            }

            fos1.close();
            fis1.close();
            fis2.close();
            br.close();
            isr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
