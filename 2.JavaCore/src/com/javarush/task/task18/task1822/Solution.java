package com.javarush.task.task18.task1822;

/* 
Поиск данных внутри файла
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String file = null;
//        String[] args = {"0"};

        try {
            file = br.readLine();
//            file = "id.txt";

            FileInputStream fis = new FileInputStream(file);
            Scanner sc = new Scanner(fis);

            while (sc.hasNextLine()) {
                String s = sc.nextLine();
                String[] strings = s.split(" ");
                if (strings[0].equals(args[0])) {
                    System.out.println(s);
                    break;
                }
            }

            fis.close();
            br.close();
            isr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
