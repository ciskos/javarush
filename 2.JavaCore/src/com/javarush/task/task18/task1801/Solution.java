package com.javarush.task.task18.task1801;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/* 
Максимальный байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String file = "";
        int maxByte = 0;
        int read = 0;

        file = br.readLine();
        br.close();

        FileInputStream fis = new FileInputStream(file);

        while (fis.available() > 0) {
            read = fis.read();
            if (maxByte < read) maxByte = read;
        }

        fis.close();

        System.out.println(maxByte);
    }
}
