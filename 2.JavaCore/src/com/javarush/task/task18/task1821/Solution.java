package com.javarush.task.task18.task1821;

/* 
Встречаемость символов
*/

import java.io.*;
import java.util.*;

public class Solution {
    public static void main(String[] args) {
//        String[] args = {"a.txt"};
        ArrayList<Character> alc = new ArrayList<>();
        HashMap<Character, Integer> hmii = new HashMap<>();
        TreeSet<Character> tsc = new TreeSet<>();

        fileToList(args[0], alc);
        Collections.sort(alc);
        listToTreeSet(alc, tsc);
        listToHashMap(alc, hmii);
        countChars(alc, hmii);
        toConsole(tsc, hmii);
    }

    public static void listToTreeSet(ArrayList<Character> alc, Set<Character> tsc) {
        for (Character c : alc) {
            tsc.add(c);
        }
    }

    public static void listToHashMap(ArrayList<Character> alc, HashMap<Character, Integer> hmii) {
        for (Character c : alc) {
            hmii.put(c, 0);
        }
    }

    public static void fileToList(String arg, ArrayList<Character> alc) {
        try {
            FileInputStream fis = new FileInputStream(arg);

            while (fis.available() > 0) {
                alc.add((char) fis.read());
            }

            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void countChars(ArrayList<Character> alc, HashMap<Character, Integer> hmii) {
        for (int i = 0; i < alc.size(); i++) {
            Character in = alc.get(i);
            Iterator<Map.Entry<Character, Integer>> hmiI = hmii.entrySet().iterator();
            while (hmiI.hasNext()) {
                Map.Entry<Character, Integer> next = hmiI.next();
                Character k = next.getKey();
                Integer v = next.getValue();

                if (k == in) {
                    hmii.replace(k, ++v);
                }
            }
        }
    }

    public static void toConsole(TreeSet<Character> tsc, HashMap<Character, Integer> hmii) {
        for (Character c : tsc) {
            Iterator<Map.Entry<Character, Integer>> hmiI = hmii.entrySet().iterator();
            while (hmiI.hasNext()) {
                Map.Entry<Character, Integer> next = hmiI.next();
                Character k = next.getKey();
                Integer v = next.getValue();

                if (k == c) {
                    System.out.println(k + " " + v);
                }
            }
        }
    }
}
