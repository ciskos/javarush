package com.javarush.task.task18.task1809;

/* 
Реверс файла
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class Solution {
    public static void main(String[] args) {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        ArrayList<Integer> ali = new ArrayList<>();
        String file1 = null;
        String file2 = null;

        try {
            file1 = br.readLine();
            file2 = br.readLine();

            FileInputStream fis = new FileInputStream(file1);
            FileOutputStream fos = new FileOutputStream(file2);

            while (fis.available() > 0) {
                ali.add(fis.read());
            }

            Collections.reverse(ali);
            for (Integer i : ali) {
                fos.write(i);
            }

            fis.close();
            fos.close();
            isr.close();
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
