package com.javarush.task.task18.task1823;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* 
Нити и байты
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        ArrayList<String> fileList = new ArrayList<>();
        String input = null;
        String stopWord = "exit";

        try {
            input = br.readLine();
            while (!input.equals(stopWord)) {
                fileList.add(input);
                input = br.readLine();
            }

            for (String s : fileList) {
                ReadThread rt = new ReadThread(s);
                rt.start();
                rt.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Map.Entry<String, Integer> entry : resultMap.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }

    public static class ReadThread extends Thread {
        private String filename;
        private FileInputStream fis;

        public ReadThread(String fileName) throws FileNotFoundException {
            //implement constructor body
            this.filename = fileName;
            this.fis = new FileInputStream(fileName);
//            start();
        }
        // implement file reading here - реализуйте чтение из файла тут

        @Override
        public void run() {
            ArrayList<Integer> ali = new ArrayList<>();
            HashMap<Integer, Integer> hmii = new HashMap<>();

            try {
                while (fis.available() > 0) {
                    int read = fis.read();
                    ali.add(read);
                    hmii.put(read, 0);
                }

                countBytes(ali, hmii);

                int maxKey = -1;
                int maxValue = -1;
                for (Map.Entry<Integer, Integer> entry : hmii.entrySet()) {
                    Integer key = entry.getKey();
                    Integer value = entry.getValue();
                    if (value > maxValue) {
                        maxKey = key;
                        maxValue = value;
                    }
                }

                resultMap.put(filename, maxKey);

                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void countBytes(ArrayList<Integer> ali, HashMap<Integer, Integer> hmii) {
        for (int i = 0; i < ali.size(); i++) {
            Integer in = ali.get(i);
            Iterator<Map.Entry<Integer, Integer>> hmiI = hmii.entrySet().iterator();
            while (hmiI.hasNext()) {
                Map.Entry<Integer, Integer> next = hmiI.next();
                Integer k = next.getKey();
                Integer v = next.getValue();

                if (k == in) {
                    hmii.replace(k, ++v);
                }
            }
        }
    }

}
