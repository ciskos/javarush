package com.javarush.task.task18.task1803;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/* 
Самые частые байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        ArrayList<Integer> alI = new ArrayList<>();
        HashMap<Integer, Integer> hmi = new HashMap<>();
        String file = "";
        int maxByte = 0;
//        int read = 0;

        file = br.readLine();
        br.close();

//        public static Map<String, Integer> countWords(ArrayList<String> list) {
//            HashMap<String, Integer> result = new HashMap<String, Integer>();
//
//            //напишите тут ваш код
//            for (String s : list) {
//                result.put(s.toLowerCase(), 0);
//            }
//            for (int i = 0; i < list.size(); i++) {
//                String l = list.get(i).toLowerCase();
//                Iterator<Map.Entry<String, Integer>> resultI = result.entrySet().iterator();
//                while (resultI.hasNext()) {
//                    Map.Entry<String, Integer> next = resultI.next();
//                    String k = next.getKey();
//                    Integer v = next.getValue();
//
//                    if (k.equals(l)) {
//                        result.replace(k, ++v);
//                    }
//                }
//            }
//
//            return result;
//        }

        FileInputStream fis = new FileInputStream(file);

        while (fis.available() > 0) {
            int read = fis.read();
            alI.add(read);
            hmi.put(read, 0);
        }

        for (int i = 0; i < alI.size(); i++) {
            Integer in = alI.get(i);
            Iterator<Map.Entry<Integer, Integer>> hmiI = hmi.entrySet().iterator();
            while (hmiI.hasNext()) {
                Map.Entry<Integer, Integer> next = hmiI.next();
                Integer k = next.getKey();
                Integer v = next.getValue();

                if (k == in) {
                    hmi.replace(k, ++v);
                }
            }
        }

        Iterator<Map.Entry<Integer, Integer>> hmiI = hmi.entrySet().iterator();
        while (hmiI.hasNext()) {
            Map.Entry<Integer, Integer> next = hmiI.next();
            Integer k = next.getKey();
            Integer v = next.getValue();

            if (v == 0) {
                hmi.replace(k, ++v);
            }
        }

        fis.close();

        int max = 0;
        for (Map.Entry<Integer, Integer> entry : hmi.entrySet()) {
            if (entry.getValue() > max) {
                max = entry.getValue();
            }
        }

        for (Map.Entry<Integer, Integer> entry : hmi.entrySet()) {
            if (entry.getValue() == max) {
                System.out.print(entry.getKey() + " ");
            }
        }
    }
}
