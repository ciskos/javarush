package com.javarush.task.task18.task1808;

/* 
Разделение файла
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String file1 = null;
        String file2 = null;
        String file3 = null;

        try {
            file1 = br.readLine();
            file2 = br.readLine();
            file3 = br.readLine();

            FileInputStream fis = new FileInputStream(file1);
            FileOutputStream fos1 = new FileOutputStream(file2);
            FileOutputStream fos2 = new FileOutputStream(file3);
            byte[] buffer1;
            byte[] buffer2;

            int fisAvailable = fis.available();
            if (fisAvailable % 2 == 0) {
                buffer1 = new byte[fisAvailable / 2];
                buffer2 = new byte[fisAvailable / 2];
            } else {
                buffer1 = new byte[(fisAvailable / 2) + (fisAvailable % 2)];
                buffer2 = new byte[fisAvailable / 2];
            }

            if (fis.available() > 0) {
                int count1 = fis.read(buffer1);
                int count2 = fis.read(buffer2);

                fos1.write(buffer1, 0, count1);
                fos2.write(buffer2, 0, count2);
            }

            fis.close();
            fos1.close();
            fos2.close();
            isr.close();
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
