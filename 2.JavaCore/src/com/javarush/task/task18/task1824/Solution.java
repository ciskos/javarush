package com.javarush.task.task18.task1824;

/* 
Файлы и исключения
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        FileInputStream fis = null;
        String file = null;

        while (true) {
            try {
                file = br.readLine();
                fis = new FileInputStream(file);
                fis.close();
            } catch (FileNotFoundException e) {
                System.out.println(file);
                break;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

