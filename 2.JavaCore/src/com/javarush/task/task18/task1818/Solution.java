package com.javarush.task.task18.task1818;

/* 
Два в одном
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String file1 = null;
        String file2 = null;
        String file3 = null;
        byte[] buffer1;
        byte[] buffer2;

        try {
            file1 = br.readLine();
            file2 = br.readLine();
            file3 = br.readLine();

            FileOutputStream fos1 = new FileOutputStream(file1, true);
            FileInputStream fis1 = new FileInputStream(file2);
            FileInputStream fis2 = new FileInputStream(file3);

            buffer1 = new byte[fis1.available()];
            buffer2 = new byte[fis2.available()];

            if (fis1.available() > 0) {
                int count1 = fis1.read(buffer1);
                fos1.write(buffer1, 0, count1);
            }

            if (fis2.available() > 0) {
                int count2 = fis2.read(buffer2);
                fos1.write(buffer2, 0, count2);
            }

            fos1.close();
            fis1.close();
            fis2.close();
            br.close();
            isr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
