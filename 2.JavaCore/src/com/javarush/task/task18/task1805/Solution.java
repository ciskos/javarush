package com.javarush.task.task18.task1805;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

/* 
Сортировка байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        HashSet<Integer> hsi = new HashSet<>();
        String file = br.readLine();
        FileInputStream fis = new FileInputStream(file);

        while (fis.available() > 0) {
            hsi.add(fis.read());
        }

        ArrayList<Integer> ali = new ArrayList<>(hsi);
        Collections.sort(ali);

        for (Integer i : ali) {
            System.out.print(i + " ");
        }

        isr.close();
        br.close();
        fis.close();
    }
}
