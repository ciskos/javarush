package com.javarush.task.task18.task1828;

/* 
Прайсы 2
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
//        String[] args = {"-c", "p0123456789012345678901234567890123456789", "0", "012"};
//        String[] args = {"-u", "3", "p123", "2.0", "1"};
//        String[] args = {"-d", "6"};
        if (args.length != 0) {
            String create = "-c";
            String update = "-u";
            String delete = "-d";
            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(isr);
            ArrayList<Product> alP = new ArrayList<>();
            String file = null;

            file = br.readLine();
//            file = "product.txt";

            fillList(alP, file);

            if (args[0].equals(create)) {
                addToFile(file, args, alP);
                return;
            } else if (args[0].equals(update)) {
                updateFile(file, alP, args);
                return;
            } else if (args[0].equals(delete)) {
                deleteFromFile(file, alP, args);
                return;
            }

            isr.close();
            br.close();
        }

    }

    public static void deleteFromFile(String file, ArrayList<Product> alP, String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream(file);

        for (int i = 0; i < alP.size(); i++) {
            if (!alP.get(i).getId().equals(args[1])) {
                String id = Product.setIdLength(alP.get(i).getId());
                String productName = Product.setProductNameLength(alP.get(i).getProductName());
                String price = Product.setPrieceLength(alP.get(i).getPrice());
                String quantity = Product.setQuantityLength(alP.get(i).getQuantity());
                String productToUpdate = id + productName + price + quantity;
                fos.write(productToUpdate.getBytes());
                if (i != (alP.size() - 1)) {
                    fos.write(13);
                }
            }
        }
        fos.close();
    }

    public static void updateFile(String file, ArrayList<Product> alP, String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream(file);
        String id = args[1];
        String productName = args[2];
        String price = args[3];
        String quantity = args[4];

        for (Product p : alP) {
            if (p.getId().equals(id)) {
                p.setProductName(productName);
                p.setPrice(price);
                p.setQuantity(quantity);
            }
        }

        for (int i = 0; i < alP.size(); i++) {
            id = Product.setIdLength(alP.get(i).getId());
            productName = Product.setProductNameLength(alP.get(i).getProductName());
            price = Product.setPrieceLength(alP.get(i).getPrice());
            quantity = Product.setQuantityLength(alP.get(i).getQuantity());
            String productToUpdate = id + productName + price + quantity;
            fos.write(productToUpdate.getBytes());

            if (i != (alP.size() - 1)) {
                fos.write(13);
            }
        }

        fos.close();
    }

    public static void addToFile(String file, String[] args, ArrayList<Product> alP) throws IOException {
        FileOutputStream fos = new FileOutputStream(file, true);
        String productToAdd;
        String productName = args[1];
        String price = args[2];
        String quantity = args[3];
        int id = 0;
        String nextID;

        id = getLastId(alP);
        nextID = Product.setIdLength(String.valueOf(id + 1));
        productName = Product.setProductNameLength(productName);
        price = Product.setPrieceLength(price);
        quantity = Product.setQuantityLength(quantity);
        productToAdd = nextID + productName + price + quantity;

        fos.write(13);
        for (int j = 0; j < productToAdd.length(); j++) {
            fos.write(productToAdd.charAt(j));
        }

        fos.close();
    }

    public static int getLastId(ArrayList<Product> alP) {
        int id = 0;
        for (Product p : alP) {
            int i = Integer.valueOf(p.getId().trim());
            if (i > id) id = i;
        }
        return id;
    }

    public static void fillList(ArrayList<Product> als, String file) throws IOException  {
        FileInputStream fis = new FileInputStream(file);
        Scanner sc = new Scanner(fis);

        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            Product p = new Product();

            String id = s.substring(0,8).trim();
            String productName = s.substring(8, 38).trim();
            String price = s.substring(38, 46).trim();
            String quantity = s.substring(46).trim();

            p.setId(id);
            p.setProductName(productName);
            p.setPrice(price);
            p.setQuantity(quantity);
            als.add(p);
        }

        fis.close();
    }
}
