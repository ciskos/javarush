package com.javarush.task.task18.task1828;

public class Product {
    private String id;
    private String productName;
    private String price;
    private String quantity;

    public static String setIdLength(String value) {
        return String.format("%-8.8s", value);
    }

    public static String setProductNameLength(String value) {
        return String.format("%-30.30s", value);
    }

    public static String setPrieceLength(String value) {
        return String.format("%-8.8s", value);
    }

    public static String setQuantityLength(String value) {
        return String.format("%-4.4s", value);
    }

    public String getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getPrice() {
        return price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
