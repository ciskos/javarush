package com.javarush.task.task18.task1827;

public class Product {
    private int id;
    private String productName;
    private double priece;
    private int quantity;

    public int getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public double getPriece() {
        return priece;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setPriece(double priece) {
        this.priece = priece;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
