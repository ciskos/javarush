package com.javarush.task.task18.task1827;

/* 
Прайсы
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
//        String[] args = {"-c", "p0123456789012345678901234567890123456789", "0.0", "012345"};
        if (args.length != 0) {
            String create = "-c";
            if (args[0].equals(create)) {
                InputStreamReader isr = new InputStreamReader(System.in);
                BufferedReader br = new BufferedReader(isr);
                ArrayList<Product> alP = new ArrayList<>();
                String productToAdd;
                String file = null;
                String productName = args[1];
                String price = args[2];
                String quantity = args[3];
                int id = 0;
                String nextID;

                file = br.readLine();
//                file = "product.txt";

                fillList(alP, file);
                id = getLastId(alP);
                nextID = getID(String.valueOf(id + 1));
                productName = getProductName(productName);
                price = getPriece(price);
                quantity = getQuantity(quantity);
                productToAdd = nextID + productName + price + quantity;
//                System.out.println("(" + nextID + ")(" + productName + ")(" + price + ")(" + quantity + ")");
//                System.out.println(productToAdd);
                writeToFile(file, productToAdd);
//                String lineSeparator = System.getProperty("line.separator");
//                System.out.println(lineSeparator);

                isr.close();
                br.close();
            }
        }

    }

    public static void writeToFile(String file, String productToAdd) throws IOException {
        FileOutputStream fos = new FileOutputStream(file, true);
//        String lineSeparator = System.getProperty("line.separator");
//        System.out.println(lineSeparator.getBytes());
//        fos.write(lineSeparator.getBytes());


        fos.write(13);
        for (int j = 0; j < productToAdd.length(); j++) {
            fos.write(productToAdd.charAt(j));
        }

        fos.close();
    }

    public static int getLastId(ArrayList<Product> alP) {
        int id = 0;
        for (Product p : alP) {
            if (p.getId() > id) id = p.getId();
        }
        return id;
    }

    public static void fillList(ArrayList<Product> als, String file) throws IOException  {
        FileInputStream fis = new FileInputStream(file);
        Scanner sc = new Scanner(fis);

        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            Product p = new Product();

            Integer id = Integer.valueOf(s.substring(0,8).trim());
            String productName = s.substring(8, 38).trim();
            Double price = Double.valueOf(s.substring(38, 46).trim());
            Integer quantity = Integer.valueOf(s.substring(46).trim());

            p.setId(id);
            p.setProductName(productName);
            p.setPriece(price);
            p.setQuantity(quantity);
            als.add(p);
        }

        fis.close();
    }

    public static String getID(String value) {
        return String.format("%-8.8s", value);
    }

    public static String getProductName(String value) {
        return String.format("%-30.30s", value);
    }

    public static String getPriece(String value) {
        return String.format("%-8.8s", value);
    }

    public static String getQuantity(String value) {
        return String.format("%-4.4s", value);
    }
}
