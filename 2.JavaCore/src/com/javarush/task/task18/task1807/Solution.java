package com.javarush.task.task18.task1807;

/* 
Подсчет запятых
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String file = null;
        int symbol = 44;
        int count = 0;

        try {
            file = br.readLine();
            FileInputStream fis = new FileInputStream(file);
            while (fis.available() > 0) {
                if (fis.read() == 44) {
                    count++;
                }
            }
            isr.close();
            br.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(count);
    }
}
