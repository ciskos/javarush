package com.javarush.task.task18.task1802;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/* 
Минимальный байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String file = "";
        int minByte = 65536;
        int read = 0;

        file = br.readLine();
        br.close();

        FileInputStream fis = new FileInputStream(file);

        while (fis.available() > 0) {
            read = fis.read();
            if (minByte > read) minByte = read;
        }

        fis.close();

        System.out.println(minByte);
    }
}
