package com.javarush.task.task18.task1826;

/* 
Шифровка
*/

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) {
        String encrypt = "-e";
        String decrypt = "-d";
//        String[] args = {"-e", "encrypt.txt", "encryptOut.txt"};
//        String[] args = {"-d", "encryptOut.txt", "decryptOut.txt"};
        String fileInput = args[1];
        String fileOutput = args[2];

        if (args[0].equals(encrypt)) {
            encrypt(fileInput, fileOutput);
        } else if (args[0].equals(decrypt)) {
            decrypt(fileInput, fileOutput);
        }
    }

    public static void decrypt(String fileInput, String fileOutput) {
        try {
            FileInputStream fis = new FileInputStream(fileInput);
            FileOutputStream fos = new FileOutputStream(fileOutput);

            byte[] buffer = new byte[fis.available()];

            if (fis.available() > 0) {
                int count1 = fis.read(buffer);
                buffer[0]--;
                fos.write(buffer, 0, count1);
            }
            fos.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void encrypt(String fileInput, String fileOutput) {
        try {
            FileInputStream fis = new FileInputStream(fileInput);
            FileOutputStream fos = new FileOutputStream(fileOutput);

            byte[] buffer = new byte[fis.available()];

            if (fis.available() > 0) {
                int count1 = fis.read(buffer);
                buffer[0]++;
                fos.write(buffer, 0, count1);
            }
            fos.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
