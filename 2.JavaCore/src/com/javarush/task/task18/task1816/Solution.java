package com.javarush.task.task18.task1816;

/* 
Английские буквы
*/

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        HashMap<Character, Integer> alphabet = new HashMap<>();
//        String[] args = {"a.txt"};
        int counter = 0;

        for (char c = 'a'; c <= 'z'; c++) {
            alphabet.put(c, 0);
        }

        for (char c = 'A'; c <= 'Z'; c++) {
            alphabet.put(c, 0);
        }

        try {
            FileInputStream fis = new FileInputStream(args[0]);

            while (fis.available() > 0) {
                int key = fis.read();
                if (alphabet.containsKey((char)key)) {
                    Iterator<Map.Entry<Character, Integer>> hmiI = alphabet.entrySet().iterator();
                    while (hmiI.hasNext()) {
                        Map.Entry<Character, Integer> next = hmiI.next();
                        Character k = next.getKey();
                        Integer v = next.getValue();

                        if (k == key) {
                            alphabet.replace(k, ++v);
                        }
                    }
                }
            }
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Map.Entry<Character, Integer> entry : alphabet.entrySet()) {
            if (entry.getValue() != 0) {
                counter += entry.getValue();
            }
        }

        System.out.println(counter);
//        System.out.println(alphabet);
    }
}