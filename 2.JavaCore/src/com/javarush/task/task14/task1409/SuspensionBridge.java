package com.javarush.task.task14.task1409;

public class SuspensionBridge implements Bridge {
    private static int carsCount = 2;

    @Override
    public int getCarsCount() {
        return carsCount;
    }
}
