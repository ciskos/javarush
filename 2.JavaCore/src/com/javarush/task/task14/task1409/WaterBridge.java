package com.javarush.task.task14.task1409;

public class WaterBridge implements Bridge {
    private static int carsCount = 1;
    @Override
    public int getCarsCount() {
        return carsCount;
    }
}
