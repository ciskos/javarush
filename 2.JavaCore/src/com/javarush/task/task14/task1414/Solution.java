package com.javarush.task.task14.task1414;

/* 
MovieFactory
*/

import javax.sound.midi.Soundbank;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        //ввести с консоли несколько ключей (строк), пункт 7
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        Movie movie = null;
        String input = null;

        /*
8 Создать переменную movie класса Movie и для каждой введенной строки(ключа):
8.1 получить объект используя MovieFactory.getMovie и присвоить его переменной movie
8.2 вывести на экран movie.getClass().getSimpleName()
        */
        input = br.readLine();
//        while (input.equals("cartoon") || input.equals("thriller") || input.equals("soapOpera")) {
        while (true) {
            movie = MovieFactory.getMovie(input);
            if (movie == null) break;
            if (input.equals("cartoon") || input.equals("thriller") || input.equals("soapOpera")) {
                System.out.println(movie.getClass().getSimpleName());
                input = br.readLine();
            }
        }
    }

    static class MovieFactory {

        static Movie getMovie(String key) {
            Movie movie = null;

            //создание объекта SoapOpera (мыльная опера) для ключа "soapOpera"
            if ("soapOpera".equals(key)) {
                movie = new SoapOpera();
            } else if ("cartoon".equals(key)) {
                movie = new Cartoon();
            } else if ("thriller".equals(key)) {
                movie = new Thriller();
            } else {
                movie = null;
            }

            //напишите тут ваш код, пункты 5,6

            return movie;
        }
    }

    static abstract class Movie {
    }

    static class SoapOpera extends Movie {
    }

    //Напишите тут ваши классы, пункт 3
    public static class Cartoon extends Movie {

    }

    public static class Thriller extends Movie {

    }
}
