package com.javarush.task.task14.task1420;

/* 
НОД
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a1;
        int a2;
//        try {
            a1 = Integer.parseInt(reader.readLine());
            a2 = Integer.parseInt(reader.readLine());
            if(a1 <= 0 || a2 <= 0) throw new NumberFormatException("Неверный формат номера");
            System.out.println(nod(a1, a2));
//        }
//        catch (IOException | NumberFormatException e) {
//            e.printStackTrace();
//        }

    }
    public static int nod(int a, int b) {
        int divider = 1;
        int min;
        if(a < b) min = a;
        else min = b;
        for (int i = min; i > 0; i--) {
            if((a % i) == 0 && (b % i) == 0) {
                divider = i;
                break;
            }
        }
        return divider;
    }
}
