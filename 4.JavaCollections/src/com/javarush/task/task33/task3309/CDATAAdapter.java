package com.javarush.task.task33.task3309;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class CDATAAdapter extends XmlAdapter<String, String> {

    @Override
    public String marshal(String s) throws Exception {
        return "<![CDATA[" + s + "]]>";
    }

    @Override
    public String unmarshal(String s) throws Exception {
        return s;
    }
}
