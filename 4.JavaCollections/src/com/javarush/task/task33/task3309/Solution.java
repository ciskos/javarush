package com.javarush.task.task33.task3309;

import com.sun.xml.bind.marshaller.CharacterEscapeHandler;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.*;

/*
Комментарий внутри xml
*/
public class Solution {
    public static void main(String[] args) {
        First first = new First();
        String tagName = "second";
        String comment = "it's a comment";

        try {
            System.out.println(toXmlWithComment(first, tagName, comment));
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    public static String toXmlWithComment(Object obj, String tagName, String comment) throws JAXBException, ParserConfigurationException, IOException, SAXException, TransformerException {
//        Document document = objectToDocument(obj);
//        StringWriter writer = new StringWriter();
        StringWriter out = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(obj.getClass());
        Marshaller marshaller = context.createMarshaller();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(obj, document);
//        marshaller.setProperty("com.sun.xml.bind.marshaller.CharacterEscapeHandler", new CharacterEscapeHandler() {
//            @Override
//            public void escape(char[] chars, int start, int length, boolean isAttVal, Writer writer) throws IOException {
//                writer.write(chars, start, length);
//            }
//        });
//        marshaller.marshal(obj, writer);

//        Document document = builder.parse(new InputSource(new StringReader(writer.toString())));

//        document.setXmlStandalone(false);

//        addComments(tagName, comment, document);
//        NodeList nodeList = document.getElementsByTagName(tagName);

        checkCDATA(document);
        NodeList nodeList = document.getElementsByTagName(tagName);
        if (nodeList.getLength() > 0) {
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node n = nodeList.item(i);
                Comment node = document.createComment(comment);
                n.getParentNode().insertBefore(node, n);
            }
        }

//        StringWriter out = getXMLString(document);

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "no");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.transform(new DOMSource(document), new StreamResult(out));

        return out.toString();
    }

    // Костыльный метод содран
    public static void checkCDATA(Document document) {
        Node root = document.getDocumentElement();
        NodeList tagList = root.getChildNodes();

        for (int i = 0; i < tagList.getLength(); i++) {
            Node n = tagList.item(i);

            if (n.getFirstChild().getNodeType() == Node.TEXT_NODE) {
                Node currentNode = n.getFirstChild();

                if (currentNode.getTextContent().matches(".*[<>&\'\"].*")) {
                    String content = currentNode.getTextContent();
                    CDATASection cdataSection = document.createCDATASection(content);
                    n.replaceChild(cdataSection, currentNode);
                }
            } else {
                if (n.hasChildNodes()) {
                    NodeList subTags = n.getChildNodes();
                    checkCDATA(document);
                }
            }
        }
    }


//    public static String toXmlWithComment(Object obj, String tagName, String comment) throws JAXBException, ParserConfigurationException, IOException, SAXException, TransformerException {
//        Document document = objectToDocument(obj);
//
//        addComments(tagName, comment, document);
//
//        StringWriter out = getXMLString(document);
//
//        return out.toString();
//    }

    public static StringWriter getXMLString(Document document) throws TransformerException {
        StringWriter out = new StringWriter();
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
//        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
//        transformer.setOutputProperty(OutputKeys.STANDALONE, "no");
        transformer.transform(new DOMSource(document), new StreamResult(out));

        return out;
    }

    public static void addComments(String tagName, String comment, Document document) {
        NodeList nodeList = document.getElementsByTagName(tagName);

        if (nodeList.getLength() > 0) {
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node n = nodeList.item(i);
                Comment node = document.createComment(comment);
                n.getParentNode().insertBefore(node, n);
            }
        }
    }

    public static Document objectToDocument(Object obj) throws JAXBException, ParserConfigurationException, SAXException, IOException {
        StringWriter writer = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(obj.getClass());
        Marshaller marshaller = context.createMarshaller();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

//        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.setProperty("com.sun.xml.bind.marshaller.CharacterEscapeHandler", new CharacterEscapeHandler() {
            @Override
            public void escape(char[] chars, int start, int length, boolean isAttVal, Writer writer) throws IOException {
                writer.write(chars, start, length);
            }
        });
        marshaller.marshal(obj, writer);

        Document document = builder.parse(new InputSource(new StringReader(writer.toString())));

        document.setXmlStandalone(false);

        return document;
    }
}
