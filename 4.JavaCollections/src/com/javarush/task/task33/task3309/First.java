package com.javarush.task.task33.task3309;

import com.sun.xml.txw2.annotation.XmlCDATA;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "first")
@XmlType(propOrder = {"one", "two", "three", "four"})
@XmlAccessorType(XmlAccessType.FIELD)
public class First {
    @XmlElement(name = "second")
    private String one = "some string";
    @XmlElement(name = "second")
    private String two = "some string";
    @XmlElement(name = "second")
    private String four = "";
    //    @XmlElement(name = "second")
//    @XmlJavaTypeAdapter(value = CDATAAdapter.class)
    @XmlElement(name = "second")
//    private String three = "<![CDATA[need CDATA because of < and >]]>";
    private String three = "need CDATA because of < and >";

    public String getOne() {
        return one;
    }

    public void setOne(String one) {
        this.one = one;
    }

    //    @XmlElement(name = "second")
    public String getTwo() {
        return two;
    }

    public void setTwo(String two) {
        this.two = two;
    }

    //    @XmlElement(name = "second")
    public String getThree() {
        return three;
    }

    public void setThree(String three) {
        this.three = three;
    }

    //    @XmlElement(name = "second")
    public String getFour() {
        return four;
    }

    public void setFour(String four) {
        this.four = four;
    }
}
