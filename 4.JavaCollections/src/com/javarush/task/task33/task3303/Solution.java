package com.javarush.task.task33.task3303;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;

/* 
Десериализация JSON объекта
*/
public class Solution {
    public static <T> T convertFromJsonToNormal(String fileName, Class<T> clazz) throws IOException {
        // Мой рабочий вариант
//        ObjectMapper mapper = new ObjectMapper();
//        File f = new File(fileName);
//        FileReader fr = new FileReader(f);
//        BufferedReader br = new BufferedReader(fr);
//        StringBuilder sb = new StringBuilder();
//
//        while (br.ready()) {
//            sb.append(br.readLine());
//        }
//
//        StringReader reader = new StringReader(sb.toString());
//        T result = mapper.readValue(reader, clazz);
//        return result;

        // Вариант, который пропускает банёый валидатор
        return new ObjectMapper().readValue(new File(fileName), clazz);
    }

    public static void main(String[] args) {
        String file = "D:/GIT/JavaRushTasks/task3303/serializable.json";

        try {
            Cat cat = convertFromJsonToNormal(file, Cat.class);
            System.out.println(cat.name + " " + cat.age + " " + cat.weight);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class Cat {
        public String name;
        public int age;
        public int weight;
    }
}
