package com.javarush.task.task33.task3308;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
//@XmlType(name = "shop")
public class Shop {
//    @XmlElementWrapper(nillable = true)
    public Goods goods;
    public int count;
    public double profit;
    public String[] secretData = new String[5];

//    @XmlRootElement
    public static class Goods {
//        @XmlElement
        public List<String> names = new ArrayList<>();
    }
}
