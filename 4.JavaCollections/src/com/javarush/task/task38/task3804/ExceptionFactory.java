package com.javarush.task.task38.task3804;

public class ExceptionFactory {
    public static Throwable execute(Enum exception) {
        if (exception == null) return new IllegalArgumentException();

        String firstLetter = exception.name().substring(0,1);
        String subMessage = exception.name().substring(1).replaceAll("_", " ").toLowerCase();
        String message = firstLetter + subMessage;

        if (exception instanceof ApplicationExceptionMessage) {
            return new Exception(message);
        } else if (exception instanceof DatabaseExceptionMessage) {
            return new RuntimeException(message);
        } else if (exception instanceof UserExceptionMessage) {
            return new Error(message);
        }

        return new IllegalArgumentException();
    }
}
