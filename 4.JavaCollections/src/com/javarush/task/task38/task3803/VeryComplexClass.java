package com.javarush.task.task38.task3803;

/* 
Runtime исключения (unchecked exception)
*/

import java.util.ArrayList;
import java.util.List;

public class VeryComplexClass {
    public void methodThrowsClassCastException() {
        Object o = 1;
        String s = (String)o;
    }

    public void methodThrowsNullPointerException() {
        List<String> ls = null;
        ls.add("s");
    }

    public static void main(String[] args) {
//        VeryComplexClass v = new VeryComplexClass();
//        v.methodThrowsNullPointerException();
//        v.methodThrowsClassCastException();
    }
}
