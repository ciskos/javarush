package com.javarush.task.task34.task3413;

/* 
Кеш на основании SoftReference
*/

public class Solution {
    public static void main(String[] args) {
        SoftCache cache = new SoftCache();

        for (long i = 0; i < 2_500_000; i++) {
//        for (long i = 0; i < 2; i++) {
            cache.put(i, new AnyObject(i, null, null));
        }

//        for (long i = 0; i < 2; i++) {
//            System.out.println(cache.get(i));
//        }

//        System.out.println(cache.get(2L));
//        cache.put(2L, new AnyObject(2L, null, null));
//        System.out.println(cache.get(2L));
//        cache.put(2L, new AnyObject(2L, null, null));
//        System.out.println(cache.get(2L));
//        System.out.println(cache.remove(2L));
//        System.out.println(cache.get(2L));
    }
}