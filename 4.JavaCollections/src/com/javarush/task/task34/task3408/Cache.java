package com.javarush.task.task34.task3408;

import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.WeakHashMap;

public class Cache<K, V> {
    /*
    * 1. Поле cache должно быть инициализировано объектом типа WeakHashMap.
    * */
    /*
    * 1. Выбери правильный тип для поля cache. Map<K, V> cache должен хранить ключи,
    * на которые есть активные ссылки.
    * */
    private Map<K, V> cache = new WeakHashMap<K,V>();   //TODO add your code here

    /*
    * 2. Метод getByKey должен возвращать объект из кеша.
    * 3. Метод getByKey должен добавлять объект в кеш если его там нет.
    * */
    /*
    * 2. Реализуй логику метода getByKey:
    * 2.1. Верни объект из cache для ключа key.
    * 2.2. Если объекта не существует в кэше, то добавьте в кэш новый экземпляр используя рефлексию,
    * см. пункт а).
    * */
    public V getByKey(K key, Class<V> clazz) throws Exception {
        //TODO add your code here
        V value = cache.get(key);

        if (value == null) {
            Constructor<V> constructor = clazz.getDeclaredConstructor(key.getClass());
            value = constructor.newInstance(key);

            put(value);

            return value;
        }

        return value;
    }

    /*
    * 4. Метод put должен извлекать из переданного объекта ключ и добавлять в кеш пару <key, obj>.
    * 5. Метод put должен возвращать true, если он отработал корректно, иначе false.
    * */
    /*
    * 3. Реализуй логику метода put:
    * 3.1. Используя рефлексию получи ссылку на метод, описанный в пункте б).
    * 3.2. Используя рефлексию разреши к нему доступ.
    * 3.3. Используя рефлексию вызови метод getKey у объекта obj, таким образом ты получишь ключ key.
    * 3.4. Добавь в кэш пару <key, obj>.
    * 3.5. Верни true, если метод отработал корректно, false в противном случае. Исключения игнорируй.
    * */
    public boolean put(V obj) {
        //TODO add your code here
        K key = null;

        try {
            Method method = obj.getClass().getDeclaredMethod("getKey");
            method.setAccessible(true);
            key = (K) method.invoke(obj);

        } catch (Exception ignore) {
            return false;
        }

        cache.put(key, obj);

        return true;
    }

    public int size() {
        return cache.size();
    }
}
