package com.javarush.task.task34.task3412;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/* 
Добавление логирования в класс
*/

public class Solution {
    private static final Logger logger = LoggerFactory.getLogger(Solution.class);

    private int value1;
    private String value2;
    private Date value3;

    public Solution(int value1, String value2, Date value3) {
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        logger.debug("value1, value2, value3 changed " + this.value1 + " " + this.value2 + " " + this.value3);
    }

    public static void main(String[] args) {

    }

    public void calculateAndSetValue3(long value) {
        // 1 - 4. Добавь логирование уровня trace четыре раза.
        logger.trace("entering method calculateAndSetValue3");
        value -= 133;
        if (value > Integer.MAX_VALUE) {
            value1 = (int) (value / Integer.MAX_VALUE);
            logger.debug("value1 changed " + value1);
        } else {
            value1 = (int) value;
            logger.debug("value1 changed " + value1);
        }
    }

    public void printString() {
        // 2 - 4. Добавь логирование уровня trace четыре раза.
        logger.trace("entering method printString");
        if (value2 != null) {
            System.out.println(value2.length());
        }
    }

    public void printDateAsLong() {
        // 3 - 4. Добавь логирование уровня trace четыре раза.
        logger.trace("entering method printDateAsLong");
        if (value3 != null) {
            System.out.println(value3.getTime());
        }
    }

    public void divide(int number1, int number2) {
        // 4 - 4. Добавь логирование уровня trace четыре раза.
        logger.trace("entering method divide");
        try {
            System.out.println(number1 / number2);
        } catch (ArithmeticException e) {
            // 1 - 2. Добавь логирование уровня error один раз.
            logger.error("error in method divide");
        }
    }

    public void setValue1(int value1) {
        this.value1 = value1;
        logger.debug("value1 changed " + value1);
    }

    public void setValue2(String value2) {
        this.value2 = value2;
        logger.debug("value2 changed " + value2);
    }

    public void setValue3(Date value3) {
        this.value3 = value3;
        logger.debug("value3 changed " + value3);
    }
}
