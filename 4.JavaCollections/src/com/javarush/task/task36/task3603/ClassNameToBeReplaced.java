package com.javarush.task.task36.task3603;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ClassNameToBeReplaced<T> extends CopyOnWriteArrayList<T> {

    public void methodNameToBeReplaced(List<T> collection) {
        addAllAbsent(collection);
    }
}
