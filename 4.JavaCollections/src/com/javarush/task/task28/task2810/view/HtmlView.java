package com.javarush.task.task28.task2810.view;

import com.javarush.task.task28.task2810.Controller;
import com.javarush.task.task28.task2810.vo.Vacancy;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.print.Doc;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HtmlView implements View {

    private Controller controller;
    private final String filePath = "./4.JavaCollections/src/" + this.getClass().getPackage().getName().replace(".", "/") + "/vacancies.html";

    @Override
    public void update(List<Vacancy> vacancies) {
        updateFile(getUpdatedFileContent(vacancies));
    }

    @Override
    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void userCitySelectEmulationMethod() {
        controller.onCitySelect("Odessa");
    }

    private String getUpdatedFileContent(List<Vacancy> vacancies) {
//    public String getUpdatedFileContent(List<Vacancy> vacancies) {
//        StringBuilder sb = new StringBuilder();
        Document document = null;

        try {
            document = getDocument();
            Element template = document.getElementsByClass("template").first();
            Element clearedTemplate = template.clone();
            Elements elements = document.getElementsByClass("vacancy");

            clearedTemplate.removeClass("template").removeAttr("style");

//            System.out.println("before " + document);
            for (Element e : elements) {
                if (e.hasClass("template")) continue;
                e.remove();
            }
//            System.out.println("after " + document);

            for (Vacancy v : vacancies) {
                Element clearedTemplateCopy = clearedTemplate.clone();
                String title = v.getTitle();
                String url = v.getUrl();
                String city = v.getCity();
                String companyName = v.getCompanyName();
                String salary = v.getSalary();

                clearedTemplateCopy.removeClass("template").removeAttr("style");
                // Добавить title
                clearedTemplateCopy
                        .getElementsByClass("title").first()
                        .getElementsByTag("a").first()
                        .appendText(title);
                // Добавить url
                clearedTemplateCopy
                        .getElementsByClass("title").first()
                        .getElementsByTag("a").first()
                        .attr("href", url);
                // Добавить city
                clearedTemplateCopy
                        .getElementsByClass("city").first()
                        .appendText(city);
                // Добавить companyName
                clearedTemplateCopy
                        .getElementsByClass("companyName").first()
                        .appendText(companyName);
                // Добавить salary
                clearedTemplateCopy
                        .getElementsByClass("salary").first()
                        .appendText(salary);

//                sb.append(clearedTemplate);
//                System.out.println("before [" + clearedTemplate + "]");
//                clearedTemplate.outerHtml();
//                System.out.println("after [" + clearedTemplate + "]");

                // Добавить узел к документу
//                clearedTemplate.appendChild(clearedTemplate);
//                document.
//                        .getElementsByTag("table").first()
//                        .appendChild(clearedTemplate);
//                System.out.println(clearedTemplate.outerHtml());
                template.before(clearedTemplateCopy.outerHtml());
            }
//            document
//                    .getElementsByTag("table").first()
//                    .appendChild(template);

//            System.out.println("template " + template);
//            System.out.println("clearedTemplate " + clearedTemplate);
//            System.out.println("elements " + elements);
//            System.out.println("document " + document);
//            System.out.println("sb " + sb);
        } catch (IOException e) {
            e.printStackTrace();
            return "Some exception occurred";
        }

        return document.html();
    }

    private void updateFile(String file) {
        try (FileWriter fileWriter = new FileWriter(filePath);) {
            fileWriter.write(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected Document getDocument() throws IOException {
        return Jsoup.parse(new File(filePath), "UTF-8");
    }

}
