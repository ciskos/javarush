package com.javarush.task.task28.task2810.model;

import com.javarush.task.task28.task2810.vo.Vacancy;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HHStrategy implements Strategy {
    private static final String URL_FORMAT="http://hh.ru/search/vacancy?text=java+%s&page=%d";

    @Override
    public List<Vacancy> getVacancies(String searchString) {
        List<Vacancy> vacancies = new ArrayList<>();
        int currentPage = 0;

        try {
//            Document d = getDocument("", currentPage);
//            Elements pages = d.getElementsByAttributeValue("data-qa", "pager-page");
//            elements = d.getElementsByAttributeValue("data-qa", "vacancy-serp__vacancy");

            while (true) {
//                System.out.println("before " + currentPage);
                Document d = getDocument(searchString, currentPage++);
//                String page = d.getElementsByAttributeValue("data-qa", "pager-page").first().text();
//                System.out.println("after " + currentPage);
//                System.out.println("page " + page);
                Elements elements = d.getElementsByAttributeValue("data-qa", "vacancy-serp__vacancy");

                if (elements.size() == 0) break;

                for (Element element : elements) {
                    String title = element
                            .getElementsByAttributeValue("data-qa", "vacancy-serp__vacancy-title")
                            .text();
                    String url = element
                            .getElementsByAttributeValue("data-qa", "vacancy-serp__vacancy-title")
                            .attr("href");
                    String city = element
                            .getElementsByAttributeValue("data-qa", "vacancy-serp__vacancy-address")
                            .text();
                    String companyName = element
                            .getElementsByAttributeValue("data-qa", "vacancy-serp__vacancy-employer")
                            .text();
                    String salary = element
                            .getElementsByAttributeValue("data-qa", "vacancy-serp__vacancy-compensation")
                            .text();

                    Vacancy vacancy = new Vacancy();
                    vacancy.setTitle(title);
                    vacancy.setUrl(url);
                    vacancy.setCity(city);
                    vacancy.setCompanyName(companyName);
                    vacancy.setSiteName(URL_FORMAT);
                    vacancy.setSalary(salary);

                    vacancies.add(vacancy);
//                    System.out.println("title " + vacancy.getTitle());
//                    System.out.println("url " + vacancy.getUrl());
//                    System.out.println("city " + vacancy.getCity());
//                    System.out.println("companyName " + vacancy.getCompanyName());
//                    System.out.println("siteName " + vacancy.getSiteName());
//                    System.out.println("salary " + vacancy.getSalary());
    //                System.out.println(element);
                }
            }
        } catch (IOException e) {
//            e.printStackTrace();
        }

        return vacancies;
    }

    protected Document getDocument(String searchString, int page) throws IOException {
//        return Jsoup.connect("http://javarush.ru/testdata/big28data.html")
//        System.out.println(String.format(URL_FORMAT, searchString, page));
        return Jsoup.connect(String.format(URL_FORMAT, searchString, page))
                .userAgent("Mozilla")
                .referrer("referrer")
                .get();
    }
}
