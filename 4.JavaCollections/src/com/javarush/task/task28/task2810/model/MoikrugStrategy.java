package com.javarush.task.task28.task2810.model;

import com.javarush.task.task28.task2810.vo.Vacancy;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MoikrugStrategy implements Strategy {
//    private static final String URL_FORMAT="http://hh.ru/search/vacancy?text=java+%s&page=%d";
//    private static final String URL_FORMAT="https://javarush.ru/testdata/big28data2.html";
    private static final String URL_FORMAT="https://moikrug.ru/vacancies?q=java+%s&page=%d";
    private final String SITE = "https://moikrug.ru";

    @Override
    public List<Vacancy> getVacancies(String searchString) {
        List<Vacancy> vacancies = new ArrayList<>();
        int currentPage = 0;

        try {
            while (true) {
                Document d = getDocument(searchString, currentPage++);
                Elements elements = d.getElementsByClass("job");

//                System.out.println(elements);

                if (elements.size() == 0) break;

                for (Element element : elements) {
                    String title = element
//                            .getElementsByClass("title")
                            .getElementsByAttributeValue("class", "title")//.first()
                            .text();
                    String url = SITE + element
//                            .getElementsByClass("title").first()
                            .getElementsByAttributeValue("class", "title").first()
                            .getElementsByTag("a").first()
                            .attr("href");
                    String city = element
//                            .getElementsByClass("meta").first()
//                            .getElementsByClass("location")//.first()
//                            .getElementsByTag("a")
                            .getElementsByAttributeValue("class", "location")//.first()
                            .text();
                    String companyName = element
//                            .getElementsByClass("companyName")//.first()
                            .getElementsByAttributeValue("class", "company_name")//.first()
//                            .html();
//                            .getElementsByTag("a")
                            .text();

//                    for (Node n : companyElement.childNodes()) {
//                        System.out.println("nodes " + n);
//                    }

                    String salary = element
//                            .getElementsByClass("salary").first()
//                            .getElementsByClass("count")
                            .getElementsByAttributeValue("class", "salary")//.first()
                            .text();

                    Vacancy vacancy = new Vacancy();
                    vacancy.setTitle(title);
                    vacancy.setUrl(url);
                    vacancy.setCity(city);
                    vacancy.setCompanyName(companyName);
                    vacancy.setSiteName(SITE);
                    vacancy.setSalary(salary);

                    vacancies.add(vacancy);
//                    System.out.println("title " + vacancy.getTitle());
//                    System.out.println("url " + vacancy.getUrl());
//                    System.out.println("city " + vacancy.getCity());
//                    System.out.println("companyName " + vacancy.getCompanyName());
//                    System.out.println("siteName " + vacancy.getSiteName());
//                    System.out.println("salary " + vacancy.getSalary());
//                    System.out.println("title " + title);
//                    System.out.println("url " + url);
//                    System.out.println("city " + city);
//                    System.out.println("companyName " + companyName);
//                    System.out.println("siteName " + site);
//                    System.out.println("salary " + salary);


//                System.out.println(element);
                }
            }
        } catch (IOException e) {
//            e.printStackTrace();
        }

        return vacancies;
    }

    protected Document getDocument(String searchString, int page) throws IOException {
        return Jsoup.connect(String.format(URL_FORMAT, searchString, page))
                .userAgent("Mozilla")
                .referrer("referrer")
                .get();
    }
}
