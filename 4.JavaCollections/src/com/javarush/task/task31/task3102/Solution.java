package com.javarush.task.task31.task3102;

import javax.print.attribute.standard.Fidelity;
import java.io.File;
import java.io.IOException;
import java.util.*;

/* 
Находим все файлы
*/
public class Solution {
    public static List<String> getFileTree(String root) throws IOException {
        File rootDir = new File(root);
        File[] listDir = rootDir.listFiles();
        List<String> files = new ArrayList<>();
        LinkedList<File> dirs = new LinkedList<>();

        Collections.addAll(dirs, listDir);

        Iterator<File> iF = dirs.iterator();

        while (iF.hasNext()) {
            File f = dirs.remove();

            if (f.isDirectory()) {
                Collections.addAll(dirs, f.listFiles());
            } else {
                files.add(f.getAbsolutePath());
            }
        }

        return files;
    }

    public static void main(String[] args) {
        try {
            System.out.println(getFileTree("D:/GIT/JavaRushTasks"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
