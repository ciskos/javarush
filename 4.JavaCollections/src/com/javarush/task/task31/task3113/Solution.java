package com.javarush.task.task31.task3113;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashSet;
import java.util.Set;

/* 
Что внутри папки?
*/
public class Solution extends SimpleFileVisitor<Path> {

//    private static Set<Path> countDirs = new HashSet<>();
    private static int countDirs = -1;
    private static int countFiles;
    private static int countSize;
    private static Path rootPath;

    public static void main(String[] args) throws IOException {
        Solution solution = new Solution();
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        rootPath = Paths.get(br.readLine());
//        rootPath = Paths.get("D:/GIT/JavaRushTasks");

        if (Files.isDirectory(rootPath)) {
            Files.walkFileTree(rootPath, solution);
            System.out.println("Всего папок - " + countDirs);
            System.out.println("Всего файлов - " + countFiles);
            System.out.println("Общий размер - " + countSize);
        } else {
            System.out.println(rootPath + " - не папка");
            return;
        }
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        countDirs++;
        return super.preVisitDirectory(dir, attrs);
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            countFiles++;
            countSize += attrs.size();

        return super.visitFile(file, attrs);
    }
}
