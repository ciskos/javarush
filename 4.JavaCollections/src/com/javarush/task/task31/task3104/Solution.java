package com.javarush.task.task31.task3104;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/* 
Поиск скрытых файлов
*/
public class Solution extends SimpleFileVisitor<Path> {
    private List<String> archived = new ArrayList<>();
    private List<String> failed = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        EnumSet<FileVisitOption> options = EnumSet.of(FileVisitOption.FOLLOW_LINKS);
        final Solution solution = new Solution();
        List<String> result = solution.getArchived();
        List<String> failed = solution.getFailed();

        Files.walkFileTree(Paths.get("D:/GIT/JavaRushTasks"), options, 20, solution);

        System.out.println("All archived files:");
        for (String path : result) {
            System.out.println("\t" + path);
        }

        System.out.println("All failed files:");
        for (String path : failed) {
            System.out.println("\t" + path);
        }
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        String zip = "zip";
        String rar = "rar";
        int dotIndex = file.getFileName().toString().lastIndexOf(".");
        String fileExtension = file.getFileName().toString().substring(dotIndex + 1);

        if (fileExtension.equals(zip) || fileExtension.equals(rar)) {
            archived.add(file.toString());
        }

        return super.visitFile(file, attrs);
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
        failed.add(file.toString());
//        return super.visitFileFailed(file, exc);
        return FileVisitResult.SKIP_SUBTREE;
    }

    public List<String> getArchived() {
        return archived;
    }

    public List<String> getFailed() {
        return failed;
    }
}
