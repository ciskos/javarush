package com.javarush.task.task31.task3112;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/* 
Загрузчик файлов
*/
public class Solution {

    public static void main(String[] args) throws IOException, URISyntaxException {
        Path passwords = downloadFile("https://javarush.ru/testdata/secretPasswords.txt", Paths.get("D:/GIT/JavaRushTasks/task3112"));

        for (String line : Files.readAllLines(passwords)) {
            System.out.println(line);
        }
    }

    public static Path downloadFile(String urlString, Path downloadDirectory) throws IOException, URISyntaxException {
        // implement this method
        URL url = new URL(urlString);
        InputStream is = url.openStream();
        String urlFileName = urlString.substring(urlString.lastIndexOf("/") + 1);
        Path outPath = Paths.get(downloadDirectory + "/" + urlFileName);
        Path tmpPath = Files.createTempFile("temp-", ".tmp");

        Files.copy(is, tmpPath, StandardCopyOption.REPLACE_EXISTING);
        Files.move(tmpPath, outPath, StandardCopyOption.REPLACE_EXISTING);

        is.close();

        return outPath;
    }
}
