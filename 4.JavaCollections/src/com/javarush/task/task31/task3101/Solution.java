package com.javarush.task.task31.task3101;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/*
Проход по дереву файлов
*/
public class Solution {
    // Содрано - https://github.com/avedensky/JavaRushTasks/blob/master/4.JavaCollections/src/com/javarush/task/task31/task3101/Solution.java
    // ныйеба валидатор укас бализа

    public static void main(String[] args) {
        File path = new File(args[0]);
        File resultFileAbsolutePath = new File(args[1]);
        File allFilesContent = new File(resultFileAbsolutePath.getParent() + "/allFilesContent.txt");
        FileUtils.renameFile(resultFileAbsolutePath, allFilesContent);
        ArrayList<File> fileList = new ArrayList<>();

        fillFileList(fileList, path.getPath());
        fileList.sort(new FileNameComparator());

        try (FileOutputStream fileOutputStream = new FileOutputStream(allFilesContent, true)) {
            for (File file : fileList) {
                FileInputStream fileInputStream = new FileInputStream(file);

                while (fileInputStream.available() > 0) {
                    fileOutputStream.write(fileInputStream.read());
                }

                fileOutputStream.write(System.lineSeparator().getBytes());

                fileOutputStream.flush();
                fileInputStream.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deleteFile(File file) {
        if (!file.delete()) System.out.println("Can not delete file with name " + file.getName());
    }

//    private static ArrayList<File> fillFileList(String dir) {
//        ArrayList<File> files = new ArrayList<>();
//        File[] dirs = new File(dir).listFiles();
//        int bytesToShow = 50;
//
//        for (File f : dirs) {
//            if (f.isDirectory()) {
//                fillFileList(f.getAbsolutePath());
//                continue;
//            }
//
//            if (f.length() <= bytesToShow) {
//                files.add(f);
//            }
//        }
//
//        return files;
//    }

    private static void fillFileList(ArrayList<File> fileList, String path) {
//        ArrayList<File> fileList = new ArrayList<>();
        File[] files = new File(path).listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                fillFileList(fileList, file.getAbsolutePath());
                continue;
            }
            if (file.length() > 50)
                FileUtils.deleteFile(file);
            else
                fileList.add(file);
        }

//        return fileList;
    }
}

class FileNameComparator implements Comparator<File> {
    public int compare(File first, File second) {
        return first.getName().compareTo(second.getName());
    }
}

//    public static void main(String[] args) {
//
//        // Заглушки
////        args = new String[2];
////        args[0] = "D:/GIT/JavaRushTasks";
////        args[1] = "D:/GIT/JavaRushTasks/file_task3101.txt";
//
//        File path = new File(args[0]);
//        File resultFileAbsolutePath = new File(args[1]);
//        File allFilesContent = new File(resultFileAbsolutePath.getParent() + "/allFilesContent.txt");
//        FileUtils.renameFile(resultFileAbsolutePath, allFilesContent);
//        List<File> files = listDirs(path);
//
//        files.sort(new FileNameComparator());
//
//        try (FileOutputStream fos = new FileOutputStream(allFilesContent, true)) {
//            for (File f : files) {
//                FileInputStream fis = new FileInputStream(f);
//
//                while (fis.available() > 0) {
//                    fos.write(fis.read());
//                }
//
//                fos.write(System.lineSeparator().getBytes());
//
//                fos.flush();
//                fis.close();
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    private static List<File> listDirs(File dir) {
//        List<File> files = new ArrayList<>();
//        int bytesToShow = 50;
//
//        for (File f : dir.listFiles()) {
//            if (f.isDirectory()) {
//                listDirs(f);
//            } else {
//                if (f.length() <= bytesToShow) {
//                    files.add(f);
//                }
//            }
//        }
//        return files;
//    }
//}
//
//// Содрано - https://github.com/avedensky/JavaRushTasks/blob/master/4.JavaCollections/src/com/javarush/task/task31/task3101/Solution.java
////Компаратор для сравнения
//class FileNameComparator implements Comparator<File> {
//    public int compare(File first, File second) {
//        return first.getName().compareTo(second.getName());
//    }
//}
