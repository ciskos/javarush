package com.javarush.task.task31.task3111;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SearchFileVisitor extends SimpleFileVisitor<Path> {

    // 1
    private String partOfName;
    private String partOfContent;
    private int minSize;
    private int maxSize;
    // 2
    private List<Path> foundFiles = new ArrayList<>();

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        byte[] content = Files.readAllBytes(file); // размер файла: content.length
        String c = new String(content);
        boolean isPartOfName = true;
        boolean isPartOfContent = true;
        boolean isMinSize = true;
        boolean isMaxSize = true;

        // 3, 4, 5, 6, 7
        if (partOfName != null) {
            isPartOfName = file.toString().contains(partOfName);
        }

        if (partOfContent != null) {
            isPartOfContent = c.contains(partOfContent);
        }

        if (minSize != 0) {
            isMinSize = content.length > minSize;
        }

        if (maxSize != 0) {
            isMaxSize = content.length < maxSize;
        }

        if (isPartOfName && isPartOfContent && isMinSize && isMaxSize) {
            foundFiles.add(file);
        }

        return super.visitFile(file, attrs);
    }

    // 1
    public void setPartOfName(String partOfName) {
        this.partOfName = partOfName;
    }

    // 1
    public void setPartOfContent(String partOfContent) {
        this.partOfContent = partOfContent;
    }

    // 1
    public void setMinSize(int minSize) {
        this.minSize = minSize;
    }

    // 1
    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    // 2
    public List<Path> getFoundFiles() {
        return foundFiles;
    }
}
