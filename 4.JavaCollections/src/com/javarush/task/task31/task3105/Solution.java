package com.javarush.task.task31.task3105;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/* 
Добавление файла в архив
*/
public class Solution {
    private static List<String> dirEntries = new ArrayList<>();
    //    private static Map<String, LinkedList<Integer>> zipEntries = new HashMap<>();
    private static Map<String, ByteArrayOutputStream> zipEntries = new HashMap<>();

    public static void main(String[] args) throws IOException {
//        args = new String[]{"D:/GIT/JavaRushTasks/task3105/result.mp3", "D:/GIT/JavaRushTasks/task3105/test.zip"};

        String fileName = args[0];
        String zipArchive = args[1];
        String file = fileName.substring(fileName.lastIndexOf("/") + 1);
        String newDir = "new/";
        Path fileToAdd = Paths.get(fileName);

        // 1
        // Читаем содержимое архива и переносим в HashMap
        readZipToMemory(zipArchive);

        // 2
        FileOutputStream fos = new FileOutputStream(zipArchive);
        ZipOutputStream zipOut = new ZipOutputStream(fos);

        // Создать директорию new
        zipOut.putNextEntry(new ZipEntry(newDir));
        zipOut.closeEntry();

        // 3
        // Добавить новый файл в папку new в архиве
        File addFile = new File(fileName);
        zipOut.putNextEntry(new ZipEntry(newDir + addFile.getName()));

        Files.copy(fileToAdd, zipOut);

        zipOut.closeEntry();

        // 4
        // Записать прочитанное из архива содержимое назад в архив
        for (Map.Entry<String, ByteArrayOutputStream> zipOutEntry : zipEntries.entrySet()) {
            String zipKey = zipOutEntry.getKey();
            ByteArrayOutputStream zipValue = zipOutEntry.getValue();

            if (zipKey.endsWith(file)) continue;
            if (zipKey.endsWith(newDir)) continue;

            zipOut.putNextEntry(new ZipEntry(zipKey));
            zipOut.write(zipValue.toByteArray());

            zipOut.closeEntry();
        }
        zipOut.close();
        fos.close();


//        System.out.println(zipEntries);

    }

    private static void readZipToMemory(String zipArchive) throws IOException {
        FileInputStream fis = new FileInputStream(zipArchive);
        ZipInputStream zis = new ZipInputStream(fis);
        ZipEntry zipInEntry = zis.getNextEntry();

        while (zipInEntry != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024 * 32];
            int bufferLength;

            while ((bufferLength = zis.read(buffer)) > 0) {
                baos.write(buffer, 0, bufferLength);
            }

            zipEntries.put(zipInEntry.getName(), baos);
            zipInEntry = zis.getNextEntry();

            baos.close();
        }

        zis.closeEntry();
        zis.close();
        fis.close();
    }
}
