package com.javarush.task.task31.task3106;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/*
Разархивируем файл
*/
public class Solution {
    private static int countParts = 1;
    private static ByteArrayOutputStream joinedParts = new ByteArrayOutputStream();

    public static void main(String[] args) {
        // Тестовые данные
/*        String path = "D:/GIT/JavaRushTasks/task3106/test.txt";
        String archive = "D:/GIT/JavaRushTasks/task3106/test.zip";
        String joinedArchive = "D:/GIT/JavaRushTasks/task3106/testJoined.zip";
        List<String> parts = new ArrayList<>();

        parts.add("D:/GIT/JavaRushTasks/task3106/test.zip.001");
        parts.add("D:/GIT/JavaRushTasks/task3106/test.zip.002");
        parts.add("D:/GIT/JavaRushTasks/task3106/test.zip.005");
        parts.add("D:/GIT/JavaRushTasks/task3106/test.zip.003");
        parts.add("D:/GIT/JavaRushTasks/task3106/test.zip.004");

        args = new String[]{
                "D:/GIT/JavaRushTasks/task3106/result.mp3"
                , "D:/GIT/JavaRushTasks/task3106/test.zip.001"
                , "D:/GIT/JavaRushTasks/task3106/test.zip.002"
                , "D:/GIT/JavaRushTasks/task3106/test.zip.005"
                , "D:/GIT/JavaRushTasks/task3106/test.zip.003"
                , "D:/GIT/JavaRushTasks/task3106/test.zip.004"
        };*/

        // Файл в который записывается результат разархивации
        String resultFileName = args[0];
        // Список частей архива
        List<String> fileNamePart = new ArrayList<>();
        List<InputStream> filePartsInputStreams = new ArrayList<>();

        // Добавить все имеющиеся части архива в список
        for (int i = 1; i < args.length; i++) {
            fileNamePart.add(args[i]);
        }

        // Отсортировать список входящих путей(на всякий случай)
        Collections.sort(fileNamePart);

        try {
            for (String f : fileNamePart) {
                filePartsInputStreams.add(new FileInputStream(f));
            }

            SequenceInputStream sis = new SequenceInputStream(Collections.enumeration(filePartsInputStreams));
            ZipInputStream zis = new ZipInputStream(sis);
            ZipEntry zipEntry = zis.getNextEntry();
            FileOutputStream fos = new FileOutputStream(resultFileName, true);
            byte[] buffer = new byte[1024 * 10];
            int bufferLength;

            while (zipEntry != null) {
                while ((bufferLength = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, bufferLength);
                }

                zipEntry = zis.getNextEntry();
            }

            zis.closeEntry();
            zis.close();
            fos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void unzipFromTmpToFile(String resultFileName, Path tmp) throws IOException {
        FileInputStream fis = new FileInputStream(tmp.toFile());
        ZipInputStream zis = new ZipInputStream(fis);
        ZipEntry zipEntry = zis.getNextEntry();
        FileOutputStream fos = new FileOutputStream(resultFileName);
        byte[] buffer = new byte[1024 * 10];
        int bufferLength;

        while (zipEntry != null) {
            while ((bufferLength = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, bufferLength);
            }

            zipEntry = zis.getNextEntry();
        }

        zis.closeEntry();
        zis.close();
        fis.close();
        fos.close();
    }

    private static void joinArchivePartsOnDisk(List<String> fileNamePart, Path tmp) throws IOException {
        FileOutputStream fos = new FileOutputStream(tmp.toFile(), true);

        for (String s : fileNamePart) {
            byte[] buffer = new byte[1024 * 10];
            int bufferLength;

            FileInputStream fis = new FileInputStream(s);

            while ((bufferLength = fis.read(buffer)) > 0) {
                fos.write(buffer, 0, bufferLength);
            }

            fis.close();
        }

        fos.close();
    }

    private static void joinArchivePartsInMemory(List<String> fileNamePart) throws IOException {
        for (String s : fileNamePart) {
            byte[] buffer = new byte[1024 * 10];
            int bufferLength;

            FileInputStream fis = new FileInputStream(s);

            while ((bufferLength = fis.read(buffer)) > 0) {
                joinedParts.write(buffer, 0, bufferLength);
            }

            fis.close();
        }
    }

    private static void joinArchiveParts(String joinedArchive, List<String> parts) throws IOException {
        FileOutputStream fos = new FileOutputStream(joinedArchive);

        Collections.sort(parts);

        for (String s : parts) {
            FileInputStream fis = new FileInputStream(s);
            byte[] buffer = new byte[1024 * 10];
            int bufferLength;

            while ((bufferLength = fis.read(buffer)) > 0) {
                fos.write(buffer);
            }

            fis.close();
        }

        fos.close();
    }

    private static void splitArchive(String archive) throws IOException {
        FileInputStream fis = new FileInputStream(archive);

        byte[] buffer = new byte[1024 * 10];
        int bufferLength;
        Path f = Paths.get(archive);
        String file = f.getFileName().toString();
        String dir = f.getParent().toString();

        while ((bufferLength = fis.read(buffer)) > 0) {
            String partFile = dir + "/" + file + ".00" + countParts++;
            FileOutputStream fos = new FileOutputStream(partFile);

            fos.write(buffer, 0, bufferLength);

            fos.close();
        }

        fis.close();
    }

    private static void zipFile(String path, String archive) throws IOException {
        FileInputStream fis = new FileInputStream(path);
        BufferedInputStream bis = new BufferedInputStream(fis);
        FileOutputStream fos = new FileOutputStream(archive);
        ZipOutputStream zos = new ZipOutputStream(fos);

        zos.putNextEntry(new ZipEntry("test.txt"));

        byte[] buffer = new byte[1024 * 10];
        int bufferLength;

        while ((bufferLength = bis.read(buffer)) > 0) {
            zos.write(buffer, 0, bufferLength);
        }

        zos.closeEntry();
        zos.close();
        fos.close();
        bis.close();
        fis.close();
    }

    private static void createFileToArchive(String path) throws IOException {
        byte[] buffer = new byte[1024 * 50_000];

        for (int i = 0; i < buffer.length; i++) {
            buffer[i] = 1;
        }

        FileOutputStream fos = new FileOutputStream(path);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        baos.write(buffer, 0, buffer.length);

        fos.write(baos.toByteArray());

        baos.close();
        fos.close();
    }
}
