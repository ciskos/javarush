package com.javarush.task.task40.task4002;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
//import java.net.http.HttpClient;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/* 
Опять POST, а не GET
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Solution solution = new Solution();
        solution.sendPost("http://requestb.in/1h4qhvv1", "name=zapp&mood=good&locale=&id=777");
    }

    public void sendPost(String url, String urlParameters) throws Exception {
        URL u = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) u.openConnection();

        // 4. Метод sendPost должен использовать метод getHttpClient для получения HttpClient.
        HttpClient client = getHttpClient();
//        HttpGet request = new HttpGet(url);
        // 1. Метод sendPost должен создавать объект типа HttpPost с параметром url.
        HttpPost request = new HttpPost(url);

        List<NameValuePair> list = Arrays.stream(urlParameters.split("&"))
                                    .map(s -> new BasicNameValuePair(s.substring(0, s.indexOf("="))
                                                                        , s.substring(s.indexOf("=") + 1))
                                                                    )
                                    .collect(Collectors.toList());

        request.addHeader("User-Agent", "Mozilla/5.0");
        // 2. Метод sendPost должен вызвать метод setEntity у созданного объекта типа HttpPost.
        request.setEntity(new UrlEncodedFormEntity(list));

        HttpResponse response = client.execute(request);

        System.out.println("Response Code: " + response.getStatusLine().getStatusCode());

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String responseLine;
        while ((responseLine = bufferedReader.readLine()) != null) {
            result.append(responseLine);
        }

        // 3. В OutputStream соединения должны быть записаны переданные в метод sendPost параметры.
        connection.getOutputStream().write(urlParameters.getBytes());

        System.out.println("Response: " + result.toString());
    }

    protected HttpClient getHttpClient() {
        return HttpClientBuilder.create().build();
    }
}
