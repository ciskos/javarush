package com.javarush.task.task40.task4011;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/* 
Свойства URL
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        decodeURLString("https://www.amrood.com/index.htm?language=en#j2se");
    }

    public static void decodeURLString(String s) throws MalformedURLException {
//        URL url = null;
        try {
            URL url = new URL(s);
            System.out.printf("%s\n", url.getProtocol());
            System.out.printf("%s\n", url.getAuthority());
            System.out.printf("%s\n", url.getFile());
            System.out.printf("%s\n", url.getHost());
            System.out.printf("%s\n", url.getPath());
            System.out.printf("%s\n", url.getPort());
            System.out.printf("%s\n", url.getDefaultPort());
            System.out.printf("%s\n", url.getQuery());
            System.out.printf("%s\n", url.getRef());
        } catch (MalformedURLException e) {
            System.out.printf("Parameter %s is not a valid URL\n", s);
        }

    }
}

