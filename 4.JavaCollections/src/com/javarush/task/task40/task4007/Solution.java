package com.javarush.task.task40.task4007;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/* 
Работа с датами
*/

public class Solution {
    public static void main(String[] args) {
        printDate("21.4.2014 15:56:45");
        System.out.println();
        printDate("21.4.2014");
        System.out.println();
        printDate("17:33:40");

//        System.out.println();
//        printDate("13.1.2019");

    }

    public static void printDate(String date) {
        //напишите тут ваш код
        Calendar calendar = Calendar.getInstance();
        DateFormat formatter = null;
        String result = null;

        if (date.contains(" ")) {
            formatter = new SimpleDateFormat("dd.M.yyyy HH:mm:ss");
        } else if (date.contains(".")) {
            formatter = new SimpleDateFormat("dd.M.yyyy");
        } else if (date.contains(":")) {
            formatter = new SimpleDateFormat("HH:mm:ss");
        }

        try {
            calendar.setTime(formatter.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date.contains(" ")) {
            result = String.format("День: %s\n"
                            + "День недели: %s\n"
                            + "День месяца: %s\n"
                            + "День года: %s\n"
                            + "Неделя месяца: %s\n"
                            + "Неделя года: %s\n"
                            + "Месяц: %s\n"
                            + "Год: %s\n"
                            + "AM или PM: %s\n"
                            + "Часы: %s\n"
                            + "Часы дня: %s\n"
                            + "Минуты: %s\n"
                            + "Секунды: %s\n"
                            , calendar.get(Calendar.DAY_OF_MONTH)
                            , (calendar.get(Calendar.DAY_OF_WEEK) - 1) != 0 ? (calendar.get(Calendar.DAY_OF_WEEK) - 1) : 7
                            , calendar.get(Calendar.DAY_OF_MONTH)
                            , calendar.get(Calendar.DAY_OF_YEAR)
                            , calendar.get(Calendar.WEEK_OF_MONTH)
                            , calendar.get(Calendar.WEEK_OF_YEAR)
                            , calendar.get(Calendar.MONTH) + 1
                            , calendar.get(Calendar.YEAR)
                            , calendar.get(Calendar.AM_PM) == 0 ? "AM" : "PM"
                            , calendar.get(Calendar.HOUR)
                            , calendar.get(Calendar.HOUR_OF_DAY)
                            , calendar.get(Calendar.MINUTE)
                            , calendar.get(Calendar.SECOND)
                    );
        } else if (date.contains(".")) {
            result = String.format("День: %s\n"
                            + "День недели: %s\n"
                            + "День месяца: %s\n"
                            + "День года: %s\n"
                            + "Неделя месяца: %s\n"
                            + "Неделя года: %s\n"
                            + "Месяц: %s\n"
                            + "Год: %s\n"
                    , calendar.get(Calendar.DAY_OF_MONTH)
                    , (calendar.get(Calendar.DAY_OF_WEEK) - 1) != 0 ? (calendar.get(Calendar.DAY_OF_WEEK) - 1) : 7
                    , calendar.get(Calendar.DAY_OF_MONTH)
                    , calendar.get(Calendar.DAY_OF_YEAR)
                    , calendar.get(Calendar.WEEK_OF_MONTH)
                    , calendar.get(Calendar.WEEK_OF_YEAR)
                    , calendar.get(Calendar.MONTH) + 1
                    , calendar.get(Calendar.YEAR)

            );
        } else if (date.contains(":")) {
            result = String.format("AM или PM: %s\n"
                            + "Часы: %s\n"
                            + "Часы дня: %s\n"
                            + "Минуты: %s\n"
                            + "Секунды: %s\n"
                    , calendar.get(Calendar.AM_PM) == 0 ? "AM" : "PM"
                    , calendar.get(Calendar.HOUR)
                    , calendar.get(Calendar.HOUR_OF_DAY)
                    , calendar.get(Calendar.MINUTE)
                    , calendar.get(Calendar.SECOND)
            );
        }

//        for (int i = 0; i < 17; i++) {
////            int dayOfWeek = Calendar.DAY_OF_WEEK;
//            System.out.println(calendar.get(i));
//        }

//        System.out.println(calendar);
//        System.out.println(calendar.get(Calendar.SUNDAY));
//        System.out.println(calendar.get(Calendar.MONDAY));
//        System.out.println(calendar.get(Calendar.TUESDAY));
//        System.out.println(calendar.get(Calendar.WEDNESDAY));
//        System.out.println(calendar.get(Calendar.THURSDAY));
//        System.out.println(calendar.get(Calendar.FRIDAY));
//        System.out.println(calendar.get(Calendar.SATURDAY));

        System.out.println(result);
    }

}
