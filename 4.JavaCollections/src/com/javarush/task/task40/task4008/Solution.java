package com.javarush.task.task40.task4008;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalField;

/* 
Работа с Java 8 DateTime API
*/

public class Solution {
    public static void main(String[] args) {
        printDate("21.4.2014 15:56:45");
        System.out.println();
        printDate("21.4.2014");
        System.out.println();
        printDate("17:33:40");

        System.out.println();
        printDate("9.10.2017 5:56:45");

    }

    public static void printDate(String date) {
        //напишите тут ваш код
//        Calendar calendar = Calendar.getInstance();
//        DateFormat formatter = null;
        String result = null;
        DateTimeFormatter dateFormatter = null;
        DateTimeFormatter timeFormatter = null;
//        LocalDateTime localDateTime = null;
        LocalDate localDate = null;
        LocalTime localTime = null;

        // https://docs.oracle.com/javase/8/docs/api/index.html
//        LocalDate date = LocalDate.now();
//        String text = date.format(formatter);
//        LocalDate parsedDate = LocalDate.parse(date, formatter);

        if (date.contains(" ")) {
//            formatter = new SimpleDateFormat("dd.M.yyyy HH:mm:ss");
            dateFormatter = DateTimeFormatter.ofPattern("d.M.yyyy");
            timeFormatter = DateTimeFormatter.ofPattern("H:m:s");
            localDate = LocalDate.parse(date.split(" ")[0], dateFormatter);
            localTime = LocalTime.parse(date.split(" ")[1], timeFormatter);

//            dateFormatter = DateTimeFormatter.ofPattern("d.M.yyyy H:m:s");
//            localDateTime = LocalDateTime.parse(date, dateFormatter);
        } else if (date.contains(".")) {
//            formatter = new SimpleDateFormat("dd.M.yyyy");
            dateFormatter = DateTimeFormatter.ofPattern("d.M.yyyy");
            localDate = LocalDate.parse(date, dateFormatter);
        } else if (date.contains(":")) {
//            formatter = new SimpleDateFormat("HH:mm:ss");
            timeFormatter = DateTimeFormatter.ofPattern("H:m:s");
            localTime = LocalTime.parse(date, timeFormatter);
        }

//        System.out.println("localDateTime " + localDateTime);
//        System.out.println("localDateTime " + localDate);
//        System.out.println("localDateTime " + localTime);

//        try {
//            calendar.setTime(formatter.parse(date));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        String day = "День: %s\n"
                + "День недели: %s\n"
                + "День месяца: %s\n"
                + "День года: %s\n"
                + "Неделя месяца: %s\n"
                + "Неделя года: %s\n"
                + "Месяц: %s\n"
                + "Год: %s\n";
        String time = "AM или PM: %s\n"
                + "Часы: %s\n"
                + "Часы дня: %s\n"
                + "Минуты: %s\n"
                + "Секунды: %s\n";

        if (date.contains(" ")) {
//            System.out.println(localDateTime.get(ChronoField.DAY_OF_MONTH));
            result = String.format(day + time
                    , localDate.get(ChronoField.DAY_OF_MONTH)
//                    , (calendar.get(Calendar.DAY_OF_WEEK) - 1) != 0 ? (calendar.get(Calendar.DAY_OF_WEEK) - 1) : 7
                    , localDate.get(ChronoField.DAY_OF_WEEK)
                    , localDate.get(ChronoField.DAY_OF_MONTH)
                    , localDate.get(ChronoField.DAY_OF_YEAR)
                    , localDate.get(ChronoField.ALIGNED_WEEK_OF_MONTH)
                    , localDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR)
                    , localDate.get(ChronoField.MONTH_OF_YEAR)
                    , localDate.get(ChronoField.YEAR)
                    , localTime.get(ChronoField.AMPM_OF_DAY) == 0 ? "AM" : "PM"
                    , localTime.get(ChronoField.CLOCK_HOUR_OF_AMPM)
                    , localTime.get(ChronoField.CLOCK_HOUR_OF_DAY)
                    , localTime.get(ChronoField.MINUTE_OF_HOUR)
                    , localTime.get(ChronoField.SECOND_OF_MINUTE)
            );
        } else if (date.contains(".")) {
//            System.out.println(localDate.get(ChronoField.DAY_OF_MONTH));
            result = String.format(day
                    , localDate.get(ChronoField.DAY_OF_MONTH)
//                    , (calendar.get(Calendar.DAY_OF_WEEK) - 1) != 0 ? (calendar.get(Calendar.DAY_OF_WEEK) - 1) : 7
                    , localDate.get(ChronoField.DAY_OF_WEEK)
                    , localDate.get(ChronoField.DAY_OF_MONTH)
                    , localDate.get(ChronoField.DAY_OF_YEAR)
                    , localDate.get(ChronoField.ALIGNED_WEEK_OF_MONTH)
                    , localDate.get(ChronoField.ALIGNED_WEEK_OF_YEAR)
                    , localDate.get(ChronoField.MONTH_OF_YEAR)
                    , localDate.get(ChronoField.YEAR)
            );
        } else if (date.contains(":")) {
//            System.out.println("ampm " + localTime.get(ChronoField.AMPM_OF_DAY));
            result = String.format(time
                    , localTime.get(ChronoField.AMPM_OF_DAY) == 0 ? "AM" : "PM"
                    , localTime.get(ChronoField.CLOCK_HOUR_OF_AMPM)
                    , localTime.get(ChronoField.CLOCK_HOUR_OF_DAY)
                    , localTime.get(ChronoField.MINUTE_OF_HOUR)
                    , localTime.get(ChronoField.SECOND_OF_MINUTE)
            );
        }

//        for (int i = 0; i < 17; i++) {
////            int dayOfWeek = Calendar.DAY_OF_WEEK;
//            System.out.println(calendar.get(i));
//        }

//        System.out.println(calendar);
//        System.out.println(calendar.get(Calendar.SUNDAY));
//        System.out.println(calendar.get(Calendar.MONDAY));
//        System.out.println(calendar.get(Calendar.TUESDAY));
//        System.out.println(calendar.get(Calendar.WEDNESDAY));
//        System.out.println(calendar.get(Calendar.THURSDAY));
//        System.out.println(calendar.get(Calendar.FRIDAY));
//        System.out.println(calendar.get(Calendar.SATURDAY));

        System.out.println(result);
    }
}
