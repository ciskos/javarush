package com.javarush.task.task26.task2613.command;

import com.javarush.task.task26.task2613.ConsoleHelper;
import com.javarush.task.task26.task2613.CurrencyManipulator;
import com.javarush.task.task26.task2613.CurrencyManipulatorFactory;

import java.util.Collection;

class InfoCommand implements Command {
    @Override
    public void execute() {
        Collection<CurrencyManipulator> currencyManipulators = CurrencyManipulatorFactory.getAllCurrencyManipulators();
        boolean hasMoney = false;

        for (CurrencyManipulator cm : currencyManipulators) {
            if (cm.hasMoney()) hasMoney = true;
        }

        if (hasMoney) {
            for (CurrencyManipulator cm : currencyManipulators) {
                System.out.printf("%s - %s", cm.getCurrencyCode(), cm.getTotalAmount());
            }
        } else {
            System.out.println("No money available.");

        }

//        currencyManipulators.stream()
//                .forEach(e -> System.out.printf("%s - %s", e.getCurrencyCode(), e.getTotalAmount()));
    }
}
