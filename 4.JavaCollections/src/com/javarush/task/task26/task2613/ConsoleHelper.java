package com.javarush.task.task26.task2613;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ConsoleHelper {
    private static BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));

    public static void writeMessage(String message) {
        System.out.println(message);
    }

    public static String readString() {
        String result = null;

        try {
            result = bis.readLine();
        } catch (Exception e) {
        }

        return result;
    }

    public static String askCurrencyCode() {
        String result = null;

        try {
            do {
                System.out.println("Введите код валюты(пример - USD):");
                result = bis.readLine();
            } while (result.length() != 3);
        } catch (Exception e) {
        }

        return result.toUpperCase();
    }

    public static String[] getValidTwoDigits(String currencyCode) {
        String result = null;

        try {
            do {
                System.out.println("Введите номинал и количество банкнот:");
                result = bis.readLine();
            } while (result == null || !result.matches("\\d+\\s\\d+"));


        } catch (Exception e) {
        }

        return result.split(" ");
    }

    public static Operation askOperation() {

        Operation allowableOperationByOrdinal = null;

        do {
            System.out.println("Введите операцию(например - 1): ");
            Integer operation = Integer.valueOf(readString());

            try {
                allowableOperationByOrdinal = Operation.getAllowableOperationByOrdinal(operation);
            } catch (Exception e) {
//                e.printStackTrace();
            }
        } while (allowableOperationByOrdinal == null);


        return allowableOperationByOrdinal;
    }
}
