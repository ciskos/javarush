package com.javarush.task.task26.task2613;

import com.javarush.task.task26.task2613.command.CommandExecutor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;

public class CashMachine {
    public static void main(String[] args) {
        Locale.setDefault(Locale.ENGLISH);

        Operation operation = null;
//        try (InputStreamReader isr = new InputStreamReader(System.in);
//             BufferedReader br = new BufferedReader(isr);) {

            do {
                System.out.println("Введите операцию: ");
                operation = ConsoleHelper.askOperation();
//                        Operation.valueOf(br.readLine());

                CommandExecutor.execute(operation);
            } while (operation != Operation.EXIT);

//        } catch (Exception e) {
//            e.printStackTrace();
//        }

//        String currencyCode = ConsoleHelper.askCurrencyCode();
//        String[] denominationAndNumber = ConsoleHelper.getValidTwoDigits(currencyCode);
//        CurrencyManipulator currencyManipulator = CurrencyManipulatorFactory.getManipulatorByCurrencyCode(currencyCode);
//
//        currencyManipulator.addAmount(Integer.valueOf(denominationAndNumber[0]), Integer.valueOf(denominationAndNumber[1]));
//        System.out.println(currencyManipulator.getTotalAmount());
    }
}
