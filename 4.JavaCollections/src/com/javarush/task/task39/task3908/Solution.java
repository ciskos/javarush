package com.javarush.task.task39.task3908;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/*
Возможен ли палиндром?
*/
public class Solution {
    public static void main(String[] args) {
        System.out.println(isPalindromePermutation("abcabc"));
    }

    public static boolean isPalindromePermutation(String s) {
        long lettersCount = Arrays.stream(s.split(""))
                .map(String::toLowerCase)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .filter(v -> (v.getValue() % 2 != 0))
                .count();

//        System.out.println(lettersCount);

        return lettersCount > 1 ? false : true;
    }
}
