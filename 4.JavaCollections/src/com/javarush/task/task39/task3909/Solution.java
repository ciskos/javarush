package com.javarush.task.task39.task3909;

import java.util.Arrays;

/*
Одно изменение
*/
public class Solution {
    public static void main(String[] args) {
//        System.out.println(isOneEditAway("ab", null));      // false
        System.out.println(isOneEditAway("ab", ""));        // false
        System.out.println(isOneEditAway("abcd", "ab"));    // false
        System.out.println(isOneEditAway("abcde", "b"));    // false
        System.out.println(isOneEditAway("xzzz", "zxzz"));  // false
        System.out.println(isOneEditAway("asdfg", "aZdf")); // false
        System.out.println(isOneEditAway("poiuy", "poud")); // false
        System.out.println("============================");
        System.out.println(isOneEditAway("", ""));          // true
        System.out.println(isOneEditAway("a", ""));         // true
        System.out.println(isOneEditAway("qert", "qwert")); // true
        System.out.println(isOneEditAway("abcd", "abd"));   // true
        System.out.println(isOneEditAway("zzz", "zzzz"));   // true
        System.out.println(isOneEditAway("xzzz", "zzzz"));  // true
    }

    // https://www.baeldung.com/java-levenshtein-distance
    public static boolean isOneEditAway(String first, String second) {
        int[][] dp = new int[first.length() + 1][second.length() + 1];

        for (int i = 0; i <= first.length(); i++) {
            for (int j = 0; j <= second.length(); j++) {
                if (i == 0) {
                    dp[i][j] = j;
                }
                else if (j == 0) {
                    dp[i][j] = i;
                }
                else {
                    dp[i][j] = min(dp[i - 1][j - 1]
                                    + costOfSubstitution(first.charAt(i - 1), second.charAt(j - 1)),
                            dp[i - 1][j] + 1,
                            dp[i][j - 1] + 1);
                }
            }
        }

//        System.out.println(dp[first.length()][second.length()]);
//        return dp[first.length()][second.length()];
        return dp[first.length()][second.length()] <= 1;
//        if (first.isEmpty()) {
//            return second.length();
//        }
//
//        if (second.isEmpty()) {
//            return first.length();
//        }
//
//        int substitution = isOneEditAway(first.substring(1), second.substring(1))
//                + costOfSubstitution(first.charAt(0), second.charAt(0));
//        int insertion = isOneEditAway(first, second.substring(1)) + 1;
//        int deletion = isOneEditAway(first.substring(1), second) + 1;
//
//        return min(substitution, insertion, deletion);
    }

    public static int costOfSubstitution(char a, char b) {
        return a == b ? 0 : 1;
    }

    public static int min(int... numbers) {
        return Arrays.stream(numbers)
                .min().orElse(Integer.MAX_VALUE);
    }
}
