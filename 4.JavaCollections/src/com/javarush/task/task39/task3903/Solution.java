package com.javarush.task.task39.task3903;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.BitSet;

/* 
Неравноценный обмен
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Please enter a number: ");

        long number = Long.parseLong(reader.readLine());
        System.out.println("Please enter the first index: ");
        int i = Integer.parseInt(reader.readLine());
        System.out.println("Please enter the second index: ");
        int j = Integer.parseInt(reader.readLine());

        System.out.println("The result of swapping bits is " + swapBits(number, i, j));
    }

    public static long swapBits(long number, int i, int j) {
        BitSet bs = BitSet.valueOf(new long[]{number});

//        System.out.println(bs);

        boolean a = bs.get(i);
        boolean b = bs.get(j);

//        System.out.println(a + " " + b);

        bs.set(i, b);
        bs.set(j, a);

//        System.out.println(bs);
//        String longBits = Long.toBinaryString(number);
//        char[] bits = new char[64];
//        longBits.getChars(0, longBits.length(), bits, 0);

//        for (int k = 0; k < bits.length; k++) System.out.println(k + " " + bits[k]);
//        System.out.println(bits);

//        char a = 0;
//        char b = 0;
//
//        if (i < longBits.length()) a = longBits.charAt(i);
//        if (j < longBits.length()) b = longBits.charAt(j);
//
//        bits[i] = b;
//        bits[j] = a;

//        System.out.println(bits);

//        return Long.valueOf(new String(bits));

//        for (long l : bs.toLongArray()) System.out.println(l);

        return bs.toLongArray()[0];
    }
}
