package com.javarush.task.task39.task3913;

import com.javarush.task.task39.task3913.query.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class LogParser implements IPQuery, UserQuery, DateQuery, EventQuery, QLQuery {
    private Path logDir;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    public LogParser(Path logDir) {
        this.logDir = logDir;
    }

    /*
    * IPQuery
    * */
    public Set<String> getIPsForDate(Date date, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getDate().equals(date))
                .map(LogLine::getIp)
                .collect(Collectors.toSet());
    }

    @Override
    public int getNumberOfUniqueIPs(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .map(LogLine::getIp)
                .collect(Collectors.toSet()).size();
    }

    @Override
    public Set<String> getUniqueIPs(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .map(LogLine::getIp)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getIPsForUser(String user, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getUser().equals(user))
                .map(LogLine::getIp)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getIPsForEvent(Event event, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getEvent().equals(event))
                .map(LogLine::getIp)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getIPsForStatus(Status status, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getStatus().equals(status))
                .map(LogLine::getIp)
                .collect(Collectors.toSet());
    }

    /*
    * UserQuery
    * */
    public Set<String> getUsersForDate(Date date, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getDate().equals(date))
                .map(LogLine::getUser)
                .collect(Collectors.toSet());
    }

    public Set<String> getUsersForEvent(Event event, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getEvent().equals(event))
                .map(LogLine::getUser)
                .collect(Collectors.toSet());
    }

    public Set<String> getUsersForStatus(Status status, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getStatus().equals(status))
                .map(LogLine::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getAllUsers() {
        return getLogLinesAfterAndBefore(null, null).stream()
                .map(LogLine::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public int getNumberOfUsers(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .map(LogLine::getUser)
                .collect(Collectors.toSet()).size();
    }

    @Override
    public int getNumberOfUserEvents(String user, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getUser().equals(user))
                .map(LogLine::getUser)
                .collect(Collectors.toSet()).size();
    }

    @Override
    public Set<String> getUsersForIP(String ip, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getIp().equals(ip))
                .map(LogLine::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getLoggedUsers(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getEvent().equals(Event.LOGIN))
                .map(LogLine::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getDownloadedPluginUsers(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getEvent().equals(Event.DOWNLOAD_PLUGIN))
                .map(LogLine::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getWroteMessageUsers(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getEvent().equals(Event.WRITE_MESSAGE))
                .map(LogLine::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getSolvedTaskUsers(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getEvent().equals(Event.SOLVE_TASK))
                .map(LogLine::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getSolvedTaskUsers(Date after, Date before, int task) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getEvent().equals(Event.SOLVE_TASK))
                .filter(logLine -> logLine.getTaskNumber() == task)
                .map(LogLine::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getDoneTaskUsers(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getEvent().equals(Event.DONE_TASK))
                .map(LogLine::getUser)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> getDoneTaskUsers(Date after, Date before, int task) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getEvent().equals(Event.DONE_TASK))
                .filter(logLine -> logLine.getTaskNumber() == task)
                .map(LogLine::getUser)
                .collect(Collectors.toSet());
    }

    /*
    * DateQuery
    * */
    // @org.jetbrains.annotations.NotNull
    public Set<Date> getAllUniqueDates(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .map(LogLine::getDate)
                .collect(Collectors.toSet());
    }

    public Set<Date> getDatesForIP(String ip, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getIp().equals(ip))
                .map(LogLine::getDate)
                .collect(Collectors.toSet());
    }

    public Set<Date> getDatesForUser(String user, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getUser().equals(user))
                .map(LogLine::getDate)
                .collect(Collectors.toSet());
    }

    public Set<Date> getDatesForEvent(Event event, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getEvent().equals(event))
                .map(LogLine::getDate)
                .collect(Collectors.toSet());
    }

    public Set<Date> getDatesForStatus(Status status, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getStatus().equals(status))
                .map(LogLine::getDate)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Date> getDatesForUserAndEvent(String user, Event event, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getEvent().equals(event))
                .filter(logLine -> logLine.getUser().equals(user))
                .map(LogLine::getDate)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Date> getDatesWhenSomethingFailed(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getStatus().equals(Status.FAILED))
                .map(LogLine::getDate)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Date> getDatesWhenErrorHappened(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getStatus().equals(Status.ERROR))
                .map(LogLine::getDate)
                .collect(Collectors.toSet());
    }

    @Override
    public Date getDateWhenUserLoggedFirstTime(String user, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getUser().equals(user))
                .filter(logLine -> logLine.getEvent().equals(Event.LOGIN))
                .map(LogLine::getDate)
                .sorted()
                .findFirst()
                .orElse(null);
    }

    @Override
    public Date getDateWhenUserSolvedTask(String user, int task, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getUser().equals(user))
                .filter(logLine -> logLine.getEvent().equals(Event.SOLVE_TASK))
                .filter(logLine -> logLine.getTaskNumber() == task)
                .map(LogLine::getDate)
                .sorted()
                .findFirst()
                .orElse(null);
    }

    @Override
    public Date getDateWhenUserDoneTask(String user, int task, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getUser().equals(user))
                .filter(logLine -> logLine.getEvent().equals(Event.DONE_TASK))
                .filter(logLine -> logLine.getTaskNumber() == task)
                .map(LogLine::getDate)
                .sorted()
                .findFirst()
                .orElse(null);
    }

    @Override
    public Set<Date> getDatesWhenUserWroteMessage(String user, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getUser().equals(user))
                .filter(logLine -> logLine.getEvent().equals(Event.WRITE_MESSAGE))
                .map(LogLine::getDate)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Date> getDatesWhenUserDownloadedPlugin(String user, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getUser().equals(user))
                .filter(logLine -> logLine.getEvent().equals(Event.DOWNLOAD_PLUGIN))
                .map(LogLine::getDate)
                .collect(Collectors.toSet());
    }


    /*
    * EventQuery
    * */
    public Set<Event> getEventsForStatus(Status status, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getStatus().equals(status))
                .map(LogLine::getEvent)
                .collect(Collectors.toSet());
    }

    public Set<Event> getEventsForDate(Date date, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getDate().equals(date))
                .map(LogLine::getEvent)
                .collect(Collectors.toSet());
    }

    @Override
    public int getNumberOfAllEvents(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .map(LogLine::getEvent)
                .collect(Collectors.toSet()).size();
    }

    @Override
    public Set<Event> getAllEvents(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .map(LogLine::getEvent)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Event> getEventsForIP(String ip, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getIp().equals(ip))
                .map(LogLine::getEvent)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Event> getEventsForUser(String user, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getUser().equals(user))
                .map(LogLine::getEvent)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Event> getFailedEvents(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getStatus().equals(Status.FAILED))
                .map(LogLine::getEvent)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Event> getErrorEvents(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getStatus().equals(Status.ERROR))
                .map(LogLine::getEvent)
                .collect(Collectors.toSet());
    }

    @Override
    public int getNumberOfAttemptToSolveTask(int task, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getEvent().equals(Event.SOLVE_TASK))
                .filter(logLine -> logLine.getTaskNumber() == task)
                .collect(Collectors.toList()).size();
    }

    @Override
    public int getNumberOfSuccessfulAttemptToSolveTask(int task, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getEvent().equals(Event.DONE_TASK))
                .filter(logLine -> logLine.getTaskNumber() == task)
//                .filter(logLine -> logLine.getStatus().equals(Status.OK))
                .collect(Collectors.toList()).size();
    }

    @Override
    public Map<Integer, Integer> getAllSolvedTasksAndTheirNumber(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getEvent().equals(Event.SOLVE_TASK))
                .map(logLine -> logLine.getTaskNumber())
                .collect(Collectors.toMap(task -> task, task -> 1, Integer::sum));
    }

    @Override
    public Map<Integer, Integer> getAllDoneTasksAndTheirNumber(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getEvent().equals(Event.DONE_TASK))
                .map(logLine -> logLine.getTaskNumber())
                .collect(Collectors.toMap(task -> task, task -> 1, Integer::sum));
    }

    /*
     * Status Query
     * */
    public Set<Status> getAllUniqueStatuses(Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .map(LogLine::getStatus)
                .collect(Collectors.toSet());
    }

    public Set<Status> getStatusesForDate(Date date, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getDate().equals(date))
                .map(LogLine::getStatus)
                .collect(Collectors.toSet());
    }

    public Set<Status> getStatusesForIP(String ip, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getIp().equals(ip))
                .map(LogLine::getStatus)
                .collect(Collectors.toSet());
    }

    public Set<Status> getStatusesForUser(String user, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getUser().equals(user))
                .map(LogLine::getStatus)
                .collect(Collectors.toSet());
    }

    public Set<Status> getStatusesForEvent(Event event, Date after, Date before) {
        return getLogLinesAfterAndBefore(after, before).stream()
                .filter(logLine -> logLine.getEvent().equals(event))
                .map(LogLine::getStatus)
                .collect(Collectors.toSet());
    }

    /*
    * QLQuery
    * */
    @Override
    public Set<Object> execute(String query) {
//        String simpleQuery = "^get\\s(\\w+)";
        String simpleQuery =
                        "^get       #Неизменяемая часть выражения запроса\n"
                        + "\\s      #Пробел\n"
                        + "(\\w+)   #Все символы и цифры с повтором хотя бы один раз\n";

        String extendedQuery = "(?:\\sfor\\s(\\w+)\\s=\\s\"((?:[^\"].+?\\s?))\")?";
/*
        String extendedQuery =
                        "(?:        #Исключить группу из списка групп\n"
                        + "\\s      #Пробел\n"
                        + "for      #Неизменяемая часть выражения запроса\n"
                        + "\\s      #Пробел\n"
                        + "(\\w+)   #Все символы и цифры с повтором хотя бы один раз\n"
                        + "\\s      #Пробел\n"
                        + "=        #Неизменяемая часть выражения запроса\n"
                        + "\\s      #Пробел\n"
                        + "\"       #Открывающая кавычка\n"
                        + "(        #Скобка открывающая группу\n"
                        + "(?:      #Исключить группу из списка групп\n"
                        + "[^\"]    #Исключить из поиска кавычки\n"
                        + ".+?      #Любой симол минимум один раз с минимальным количеством повторений\n"
                        + "\\s?     #Необязательный пробел\n"
                        + ")        #Скобка закрывающая группу\n"
                        + ")        #Скобка закрывающая группу\n"
                        + "\"       #Закрывающая кавычка\n"
                        + ")?       #Группа необязательна\n";
*/

        String datedQuery = "(?:\\sand\\sdate\\sbetween\\s\"([^\"].+?)\"\\sand\\s\"([^\"].+?)\")?";

/*
        String datedQuery =
                        "(?:        #Исключить группу из списка групп\n"
                        + "\\s      #Пробел\n"
                        + "and      #Неизменяемая часть выражения запроса\n"
                        + "\\s      #Пробел\n"
                        + "date     #Неизменяемая часть выражения запроса\n"
                        + "\\s      #Пробел\n"
                        + "between  #Неизменяемая часть выражения запроса\n"
                        + "\\s      #Пробел\n"
                        + "\"       #Открывающая кавычка\n"
                        + "(        #Скобка открывающая группу\n"
                        + "[^\"]    #Исключить из поиска кавычки\n"
                        + ".+?      #Любой симол минимум один раз с минимальным количеством повторений\n"
                        + ")        #Скобка закрывающая группу\n"
                        + "\"       #Закрывающая кавычка\n"
                        + "\\s      #Пробел\n"
                        + "and      #Неизменяемая часть выражения запроса\n"
                        + "\\s      #Пробел\n"
                        + "\"       #Открывающая кавычка\n"
                        + "(        #Скобка открывающая группу\n"
                        + "[^\"]    #Исключить из поиска кавычки\n"
                        + ".+?      #Любой симолв минимум один раз с минимальным количеством повторений\n"
                        + ")        #Скобка закрывающая группу\n"
                        + "\"       #Закрывающая кавычка\n"
                        + ")?       #Группа необязательна\n";
*/

        String pattern = simpleQuery + extendedQuery + datedQuery;
        Matcher matcher = Pattern.compile(pattern, Pattern.COMMENTS).matcher(query);

//        System.out.println("pattern " + matcher.pattern());
//        while (matcher.find()) {
//            for (int i = 0; i <= matcher.groupCount(); i++) {
//                System.out.println("[i " + i + "] [" + matcher.group(i) + "]");
//            }
//        }

        matcher.find();

        String get = matcher.group(1);
        String getFor = matcher.group(2);
        String getForValue = matcher.group(3);
        Date getDateAfter = null;
        Date getDateBefore = null;
        try {
            getDateAfter = matcher.group(4) == null ? null : sdf.parse(matcher.group(4));
            getDateBefore = matcher.group(5) == null ? null : sdf.parse(matcher.group(5));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Обработка простых случаев
        if (getFor == null) return executeSimpleQuery(get);

        try {
            switch (get) {
                case "ip":
                    return getIPsFor(getFor, getForValue, getDateAfter, getDateBefore);
                case "user":
                    return getUsersFor(getFor, getForValue, getDateAfter, getDateBefore);
                case "date":
                    return getDatesFor(getFor, getForValue, getDateAfter, getDateBefore);
                case "event":
                    return getEventsFor(getFor, getForValue, getDateAfter, getDateBefore);
                case "status":
                    return getStatusesFor(getFor, getForValue, getDateAfter, getDateBefore);
                default:
                    return null;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    /*
    * QLQuery methods
    * */
    public Set<Object> getIPsFor(String getFor, String getForValue, Date after, Date before) throws ParseException {
        switch (getFor) {
            case "user":
                return new HashSet<>(getIPsForUser(getForValue, after, before));
            case "date":
                return new HashSet<>(getIPsForDate(sdf.parse(getForValue), after, before));
            case "event":
                return new HashSet<>(getIPsForEvent(Event.valueOf(getForValue), after, before));
            case "status":
                return new HashSet<>(getIPsForStatus(Status.valueOf(getForValue), after, before));
            default:
                return null;
        }
    }

    public Set<Object> getUsersFor(String getFor, String getForValue, Date after, Date before) throws ParseException {
        switch (getFor) {
            case "ip":
                return new HashSet<>(getUsersForIP(getForValue, after, before));
            case "date":
                return new HashSet<>(getUsersForDate(sdf.parse(getForValue), after, before));
            case "event":
                return new HashSet<>(getUsersForEvent(Event.valueOf(getForValue), after, before));
            case "status":
                return new HashSet<>(getUsersForStatus(Status.valueOf(getForValue), after, before));
            default:
                return null;
        }
    }

    public Set<Object> getDatesFor(String getFor, String getForValue, Date after, Date before) {
        switch (getFor) {
            case "ip":
                return new HashSet<>(getDatesForIP(getForValue, after, before));
            case "user":
                return new HashSet<>(getDatesForUser(getForValue, after, before));
            case "event":
                return new HashSet<>(getDatesForEvent(Event.valueOf(getForValue), after, before));
            case "status":
                return new HashSet<>(getDatesForStatus(Status.valueOf(getForValue), after, before));
            default:
                return null;
        }
    }

    public Set<Object> getEventsFor(String getFor, String getForValue, Date after, Date before) throws ParseException {
        switch (getFor) {
            case "ip":
                return new HashSet<>(getEventsForIP(getForValue, after, before));
            case "user":
                return new HashSet<>(getEventsForUser(getForValue, after, before));
            case "date":
                return new HashSet<>(getEventsForDate(sdf.parse(getForValue), after, before));
            case "status":
                return new HashSet<>(getEventsForStatus(Status.valueOf(getForValue), after, before));
            default:
                return null;
        }
    }

    public Set<Object> getStatusesFor(String getFor, String getForValue, Date after, Date before) throws ParseException {
        switch (getFor) {
            case "ip":
                return new HashSet<>(getStatusesForIP(getForValue, after, before));
            case "user":
                return new HashSet<>(getStatusesForUser(getForValue, after, before));
            case "date":
                return new HashSet<>(getStatusesForDate(sdf.parse(getForValue), after, before));
            case "event":
                return new HashSet<>(getStatusesForEvent(Event.valueOf(getForValue), after, before));
            default:
                return null;
        }
    }

    public Set<Object> executeSimpleQuery(String simpleQuery) {
        switch (simpleQuery) {
            case "ip":
                return new HashSet<>(getUniqueIPs(null, null));
            case "user":
                return new HashSet<>(getAllUsers());
            case "date":
                return new HashSet<>(getAllUniqueDates(null, null));
            case "event":
                return new HashSet<>(getAllEvents(null, null));
            case "status":
                return new HashSet<>(getAllUniqueStatuses(null, null));
            default:
                return null;
        }
    }

    /*
     * Log Parse Methods
     * */
    public List<LogLine> getLogLinesAfterAndBefore(Date after, Date before) {
        List<LogLine> result = new ArrayList<>();
        Long afterLong = after == null ? Long.MIN_VALUE : after.getTime();
        Long beforeLong = before == null ? Long.MAX_VALUE : before.getTime();

        try {
            for (String log : getLogsList(logDir.toString())) {
                for (String line : getLogLinesList(log)) {
                    LogLine parsedLine = parseLogLine(line);
                    Long findDate = parsedLine.getDate().getTime();

                    if (findDate > afterLong && findDate < beforeLong)
                        result.add(parsedLine);
                }
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    public List<String> getLogsList(String logDir) throws IOException {
        final String finalLogDir = logDir;
        return Files.walk(Paths.get(finalLogDir))
                .map(f -> f.toAbsolutePath().toString())
                .filter(f -> f.endsWith("log"))
                .collect(Collectors.toList());
    }

    public List<String> getLogLinesList(String file) throws IOException {
        File f = new File(file);
        FileReader fr = new FileReader(f);
        BufferedReader br = new BufferedReader(fr);
        List<String> result = new LinkedList<>();

        while (br.ready()) {
            String line = br.readLine();
            result.add(line);
        }

        br.close();

//        for (int i = 0; i < result.size(); i++) System.out.println(i + " " + result.get(i));

        return result;
    }

    public LogLine parseLogLine(String logLine) throws ParseException {
        LogLine result = new LogLine();
        String[] lineElements = logLine.split("\t");
        String[] splitedEvent = lineElements[3].split(" ");
//        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

        result.setIp(lineElements[0]);
        result.setUser(lineElements[1]);
        result.setDate(sdf.parse(lineElements[2]));
        result.setEvent(Event.valueOf(splitedEvent[0]));
        result.setTaskNumber(splitedEvent.length > 1 ? Integer.valueOf(splitedEvent[1]) : null);
        result.setStatus(Status.valueOf(lineElements[4]));

        return result;
    }

    /*
    * Log Line Class
    * */
    class LogLine {
        private String ip;
        private String user;
        private Date date;
        private Event event;
        private Integer taskNumber;
        private Status status;

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public Event getEvent() {
            return event;
        }

        public void setEvent(Event event) {
            this.event = event;
        }

        public Integer getTaskNumber() {
            return taskNumber;
        }

        public void setTaskNumber(Integer taskNumber) {
            this.taskNumber = taskNumber;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            LogLine logLine = (LogLine) o;

            if (ip != null ? !ip.equals(logLine.ip) : logLine.ip != null) return false;
            if (user != null ? !user.equals(logLine.user) : logLine.user != null) return false;
            if (date != null ? !date.equals(logLine.date) : logLine.date != null) return false;
            if (event != logLine.event) return false;
            if (taskNumber != null ? !taskNumber.equals(logLine.taskNumber) : logLine.taskNumber != null) return false;
            return status == logLine.status;
        }

        @Override
        public int hashCode() {
            int result = ip != null ? ip.hashCode() : 0;
            result = 31 * result + (user != null ? user.hashCode() : 0);
            result = 31 * result + (date != null ? date.hashCode() : 0);
            result = 31 * result + (event != null ? event.hashCode() : 0);
            result = 31 * result + (taskNumber != null ? taskNumber.hashCode() : 0);
            result = 31 * result + (status != null ? status.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "LogLine{" +
                    "ip='" + ip + '\'' +
                    ", user='" + user + '\'' +
                    ", date=" + date +
                    ", event=" + event +
                    ", taskNumber=" + taskNumber +
                    ", status=" + status +
                    '}';
        }
    }
}