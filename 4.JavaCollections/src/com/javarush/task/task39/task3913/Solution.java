package com.javarush.task.task39.task3913;

import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Date;

public class Solution {
    public static void main(String[] args) {
//        LogParser logParser = new LogParser(Paths.get("c:/logs/"));
        LogParser logParser = new LogParser(Paths.get("D:\\GIT\\JavaRushTasks\\4.JavaCollections\\src\\com\\javarush\\task\\task39\\task3913\\logs\\example.log"));

//        System.out.println(logParser.getLogLinesAfterAndBefore(null, new Date()));
//        System.out.println(logParser.getLogLinesAfterAndBefore(new Date(), new Date()));
//        System.out.println(logParser.getLogLinesAfterAndBefore(new Date(), null));
//        logParser.getLogLinesAfterAndBefore(null, null).stream()
//                .sorted(Comparator.comparing(LogParser.LogLine::getUser))
//                .forEach(System.out::println);

/*        System.out.println("getNumberOfUniqueIPs");
        // getNumberOfUniqueIPs
        System.out.println(logParser.getNumberOfUniqueIPs(null, new Date()));
        System.out.println(logParser.getNumberOfUniqueIPs(new Date(), new Date()));
        System.out.println(logParser.getNumberOfUniqueIPs(new Date(), null));
        System.out.println(logParser.getNumberOfUniqueIPs(null, null));

        System.out.println("getUniqueIPs---------------------------------");
        // getUniqueIPs
        System.out.println(logParser.getUniqueIPs(null, new Date()));
        System.out.println(logParser.getUniqueIPs(new Date(), new Date()));
        System.out.println(logParser.getUniqueIPs(new Date(), null));

        System.out.println("getIPsForUser---------------------------------");
        // getIPsForUser
        System.out.println(logParser.getIPsForUser("Amigo", null, new Date()));
        System.out.println(logParser.getIPsForUser("Eduard Petrovich Morozko", new Date(), new Date()));
        System.out.println(logParser.getIPsForUser("Vasya Pupkin", new Date(), null));

        System.out.println("getIPsForEvent---------------------------------");
        // 1 getIPsForEvent
        System.out.println(logParser.getIPsForEvent(Event.DONE_TASK, null, new Date()));
        System.out.println(logParser.getIPsForEvent(Event.DONE_TASK, new Date(), new Date()));
        System.out.println(logParser.getIPsForEvent(Event.DONE_TASK, new Date(), null));
        // 2
        System.out.println(logParser.getIPsForEvent(Event.SOLVE_TASK, null, new Date()));
        System.out.println(logParser.getIPsForEvent(Event.SOLVE_TASK, new Date(), new Date()));
        System.out.println(logParser.getIPsForEvent(Event.SOLVE_TASK, new Date(), null));
        // 3
        System.out.println(logParser.getIPsForEvent(Event.WRITE_MESSAGE, null, new Date()));
        System.out.println(logParser.getIPsForEvent(Event.WRITE_MESSAGE, new Date(), new Date()));
        System.out.println(logParser.getIPsForEvent(Event.WRITE_MESSAGE, new Date(), null));
        // 4
        System.out.println(logParser.getIPsForEvent(Event.DOWNLOAD_PLUGIN, null, new Date()));
        System.out.println(logParser.getIPsForEvent(Event.DOWNLOAD_PLUGIN, new Date(), new Date()));
        System.out.println(logParser.getIPsForEvent(Event.DOWNLOAD_PLUGIN, new Date(), null));
        // 5
        System.out.println(logParser.getIPsForEvent(Event.LOGIN, null, new Date()));
        System.out.println(logParser.getIPsForEvent(Event.LOGIN, new Date(), new Date()));
        System.out.println(logParser.getIPsForEvent(Event.LOGIN, new Date(), null));

        System.out.println("getIPsForStatus---------------------------------");
        // 1 getIPsForStatus
        System.out.println(logParser.getIPsForStatus(Status.ERROR, null, new Date()));
        System.out.println(logParser.getIPsForStatus(Status.ERROR, new Date(), new Date()));
        System.out.println(logParser.getIPsForStatus(Status.ERROR, new Date(), null));
        // 2
        System.out.println(logParser.getIPsForStatus(Status.FAILED, null, new Date()));
        System.out.println(logParser.getIPsForStatus(Status.FAILED, new Date(), new Date()));
        System.out.println(logParser.getIPsForStatus(Status.FAILED, new Date(), null));
        // 3
        System.out.println(logParser.getIPsForStatus(Status.OK, null, new Date()));
        System.out.println(logParser.getIPsForStatus(Status.OK, new Date(), new Date()));
        System.out.println(logParser.getIPsForStatus(Status.OK, new Date(), null));

        System.out.println("getLogLinesAfterAndBefore---------------------------------");
        // getLogLinesAfterAndBefore
        System.out.println(logParser.getLogLinesAfterAndBefore(null, new Date()));
        System.out.println(logParser.getLogLinesAfterAndBefore(new Date(), new Date()));
        System.out.println(logParser.getLogLinesAfterAndBefore(new Date(), null));


        System.out.println("getAllUsers---------------------------------");
        // getAllUsers
        System.out.println(logParser.getAllUsers());

        System.out.println("getNumberOfUsers---------------------------------");
        // getNumberOfUsers
        System.out.println(logParser.getNumberOfUsers(null, new Date()));
        System.out.println(logParser.getNumberOfUsers(new Date(), new Date()));
        System.out.println(logParser.getNumberOfUsers(new Date(), null));
        System.out.println(logParser.getNumberOfUsers(null, null));

        System.out.println("getNumberOfUserEvents---------------------------------");
        // getNumberOfUserEvents
        System.out.println(logParser.getNumberOfUserEvents("Amigo", null, new Date()));
        System.out.println(logParser.getNumberOfUserEvents("Amigo", new Date(), new Date()));
        System.out.println(logParser.getNumberOfUserEvents("Amigo", new Date(), null));
        System.out.println(logParser.getNumberOfUserEvents("Amigo", null, null));

        System.out.println(logParser.getNumberOfUserEvents("Eduard Petrovich Morozko", null, new Date()));
        System.out.println(logParser.getNumberOfUserEvents("Eduard Petrovich Morozko", new Date(), new Date()));
        System.out.println(logParser.getNumberOfUserEvents("Eduard Petrovich Morozko", new Date(), null));
        System.out.println(logParser.getNumberOfUserEvents("Eduard Petrovich Morozko", null, null));

        System.out.println(logParser.getNumberOfUserEvents("Vasya Pupkin", null, new Date()));
        System.out.println(logParser.getNumberOfUserEvents("Vasya Pupkin", new Date(), new Date()));
        System.out.println(logParser.getNumberOfUserEvents("Vasya Pupkin", new Date(), null));
        System.out.println(logParser.getNumberOfUserEvents("Vasya Pupkin", null, null));

        System.out.println("getUsersForIP---------------------------------");
        // getUsersForIP
        System.out.println(logParser.getUsersForIP("127.0.0.1", null, new Date()));
        System.out.println(logParser.getUsersForIP("127.0.0.1", new Date(), new Date()));
        System.out.println(logParser.getUsersForIP("127.0.0.1", new Date(), null));
        System.out.println(logParser.getUsersForIP("127.0.0.1", null, null));

        System.out.println(logParser.getUsersForIP("192.168.100.2", null, new Date()));
        System.out.println(logParser.getUsersForIP("192.168.100.2", new Date(), new Date()));
        System.out.println(logParser.getUsersForIP("192.168.100.2", new Date(), null));
        System.out.println(logParser.getUsersForIP("192.168.100.2", null, null));

        System.out.println(logParser.getUsersForIP("146.34.15.5", null, new Date()));
        System.out.println(logParser.getUsersForIP("146.34.15.5", new Date(), new Date()));
        System.out.println(logParser.getUsersForIP("146.34.15.5", new Date(), null));
        System.out.println(logParser.getUsersForIP("146.34.15.5", null, null));

        System.out.println(logParser.getUsersForIP("12.12.12.12", null, new Date()));
        System.out.println(logParser.getUsersForIP("12.12.12.12", new Date(), new Date()));
        System.out.println(logParser.getUsersForIP("12.12.12.12", new Date(), null));
        System.out.println(logParser.getUsersForIP("12.12.12.12", null, null));

        System.out.println(logParser.getUsersForIP("120.120.120.122", null, new Date()));
        System.out.println(logParser.getUsersForIP("120.120.120.122", new Date(), new Date()));
        System.out.println(logParser.getUsersForIP("120.120.120.122", new Date(), null));
        System.out.println(logParser.getUsersForIP("120.120.120.122", null, null));

        System.out.println("getLoggedUsers---------------------------------");
        // getLoggedUsers
        System.out.println(logParser.getLoggedUsers(null, new Date()));
        System.out.println(logParser.getLoggedUsers(new Date(), new Date()));
        System.out.println(logParser.getLoggedUsers(new Date(), null));
        System.out.println(logParser.getLoggedUsers(null, null));

        System.out.println("getDownloadedPluginUsers---------------------------------");
        // getDownloadedPluginUsers
        System.out.println(logParser.getDownloadedPluginUsers(null, new Date()));
        System.out.println(logParser.getDownloadedPluginUsers(new Date(), new Date()));
        System.out.println(logParser.getDownloadedPluginUsers(new Date(), null));
        System.out.println(logParser.getDownloadedPluginUsers(null, null));

        System.out.println("getWroteMessageUsers---------------------------------");
        // getWroteMessageUsers
        System.out.println(logParser.getWroteMessageUsers(null, new Date()));
        System.out.println(logParser.getWroteMessageUsers(new Date(), new Date()));
        System.out.println(logParser.getWroteMessageUsers(new Date(), null));
        System.out.println(logParser.getWroteMessageUsers(null, null));

        System.out.println("getSolvedTaskUsers---------------------------------");
        // getSolvedTaskUsers
        System.out.println(logParser.getSolvedTaskUsers(null, new Date()));
        System.out.println(logParser.getSolvedTaskUsers(new Date(), new Date()));
        System.out.println(logParser.getSolvedTaskUsers(new Date(), null));
        System.out.println(logParser.getSolvedTaskUsers(null, null));

        System.out.println("getSolvedTaskUsers---------------------------------");
        // getSolvedTaskUsers
        System.out.println(logParser.getSolvedTaskUsers(null, new Date(), 1));
        System.out.println(logParser.getSolvedTaskUsers(new Date(), new Date(), 1));
        System.out.println(logParser.getSolvedTaskUsers(new Date(), null, 1));
        System.out.println(logParser.getSolvedTaskUsers(null, null, 1));

        System.out.println(logParser.getSolvedTaskUsers(null, new Date(), 18));
        System.out.println(logParser.getSolvedTaskUsers(new Date(), new Date(), 18));
        System.out.println(logParser.getSolvedTaskUsers(new Date(), null, 18));
        System.out.println(logParser.getSolvedTaskUsers(null, null, 18));

        System.out.println("getDoneTaskUsers---------------------------------");
        // getDoneTaskUsers
        System.out.println(logParser.getDoneTaskUsers(null, new Date()));
        System.out.println(logParser.getDoneTaskUsers(new Date(), new Date()));
        System.out.println(logParser.getDoneTaskUsers(new Date(), null));
        System.out.println(logParser.getDoneTaskUsers(null, null));

        System.out.println("getDoneTaskUsers---------------------------------");
        // getDoneTaskUsers
        System.out.println(logParser.getDoneTaskUsers(null, new Date(), 15));
        System.out.println(logParser.getDoneTaskUsers(new Date(), new Date(), 15));
        System.out.println(logParser.getDoneTaskUsers(new Date(), null, 15));
        System.out.println(logParser.getDoneTaskUsers(null, null, 15));

        System.out.println(logParser.getDoneTaskUsers(null, new Date(), 48));
        System.out.println(logParser.getDoneTaskUsers(new Date(), new Date(), 48));
        System.out.println(logParser.getDoneTaskUsers(new Date(), null, 48));
        System.out.println(logParser.getDoneTaskUsers(null, null, 48));

        System.out.println("getDatesForUserAndEvent---------------------------------");
        // getDatesForUserAndEvent
        System.out.println(logParser.getDatesForUserAndEvent("Amigo", Event.LOGIN, null, new Date()));
        System.out.println(logParser.getDatesForUserAndEvent("Eduard Petrovich Morozko", Event.DOWNLOAD_PLUGIN, new Date(), new Date()));
        System.out.println(logParser.getDatesForUserAndEvent("Vasya Pupkin", Event.WRITE_MESSAGE, new Date(), null));
        System.out.println(logParser.getDatesForUserAndEvent("Amigo", Event.SOLVE_TASK, null, null));

        System.out.println("getDatesWhenSomethingFailed---------------------------------");
        // getDatesWhenSomethingFailed
        System.out.println(logParser.getDatesWhenSomethingFailed(null, new Date()));
        System.out.println(logParser.getDatesWhenSomethingFailed(new Date(), new Date()));
        System.out.println(logParser.getDatesWhenSomethingFailed(new Date(), null));
        System.out.println(logParser.getDatesWhenSomethingFailed(null, null));

        System.out.println("getDatesWhenErrorHappened---------------------------------");
        // getDatesWhenErrorHappened
        System.out.println(logParser.getDatesWhenErrorHappened(null, new Date()));
        System.out.println(logParser.getDatesWhenErrorHappened(new Date(), new Date()));
        System.out.println(logParser.getDatesWhenErrorHappened(new Date(), null));
        System.out.println(logParser.getDatesWhenErrorHappened(null, null));

        System.out.println("getDateWhenUserLoggedFirstTime---------------------------------");
        // getDateWhenUserLoggedFirstTime
        System.out.println(logParser.getDateWhenUserLoggedFirstTime("Amigo", null, new Date()));
        System.out.println(logParser.getDateWhenUserLoggedFirstTime("Amigo", new Date(), new Date()));
        System.out.println(logParser.getDateWhenUserLoggedFirstTime("Amigo", new Date(), null));
        System.out.println(logParser.getDateWhenUserLoggedFirstTime("Amigo", null, null));

        System.out.println(logParser.getDateWhenUserLoggedFirstTime("Eduard Petrovich Morozko", null, new Date()));
        System.out.println(logParser.getDateWhenUserLoggedFirstTime("Eduard Petrovich Morozko", new Date(), new Date()));
        System.out.println(logParser.getDateWhenUserLoggedFirstTime("Eduard Petrovich Morozko", new Date(), null));
        System.out.println(logParser.getDateWhenUserLoggedFirstTime("Eduard Petrovich Morozko", null, null));

        System.out.println(logParser.getDateWhenUserLoggedFirstTime("Vasya Pupkin", null, new Date()));
        System.out.println(logParser.getDateWhenUserLoggedFirstTime("Vasya Pupkin", new Date(), new Date()));
        System.out.println(logParser.getDateWhenUserLoggedFirstTime("Vasya Pupkin", new Date(), null));
        System.out.println(logParser.getDateWhenUserLoggedFirstTime("Vasya Pupkin", null, null));

        System.out.println("getDateWhenUserSolvedTask---------------------------------");
        // getDateWhenUserSolvedTask
        System.out.println(logParser.getDateWhenUserSolvedTask("Amigo", 1, null, new Date()));
        System.out.println(logParser.getDateWhenUserSolvedTask("Amigo", 1, new Date(), new Date()));
        System.out.println(logParser.getDateWhenUserSolvedTask("Amigo", 18, new Date(), null));
        System.out.println(logParser.getDateWhenUserSolvedTask("Amigo", 18, null, null));

        System.out.println(logParser.getDateWhenUserSolvedTask("Eduard Petrovich Morozko", 1, null, new Date()));
        System.out.println(logParser.getDateWhenUserSolvedTask("Eduard Petrovich Morozko", 1, new Date(), new Date()));
        System.out.println(logParser.getDateWhenUserSolvedTask("Eduard Petrovich Morozko", 18, new Date(), null));
        System.out.println(logParser.getDateWhenUserSolvedTask("Eduard Petrovich Morozko", 18, null, null));

        System.out.println(logParser.getDateWhenUserSolvedTask("Vasya Pupkin", 1, null, new Date()));
        System.out.println(logParser.getDateWhenUserSolvedTask("Vasya Pupkin", 1, new Date(), new Date()));
        System.out.println(logParser.getDateWhenUserSolvedTask("Vasya Pupkin", 18, new Date(), null));
        System.out.println(logParser.getDateWhenUserSolvedTask("Vasya Pupkin", 18, null, null));

        System.out.println("getDateWhenUserDoneTask---------------------------------");
        // getDateWhenUserDoneTask
        System.out.println(logParser.getDateWhenUserDoneTask("Amigo", 15, null, new Date()));
        System.out.println(logParser.getDateWhenUserDoneTask("Amigo", 15, new Date(), new Date()));
        System.out.println(logParser.getDateWhenUserDoneTask("Amigo", 48, new Date(), null));
        System.out.println(logParser.getDateWhenUserDoneTask("Amigo", 48, null, null));

        System.out.println(logParser.getDateWhenUserDoneTask("Eduard Petrovich Morozko", 15, null, new Date()));
        System.out.println(logParser.getDateWhenUserDoneTask("Eduard Petrovich Morozko", 15, new Date(), new Date()));
        System.out.println(logParser.getDateWhenUserDoneTask("Eduard Petrovich Morozko", 48, new Date(), null));
        System.out.println(logParser.getDateWhenUserDoneTask("Eduard Petrovich Morozko", 48, null, null));

        System.out.println(logParser.getDateWhenUserDoneTask("Vasya Pupkin", 15, null, new Date()));
        System.out.println(logParser.getDateWhenUserDoneTask("Vasya Pupkin", 15, new Date(), new Date()));
        System.out.println(logParser.getDateWhenUserDoneTask("Vasya Pupkin", 48, new Date(), null));
        System.out.println(logParser.getDateWhenUserDoneTask("Vasya Pupkin", 48, null, null));

        System.out.println("getDatesWhenUserWroteMessage---------------------------------");
        // getDatesWhenUserWroteMessage
        System.out.println(logParser.getDatesWhenUserWroteMessage("Amigo", null, new Date()));
        System.out.println(logParser.getDatesWhenUserWroteMessage("Amigo", new Date(), new Date()));
        System.out.println(logParser.getDatesWhenUserWroteMessage("Amigo", new Date(), null));
        System.out.println(logParser.getDatesWhenUserWroteMessage("Amigo", null, null));

        System.out.println(logParser.getDatesWhenUserWroteMessage("Eduard Petrovich Morozko", null, new Date()));
        System.out.println(logParser.getDatesWhenUserWroteMessage("Eduard Petrovich Morozko", new Date(), new Date()));
        System.out.println(logParser.getDatesWhenUserWroteMessage("Eduard Petrovich Morozko", new Date(), null));
        System.out.println(logParser.getDatesWhenUserWroteMessage("Eduard Petrovich Morozko", null, null));

        System.out.println(logParser.getDatesWhenUserWroteMessage("Vasya Pupkin", null, new Date()));
        System.out.println(logParser.getDatesWhenUserWroteMessage("Vasya Pupkin", new Date(), new Date()));
        System.out.println(logParser.getDatesWhenUserWroteMessage("Vasya Pupkin", new Date(), null));
        System.out.println(logParser.getDatesWhenUserWroteMessage("Vasya Pupkin", null, null));

        System.out.println("getDatesWhenUserDownloadedPlugin---------------------------------");
        // getDatesWhenUserDownloadedPlugin
        System.out.println(logParser.getDatesWhenUserDownloadedPlugin("Amigo", null, new Date()));
        System.out.println(logParser.getDatesWhenUserDownloadedPlugin("Amigo", new Date(), new Date()));
        System.out.println(logParser.getDatesWhenUserDownloadedPlugin("Amigo", new Date(), null));
        System.out.println(logParser.getDatesWhenUserDownloadedPlugin("Amigo", null, null));

        System.out.println(logParser.getDatesWhenUserDownloadedPlugin("Eduard Petrovich Morozko", null, new Date()));
        System.out.println(logParser.getDatesWhenUserDownloadedPlugin("Eduard Petrovich Morozko", new Date(), new Date()));
        System.out.println(logParser.getDatesWhenUserDownloadedPlugin("Eduard Petrovich Morozko", new Date(), null));
        System.out.println(logParser.getDatesWhenUserDownloadedPlugin("Eduard Petrovich Morozko", null, null));

        System.out.println(logParser.getDatesWhenUserDownloadedPlugin("Vasya Pupkin", null, new Date()));
        System.out.println(logParser.getDatesWhenUserDownloadedPlugin("Vasya Pupkin", new Date(), new Date()));
        System.out.println(logParser.getDatesWhenUserDownloadedPlugin("Vasya Pupkin", new Date(), null));
        System.out.println(logParser.getDatesWhenUserDownloadedPlugin("Vasya Pupkin", null, null));

        System.out.println("getNumberOfAllEvents---------------------------------");
        // getNumberOfAllEvents
        System.out.println(logParser.getNumberOfAllEvents(null, new Date()));
        System.out.println(logParser.getNumberOfAllEvents(new Date(), new Date()));
        System.out.println(logParser.getNumberOfAllEvents(new Date(), null));
        System.out.println(logParser.getNumberOfAllEvents(null, null));

        System.out.println("getAllEvents---------------------------------");
        // getAllEvents
        System.out.println(logParser.getAllEvents(null, new Date()));
        System.out.println(logParser.getAllEvents(new Date(), new Date()));
        System.out.println(logParser.getAllEvents(new Date(), null));
        System.out.println(logParser.getAllEvents(null, null));

        System.out.println("getEventsForIP---------------------------------");
        // getEventsForIP
        System.out.println(logParser.getEventsForIP("127.0.0.1", null, new Date()));
        System.out.println(logParser.getEventsForIP("127.0.0.1", new Date(), new Date()));
        System.out.println(logParser.getEventsForIP("127.0.0.1", new Date(), null));
        System.out.println(logParser.getEventsForIP("127.0.0.1", null, null));

        System.out.println(logParser.getEventsForIP("192.168.100.2", null, new Date()));
        System.out.println(logParser.getEventsForIP("192.168.100.2", new Date(), new Date()));
        System.out.println(logParser.getEventsForIP("192.168.100.2", new Date(), null));
        System.out.println(logParser.getEventsForIP("192.168.100.2", null, null));

        System.out.println(logParser.getEventsForIP("146.34.15.5", null, new Date()));
        System.out.println(logParser.getEventsForIP("146.34.15.5", new Date(), new Date()));
        System.out.println(logParser.getEventsForIP("146.34.15.5", new Date(), null));
        System.out.println(logParser.getEventsForIP("146.34.15.5", null, null));

        System.out.println(logParser.getEventsForIP("12.12.12.12", null, new Date()));
        System.out.println(logParser.getEventsForIP("12.12.12.12", new Date(), new Date()));
        System.out.println(logParser.getEventsForIP("12.12.12.12", new Date(), null));
        System.out.println(logParser.getEventsForIP("12.12.12.12", null, null));

        System.out.println(logParser.getEventsForIP("120.120.120.122", null, new Date()));
        System.out.println(logParser.getEventsForIP("120.120.120.122", new Date(), new Date()));
        System.out.println(logParser.getEventsForIP("120.120.120.122", new Date(), null));
        System.out.println(logParser.getEventsForIP("120.120.120.122", null, null));

        System.out.println("getEventsForUser---------------------------------");
        // getEventsForUser
        System.out.println(logParser.getEventsForUser("Amigo", null, new Date()));
        System.out.println(logParser.getEventsForUser("Amigo", new Date(), new Date()));
        System.out.println(logParser.getEventsForUser("Amigo", new Date(), null));
        System.out.println(logParser.getEventsForUser("Amigo", null, null));

        System.out.println(logParser.getEventsForUser("Eduard Petrovich Morozko", null, new Date()));
        System.out.println(logParser.getEventsForUser("Eduard Petrovich Morozko", new Date(), new Date()));
        System.out.println(logParser.getEventsForUser("Eduard Petrovich Morozko", new Date(), null));
        System.out.println(logParser.getEventsForUser("Eduard Petrovich Morozko", null, null));

        System.out.println(logParser.getEventsForUser("Vasya Pupkin", null, new Date()));
        System.out.println(logParser.getEventsForUser("Vasya Pupkin", new Date(), new Date()));
        System.out.println(logParser.getEventsForUser("Vasya Pupkin", new Date(), null));
        System.out.println(logParser.getEventsForUser("Vasya Pupkin", null, null));

        System.out.println("getFailedEvents---------------------------------");
        // getFailedEvents
        System.out.println(logParser.getFailedEvents(null, new Date()));
        System.out.println(logParser.getFailedEvents(new Date(), new Date()));
        System.out.println(logParser.getFailedEvents(new Date(), null));
        System.out.println(logParser.getFailedEvents(null, null));

        System.out.println("getErrorEvents---------------------------------");
        // getErrorEvents
        System.out.println(logParser.getErrorEvents(null, new Date()));
        System.out.println(logParser.getErrorEvents(new Date(), new Date()));
        System.out.println(logParser.getErrorEvents(new Date(), null));
        System.out.println(logParser.getErrorEvents(null, null));

        System.out.println("getNumberOfAttemptToSolveTask---------------------------------");
        // getNumberOfAttemptToSolveTask
        System.out.println(logParser.getNumberOfAttemptToSolveTask(1, null, new Date()));
        System.out.println(logParser.getNumberOfAttemptToSolveTask(1, new Date(), new Date()));
        System.out.println(logParser.getNumberOfAttemptToSolveTask(18, new Date(), null));
        System.out.println(logParser.getNumberOfAttemptToSolveTask(18, null, null));

        System.out.println("getNumberOfSuccessfulAttemptToSolveTask---------------------------------");
        // getNumberOfSuccessfulAttemptToSolveTask
        System.out.println(logParser.getNumberOfSuccessfulAttemptToSolveTask(1, null, new Date()));
        System.out.println(logParser.getNumberOfSuccessfulAttemptToSolveTask(1, new Date(), new Date()));
        System.out.println(logParser.getNumberOfSuccessfulAttemptToSolveTask(1, new Date(), null));
        System.out.println(logParser.getNumberOfSuccessfulAttemptToSolveTask(1, null, null));

        System.out.println(logParser.getNumberOfSuccessfulAttemptToSolveTask(18, null, new Date()));
        System.out.println(logParser.getNumberOfSuccessfulAttemptToSolveTask(18, new Date(), new Date()));
        System.out.println(logParser.getNumberOfSuccessfulAttemptToSolveTask(18, new Date(), null));
        System.out.println(logParser.getNumberOfSuccessfulAttemptToSolveTask(18, null, null));

        System.out.println("getAllSolvedTasksAndTheirNumber---------------------------------");
        // getAllSolvedTasksAndTheirNumber
        System.out.println(logParser.getAllSolvedTasksAndTheirNumber(null, new Date()));
        System.out.println(logParser.getAllSolvedTasksAndTheirNumber(new Date(), new Date()));
        System.out.println(logParser.getAllSolvedTasksAndTheirNumber(new Date(), null));
        System.out.println(logParser.getAllSolvedTasksAndTheirNumber(null, null));

        System.out.println("getAllDoneTasksAndTheirNumber---------------------------------");
        // getAllDoneTasksAndTheirNumber
        System.out.println(logParser.getAllDoneTasksAndTheirNumber(null, new Date()));
        System.out.println(logParser.getAllDoneTasksAndTheirNumber(new Date(), new Date()));
        System.out.println(logParser.getAllDoneTasksAndTheirNumber(new Date(), null));
        System.out.println(logParser.getAllDoneTasksAndTheirNumber(null, null));*/

//        System.out.println("simpleExecute---------------------------------");
//        // simpleExecute
//        System.out.println(logParser.execute("get ip"));
//        System.out.println(logParser.execute("get user"));
//        System.out.println(logParser.execute("get date"));
//        System.out.println(logParser.execute("get event"));
//        System.out.println(logParser.execute("get status"));
//
//        System.out.println("getIPsFor---------------------------------");
//        // getIPsFor
//        System.out.println(logParser.execute("get ip for user = \"Amigo\""));
//        System.out.println(logParser.execute("get ip for user = \"Vasya Pupkin\""));
//        System.out.println(logParser.execute("get ip for user = \"Eduard Petrovich Morozko\""));
//        System.out.println(logParser.execute("get ip for date = \"30.08.2012 16:08:40\""));
//        System.out.println(logParser.execute("get ip for event = \"DONE_TASK\""));
//        System.out.println(logParser.execute("get ip for event = \"SOLVE_TASK\""));
//        System.out.println(logParser.execute("get ip for status = \"OK\""));
//
//        System.out.println("getUsersFor---------------------------------");
//        // getUsersFor
//        System.out.println(logParser.execute("get user for ip = \"127.0.0.1\""));
//        System.out.println(logParser.execute("get user for date = \"30.08.2012 16:08:40\""));
//        System.out.println(logParser.execute("get user for event = \"DONE_TASK\""));
//        System.out.println(logParser.execute("get user for status = \"OK\""));
//
//        System.out.println("getUsersFor---------------------------------");
//        // getUsersFor
//        System.out.println(logParser.execute("get date for ip = \"127.0.0.1\""));
//        System.out.println(logParser.execute("get date for user = \"Amigo\""));
//        System.out.println(logParser.execute("get date for event = \"DONE_TASK\""));
//        System.out.println(logParser.execute("get date for status = \"OK\""));
//
//        System.out.println("getEventsFor---------------------------------");
//        // getEventsFor
//        System.out.println(logParser.execute("get event for ip = \"127.0.0.1\""));
//        System.out.println(logParser.execute("get event for user = \"Amigo\""));
//        System.out.println(logParser.execute("get event for date = \"30.08.2012 16:08:40\""));
//        System.out.println(logParser.execute("get event for status = \"OK\""));
//
//        System.out.println("getStatusesFor---------------------------------");
//        // getStatusesFor
//        System.out.println(logParser.execute("get status for ip = \"127.0.0.1\""));
//        System.out.println(logParser.execute("get status for user = \"Amigo\""));
//        System.out.println(logParser.execute("get status for date = \"30.08.2012 16:08:40\""));
//        System.out.println(logParser.execute("get status for event = \"DONE_TASK\""));

        System.out.println("getIPsFor---------------------------------");
        // getIPsFor
        System.out.println(logParser.execute("get ip for user = \"Amigo\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get ip for user = \"Vasya Pupkin\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get ip for user = \"Eduard Petrovich Morozko\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get ip for date = \"30.08.2012 16:08:40\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get ip for event = \"DONE_TASK\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get ip for event = \"SOLVE_TASK\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get ip for status = \"OK\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));

        System.out.println("getUsersFor---------------------------------");
        // getUsersFor
        System.out.println(logParser.execute("get user for ip = \"127.0.0.1\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get user for date = \"30.08.2012 16:08:40\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get user for event = \"DONE_TASK\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get user for status = \"OK\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));

        System.out.println("getDatesFor---------------------------------");
        // getDatesFor
        System.out.println(logParser.execute("get date for ip = \"127.0.0.1\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get date for user = \"Amigo\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get date for event = \"DONE_TASK\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get date for status = \"OK\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));

        System.out.println("getEventsFor---------------------------------");
        // getEventsFor
        System.out.println(logParser.execute("get event for ip = \"127.0.0.1\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get event for user = \"Amigo\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get event for date = \"30.08.2012 16:08:40\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get event for status = \"OK\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));

        System.out.println("getStatusesFor---------------------------------");
        // getStatusesFor
        System.out.println(logParser.execute("get status for ip = \"127.0.0.1\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get status for user = \"Amigo\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get status for date = \"30.08.2012 16:08:40\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));
        System.out.println(logParser.execute("get status for event = \"DONE_TASK\" and date between \"01.01.2012 00:00:00\" and \"01.01.2019 00:00:00\""));

    }
}