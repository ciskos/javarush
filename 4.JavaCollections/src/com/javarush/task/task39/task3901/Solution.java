package com.javarush.task.task39.task3901;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/* 
Уникальные подстроки
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please enter your string: ");
        String s = bufferedReader.readLine();

        System.out.println("The longest unique substring length is: \n" + lengthOfLongestUniqueSubstring(s));
    }

    public static int lengthOfLongestUniqueSubstring(String s) {
        if (s == null || s.isEmpty()) return 0;

        List<Integer> longest = new ArrayList<>();
        Set<Character> chars = new HashSet<>();

        for (int i = 0; i < s.length(); i++) {
            for (int j = i; j < s.length(); j++) {
                if (!chars.contains(s.charAt(j))) {
//                    System.out.printf("%s", s.charAt(j));
                    chars.add(s.charAt(j));
                    longest.add(chars.size());
                } else {
//                    System.out.println(" " + chars.size() + " " + chars);
                    chars.clear();
                    break;
                }
            }

        }

//        System.out.println(longest);
        Optional<Integer> optionalInteger = longest.stream().max(Integer::compareTo);
//        System.out.println(optionalInteger);
        Integer result = optionalInteger.isPresent() ? optionalInteger.get() : -1;

        return result;
    }
}
