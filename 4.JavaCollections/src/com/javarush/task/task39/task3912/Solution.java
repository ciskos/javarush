package com.javarush.task.task39.task3912;

/* 
Максимальная площадь
*/

public class Solution {

    // https://github.com/mission-peace/interview/blob/master/src/com/interview/dynamic/MaximumSizeSubMatrix.java
    public static void main(String[] args) {
        int arr[][] = {{0,1,1,0,1},{1,1,1,0,0},{1,1,1,1,0},{1,1,1,0,1}};
        Solution solution = new Solution();
        int result = solution.maxSquare(arr);
        System.out.print(result);
    }

    public static int maxSquare(int[][] matrix) {
        int result[][] = new int[matrix.length][matrix[0].length];
        int max = 0;

        for(int i=0; i < matrix.length; i++){
            result[i][0] = matrix[i][0];
            if (result[i][0] == 1)
            {
                max = 1;
            }
        }

        for(int i=0; i < matrix[0].length; i++){
            result[0][i] = matrix[0][i];
            if (result[0][i] == 1)
            {
                max = 1;
            }

        }

        for(int i=1; i < matrix.length; i++){
            for(int j=1; j < matrix[i].length; j++){
                if(matrix[i][j] == 0){
                    continue;
                }
                int t = min(result[i-1][j],result[i-1][j-1],result[i][j-1]);
                result[i][j] =  t +1;
                if(result[i][j] > max){
                    max = result[i][j];
                }
            }
        }
        return max * max;
    }

    private static int min(int a,int b, int c){
        int l = Math.min(a, b);
        return Math.min(l, c);
    }
}
