package com.javarush.task.task37.task3714;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/* 
Древний Рим
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Input a roman number to be converted to decimal: ");
        String romanString = bufferedReader.readLine();
        System.out.println("Conversion result equals " + romanToInteger(romanString));

//        System.out.println("4 " + romanToInteger("IV"));
//        System.out.println("6 " + romanToInteger("VI"));
//        System.out.println("8 " + romanToInteger("VIII"));
//        System.out.println("9 " + romanToInteger("IX"));
//        System.out.println("15 " + romanToInteger("XV"));
//        System.out.println("19 " + romanToInteger("XIX"));
//        System.out.println("40 " + romanToInteger("XL"));
//        System.out.println("42 " + romanToInteger("XLII"));
//        System.out.println("60 " + romanToInteger("LX"));
//        System.out.println("80 " + romanToInteger("LXXX"));
//        System.out.println("83 " + romanToInteger("LXXXIII"));
//        System.out.println("90 " + romanToInteger("XC"));
//        System.out.println("94 " + romanToInteger("XCIV"));
//        System.out.println("150 " + romanToInteger("CL"));
//        System.out.println("283 " + romanToInteger("CCLXXXIII"));
//        System.out.println("800 " + romanToInteger("DCCC"));
//        System.out.println("1604 " + romanToInteger("MDCIV"));
//        System.out.println("1988 " + romanToInteger("MCMLXXXVIII"));
//        System.out.println("2683 " + romanToInteger("MMDCLXXXIII"));
//        System.out.println("3332 " + romanToInteger("MMDDCCLLXXVVII"));
//        System.out.println("3500 " + romanToInteger("MMMD"));
    }

    public static int romanToInteger(String s) {
        Map<Character, Integer> romanToDecimal = new HashMap<>();

        romanToDecimal.put('I', 1);
        romanToDecimal.put('V', 5);
        romanToDecimal.put('X', 10);
        romanToDecimal.put('L', 50);
        romanToDecimal.put('C', 100);
        romanToDecimal.put('D', 500);
        romanToDecimal.put('M', 1000);

        int result = 0;
        int previousNumber = 0;

        for (int i = s.length() - 1; i >= 0; i--) {
            if (romanToDecimal.containsKey(s.charAt(i))) {
                int currentNumber = romanToDecimal.get(s.charAt(i));

                if (currentNumber < previousNumber) {
                    result -= romanToDecimal.get(s.charAt(i));
                } else {
                    result += romanToDecimal.get(s.charAt(i));
                }

                previousNumber = romanToDecimal.get(s.charAt(i));
            }
        }

        return result;
    }

}
