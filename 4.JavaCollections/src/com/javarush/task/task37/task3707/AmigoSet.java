package com.javarush.task.task37.task3707;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;

public class AmigoSet<E> extends AbstractSet<E> implements Serializable, Cloneable, Set<E> {

    private static final Object PRESENT = new Object();
    private transient HashMap<E,Object> map;

    public AmigoSet() {
        this.map = new HashMap<>();
    }

    public AmigoSet(Collection<? extends E> collection) {
        int capacity = (int)(Math.max(16, collection.size()/.75F + 1));
        this.map = new HashMap<>(capacity);

        Iterator<? extends E> collectionIterator = collection.iterator();
        while (collectionIterator.hasNext()) {
            add(collectionIterator.next());
        }
    }

    @Override
    public Iterator<E> iterator() {
        return map.keySet().iterator();
    }

    @Override
    public boolean add(E e) {
        return map.put(e, PRESENT) == null;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return map.containsKey(o);
    }

    @Override
    public boolean remove(Object o) {
        return map.remove(o) != null;
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        AmigoSet clone;

        try {
            clone = (AmigoSet)super.clone();
            clone.map = (HashMap) map.clone();
        } catch (Exception e) {
            throw new InternalError();
        }

        return clone;
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject();
        oos.writeObject(HashMapReflectionHelper.callHiddenMethod(map, "capacity"));
        oos.writeObject(HashMapReflectionHelper.callHiddenMethod(map, "loadFactor"));
        oos.writeObject(map.size());

        map.forEach((k, v) -> {
            try {
                oos.writeObject(k);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
        ois.defaultReadObject();
        int capacity = (int)ois.readObject();
        float loadFactor = (float)ois.readObject();
        this.map = new HashMap<>(capacity, loadFactor);

        int size = (int) ois.readObject();
        for (int i = 0; i < size; i++) {
            this.map.put((E) ois.readObject(), PRESENT);
        }
    }
}
