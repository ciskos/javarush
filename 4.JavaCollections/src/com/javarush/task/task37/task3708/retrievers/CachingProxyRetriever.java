package com.javarush.task.task37.task3708.retrievers;

import com.javarush.task.task37.task3708.cache.LRUCache;
import com.javarush.task.task37.task3708.storage.Storage;

public class CachingProxyRetriever implements Retriever {
    OriginalRetriever originalRetriever;
    LRUCache lruCache;

    public CachingProxyRetriever(Storage storage) {
        originalRetriever = new OriginalRetriever(storage);
        this.lruCache = new LRUCache(16);
    }

    /*
    * 4. Метод retrieve класса CachingProxyRetriever должен выполнять поиск подходящего
    * объекта в кеше с помощью метода find.
    * 5. Метод retrieve класса CachingProxyRetriever должен получать объект из хранилища
    * с помощью метода retrieve объекта типа OriginalRetriever и добавлять в кеш, если он не был там найден.
    * 6. Метод retrieve класса CachingProxyRetriever не должен вызывать метод retrieve класса
    * OriginalRetriever, если объект был найден в кеше.
    * */
    @Override
    public Object retrieve(long id) {
        Object cacheObject = lruCache.find(id);

        if (cacheObject == null) {
            Object storageObject = originalRetriever.retrieve(id);
            lruCache.set(id, storageObject);
            return storageObject;
        }

        return cacheObject;
    }
}
