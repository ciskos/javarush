package com.javarush.task.task32.task3202;

import java.io.*;

/* 
Читаем из потока
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        StringWriter writer = getAllDataFromInputStream(new FileInputStream("D:/GIT/JavaRushTasks/task3202/testFile.log"));
        System.out.println(writer.toString());
    }

    public static StringWriter getAllDataFromInputStream(InputStream is) throws IOException {
        if (is == null) return new StringWriter();

        StringWriter sw = new StringWriter();
        BufferedInputStream bis = new BufferedInputStream(is);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024 * 10];
        int bufferLength;

        while ((bufferLength = bis.read(buffer)) > 0) {
            baos.write(buffer, 0, bufferLength);
        }

        sw.write(baos.toString());

        return sw;
    }
}