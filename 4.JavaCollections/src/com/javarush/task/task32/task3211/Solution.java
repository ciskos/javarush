package com.javarush.task.task32.task3211;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.security.MessageDigest;
import java.util.Arrays;

/* 
Целостность информации
*/

public class Solution {
    public static void main(String... args) throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(new String("test string"));
        oos.flush();
        System.out.println(compareMD5(bos, "5a47d12a2e3f9fecf2d9ba1fd98152eb")); //true

    }

    public static boolean compareMD5(ByteArrayOutputStream byteArrayOutputStream, String md5) throws Exception {
        // Создать экземпляр для типа хеша MD5
        // Можно использовать также SHA-1 и SHA-256
        MessageDigest md = MessageDigest.getInstance("MD5");
        // Обновить содержимое. Можно использовать в случае передачи порций байтов.
        // После завершения можно использовать md.reset(). Это очистит текущее значение
        // хэша
        md.update(byteArrayOutputStream.toByteArray());
        // Заполнить массив байтами хеша
        byte[] hashInBytes = md.digest();

        StringBuilder sb = new StringBuilder();
        // Каждый байт переводится в шестнадацтиричную систему счисления
        for (byte b : hashInBytes) {
            // %02x по идее значит, что
            // %x - вывод шестнадацтиричного числа
            // 0 - заполнитель пустого пространства(если есть)
            // 2 - количество заполняемых позиций символом 0
            // т.е. если будет передан байт со значением 0, то будет возвращено
            // шестнадцатиричное значение 00
            sb.append(String.format("%02x", b));
        }

        // Сравнить и вернуть
        return sb.toString().equals(md5);
    }
}
