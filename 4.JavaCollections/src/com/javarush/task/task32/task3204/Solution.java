package com.javarush.task.task32.task3204;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* 
Генератор паролей
*/
public class Solution {
    public static void main(String[] args) {
        ByteArrayOutputStream password = getPassword();
        System.out.println(password.toString());
    }

    public static ByteArrayOutputStream getPassword() {
        List<Character> symbols = new ArrayList<>();
        List<Character> result = new ArrayList<>();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        // Формирование полного словаря
        // Числа
        for (int i = 48; i <= 57; i++) {
            symbols.add((char)i);
        }

        // Заглавные буквы
        for (int i = 65; i <= 90; i++) {
            symbols.add((char)i);
        }

        // Прописные буквы
        for (int i = 97; i <= 122; i++) {
            symbols.add((char)i);
        }

        // Взболтать, но не смешивать
        Collections.shuffle(symbols);

        // Гарантированное добавление требуемых символов, хотя бы по разу
        result.add((char)(48 + (int)(Math.random() * 9)));  // Число
        result.add((char)(65 + (int)(Math.random() * 25))); // Заглавная буква
        result.add((char)(97 + (int)(Math.random() * 25))); // Прописная буква

        // Добавить ещё 5 символов в результат
        for (int i = 0; i < 5; i++) {
            result.add(symbols.get(i));
        }

        // Ещё раз взболтать, на всякий случай
        Collections.shuffle(result);

        for (Character c : result) {
            baos.write((int)c);
        }

        return baos;
    }

}