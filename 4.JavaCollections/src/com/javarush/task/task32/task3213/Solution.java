package com.javarush.task.task32.task3213;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

/* 
Шифр Цезаря
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        StringReader reader = new StringReader("Khoor#Dpljr#&C,₷B'3");
        System.out.println(decode(reader, -3));  //Hello Amigo #@)₴?$0
    }

    public static String decode(StringReader reader, int key) throws IOException {
        if (reader == null) return new String();

        BufferedReader br = new BufferedReader(reader);
        StringBuilder sb = new StringBuilder();
        StringBuilder result = new StringBuilder();
        String line;

        while ((line = br.readLine()) != null) {
            sb.append(line);
        }

        for (int i = 0; i < sb.length(); i++) {
            result.append((char)((int)sb.charAt(i) + key));
        }

        return result.toString();
    }
}
