package com.javarush.task.task32.task3210;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;

/* 
Используем RandomAccessFile
*/

public class Solution {
    public static void main(String... args) {
//        args = new String[]{
//                "D:/GIT/JavaRushTasks/task3210/file.txt"
//                , "10"
//                , "Zasdfasdfasdfasdf"
////                , "абв"
//        };


        String fileName = args[0];
        int number = Integer.parseInt(args[1]);
        String text = args[2];

        try (RandomAccessFile raf = new RandomAccessFile(fileName, "rw");
//             ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ) {
            int lengthToRead = text.length();
            byte[] buffer = new byte[lengthToRead];
//            int bufferLength;

            raf.seek(number);
            raf.read(buffer, 0, lengthToRead);
            raf.seek(raf.length());
//            while ((bufferLength = raf.read(buffer, 0, lengthToRead)) > 0) {
//                baos.write(buffer, 0, bufferLength);
//            }


            String output = new String(buffer);

            System.out.println(output);
            System.out.println(text);
            System.out.println(output.equals(text));
            System.out.println(buffer.length + " " + output.length() + " " + text.length());
            if (output.trim().equals(text.trim())) {
                raf.write("true".getBytes());
            } else {
                raf.write("false".getBytes());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
