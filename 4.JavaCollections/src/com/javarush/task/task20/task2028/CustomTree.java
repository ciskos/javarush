package com.javarush.task.task20.task2028;

import java.io.Serializable;
import java.util.*;

/* 
Построй дерево(1)
*/
public class CustomTree extends AbstractList<String> implements Cloneable, Serializable {

    Entry<String> root;
    List<Entry<String>> leafs = new ArrayList<>();
    ArrayList<Entry<String>> leafsToRemove = new ArrayList<>();

    public CustomTree() {
        this.root = new Entry<>("0");
        leafs.add(root);
    }

    @Override
    public String get(int index) {
        throw new UnsupportedOperationException();
//        return null;
    }

    @Override
    public String set(int index, String element) {
        throw new UnsupportedOperationException();
//        return super.set(index, element);
    }

    @Override
    public boolean add(String s) {
        Entry<String> newNode = new Entry<>(s);
        leafs.add(newNode);

        for (int i = 0; i < leafs.size(); i++) {
            Entry<String> current = leafs.get(i);
            Entry<String> parent = current;

            if (current.isAvailableToAddChildren()) {
                if (current.availableToAddLeftChildren) {
                    current.leftChild = newNode;
                    current.availableToAddLeftChildren = false;
                    current.leftChild.parent = parent;
                    return true;
                } else if (current.availableToAddRightChildren) {
                    current.rightChild = newNode;
                    current.availableToAddRightChildren = false;
                    current.rightChild.parent = parent;
                    return true;
                }
            }
        }

        return false;
//        throw new UnsupportedOperationException();
//        return super.add(s);
    }

    // Содрано - https://github.com/Artemsetko/JavaRushTasks/blob/master/4.JavaCollections/src/com/javarush/task/task20/task2028/CustomTree.java
    @Override
    public boolean remove(Object o) {
        if (o instanceof String) {
            String name = (String) o;
            for (int i = 0; i < leafs.size(); i++) {
                Entry<String> leaf = leafs.get(i);
                if (name.equals(leaf.elementName)) {
                    addLeafsToRemove(leaf);
                }
            }
        } else {
            throw new UnsupportedOperationException();
        }
        leafs.removeAll(leafsToRemove);
        return true;
//        if (!(o instanceof String)) {
//            throw new UnsupportedOperationException();
//        }
//
//        Entry<String> remove;
//
//        Iterator<Entry<String>> iES = leafs.iterator();
//
//        while (iES.hasNext()) {
//            Entry<String> leaf = iES.next();
//            if (leaf.elementName.equals(o)) {
////                System.out.println("leaf element Name " + leaf.elementName);
//                iES.remove();
//                return true;
//            }
//        }
//
//
//
//        return false;
//        return super.remove(o);
    }

    private void addLeafsToRemove(Entry<String> leaf) {
        if (leaf == null) return;
        if (leaf.equals(leaf.parent.leftChild)) {
            leaf.parent.leftChild = null;
        } else {
            leaf.parent.availableToAddLeftChildren = true;
            leaf.parent.availableToAddRightChildren = true;
            leaf.parent.rightChild = null;
        }
        leafsToRemove.add(leaf);
        addLeafsToRemove(leaf.leftChild);
        addLeafsToRemove(leaf.rightChild);
    }

    @Override
    public List<String> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
//        return super.subList(fromIndex, toIndex);
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
//        super.removeRange(fromIndex, toIndex);
    }

    @Override
    public boolean addAll(int index, Collection<? extends String> c) {
        throw new UnsupportedOperationException();
//        return super.addAll(index, c);
    }

    @Override
    public int size() {
        return leafs.size() - 1;
//        return 0;
    }

    public String getParent(String s) {

        for (Entry<String> leaf : leafs) {
            if (leaf.elementName.equals(s)) {
                return leaf.parent.elementName;
            }
        }

        return null;
    }


    static class Entry<T> implements Serializable {
        String elementName;
        boolean availableToAddLeftChildren;
        boolean availableToAddRightChildren;
        Entry<T> parent;
        Entry<T> leftChild;
        Entry<T> rightChild;

        public Entry(String elementName) {
            this.elementName = elementName;
            availableToAddLeftChildren = true;
            availableToAddRightChildren = true;
        }

        public boolean isAvailableToAddChildren() {
            return availableToAddLeftChildren | availableToAddRightChildren;
        }
    }
}
