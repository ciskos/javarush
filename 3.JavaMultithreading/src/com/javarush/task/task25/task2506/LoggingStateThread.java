package com.javarush.task.task25.task2506;

public class LoggingStateThread extends Thread {
    private Thread target;

    public LoggingStateThread(Thread target) {
        this.target = target;
    }

    @Override
    public void run() {
        State currentState = target.getState();

        System.out.println(target.getState());

        while (true) {
            if (!currentState.equals(target.getState())) {
                System.out.println(target.getState());
                currentState = target.getState();
            }

            if (currentState.equals(State.TERMINATED)) break;
        }
    }
}
