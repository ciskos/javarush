package com.javarush.task.task25.task2504;

/* 
Switch для нитей
*/
public class Solution {
    private static Thread[] threads = new Thread[10];
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            threads[i] = new Thread();
        }

        processThreads(threads);
    }

    public static void processThreads(Thread... threads) {
        //implement this method - реализуйте этот метод

        for (Thread t : threads) {
            switch (t.getState()) {
                case NEW:
                    t.start();
                    break;
                case RUNNABLE:
                    t.isInterrupted();
                    break;
                case BLOCKED:
                    t.interrupt();
                    break;
                case WAITING:
                    t.interrupt();
                    break;
                case TIMED_WAITING:
                    t.interrupt();
                    break;
                case TERMINATED:
                    System.out.println(t.getPriority());
                    break;
            }
        }
    }
}
