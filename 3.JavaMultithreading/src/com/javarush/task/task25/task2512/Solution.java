package com.javarush.task.task25.task2512;

import java.util.ArrayList;
import java.util.List;

/*
Живем своим умом
*/
public class Solution implements Thread.UncaughtExceptionHandler {

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        t.interrupt();
        eGetCause(e);
    }

    /*
    * В метод передаётся последнее выводимое исключение e.
    * Далее у этого исключения вызывается метод getCause(), который
    * выводит причину-исключение, которая вызвала предыдущее исключение.
    * И так углубляемся до тех пор пока метод getCause не будет равен null.
    * После чего производится вывод цепочки исключений.
    * */
    private void eGetCause(Throwable e) {
        if (e.getCause() != null) eGetCause(e.getCause());

        System.out.println(e);
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        Thread t = new Thread();

        s.uncaughtException(t, new Exception("ABC", new RuntimeException("DEF", new IllegalAccessException("GHI"))));
    }
}
