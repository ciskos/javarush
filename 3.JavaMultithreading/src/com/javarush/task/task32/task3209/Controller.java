package com.javarush.task.task32.task3209;

import javax.swing.*;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import java.io.*;

public class Controller {
    private View view;
    private HTMLDocument document;
    private File currentFile;

    public static void main(String[] args) {
        View view = new View();
        Controller controller = new Controller(view);

        view.setController(controller);
        view.init();
        controller.init();
    }

    public void exit() {
        System.exit(0);
    }

    public Controller(View view) {
        this.view = view;
    }

    public void init() {
        createNewDocument();
    }

    public HTMLDocument getDocument() {
        return document;
    }

    public void resetDocument() {
        if (document != null) {
            document.removeUndoableEditListener(view.getUndoListener());
        }
        document = (HTMLDocument) new HTMLEditorKit().createDefaultDocument();
        document.addUndoableEditListener(view.getUndoListener());
        view.update();
    }

    public void setPlainText(String text) {
        resetDocument();
        StringReader stringReader = new StringReader(text);
        HTMLEditorKit htmlEditorKit = new HTMLEditorKit();

        try {
            htmlEditorKit.read(stringReader, document, 0);
        } catch (Exception e) {
            ExceptionHandler.log(e);
        }
    }

    public String getPlainText() {
        StringWriter stringWriter = new StringWriter();
        HTMLEditorKit htmlEditorKit = new HTMLEditorKit();

        try {
            htmlEditorKit.write(stringWriter, document, 0, document.getLength());
        } catch (Exception e) {
            ExceptionHandler.log(e);
        }

        return stringWriter.toString();
    }

    public void createNewDocument() {
        view.selectHtmlTab();
        resetDocument();
        view.setTitle("HTML редактор");
        view.resetUndo();
        currentFile = null;
    }

    public void openDocument() {
        view.selectHtmlTab();
        JFileChooser jFileChooser = new JFileChooser();
        HTMLFileFilter htmlFileFilter = new HTMLFileFilter();

        jFileChooser.setFileFilter(htmlFileFilter);
        int choose = jFileChooser.showOpenDialog(view);

        if (choose == JFileChooser.APPROVE_OPTION) {
            currentFile = jFileChooser.getSelectedFile();
            resetDocument();
            view.setTitle(currentFile.getName());

            HTMLEditorKit htmlEditorKit = new HTMLEditorKit();
            try (FileReader fileReader = new FileReader(currentFile);) {
                htmlEditorKit.read(fileReader, document, 0);
            } catch (Exception e) {
                ExceptionHandler.log(e);
            }

            view.resetUndo();
        }
    }

    public void saveDocument() {
        view.selectHtmlTab();
        if (currentFile != null) {
//            JFileChooser jFileChooser = new JFileChooser();
//            HTMLFileFilter htmlFileFilter = new HTMLFileFilter();

//            jFileChooser.setSelectedFile(currentFile);
//            jFileChooser.setFileFilter(htmlFileFilter);
//            int choose = jFileChooser.showSaveDialog(view);

//            if (choose == JFileChooser.APPROVE_OPTION) {
//                view.setTitle(currentFile.getName());

                HTMLEditorKit htmlEditorKit = new HTMLEditorKit();
                try (FileWriter fileWriter = new FileWriter(currentFile);) {
                    htmlEditorKit.write(fileWriter, document, 0, document.getLength());
                } catch (Exception e) {
                    ExceptionHandler.log(e);
                }
//            }
        } else {
            saveDocumentAs();
        }
    }

    public void saveDocumentAs() {
        view.selectHtmlTab();
        JFileChooser jFileChooser = new JFileChooser();
        HTMLFileFilter htmlFileFilter = new HTMLFileFilter();

        jFileChooser.setFileFilter(htmlFileFilter);
        int choose = jFileChooser.showSaveDialog(view);

        if (choose == JFileChooser.APPROVE_OPTION) {
            currentFile = jFileChooser.getSelectedFile();
            view.setTitle(currentFile.getName());

            HTMLEditorKit htmlEditorKit = new HTMLEditorKit();
            try (FileWriter fileWriter = new FileWriter(currentFile);) {
                htmlEditorKit.write(fileWriter, document, 0, document.getLength());
            } catch (Exception e) {
                ExceptionHandler.log(e);
            }
        }
    }
}
