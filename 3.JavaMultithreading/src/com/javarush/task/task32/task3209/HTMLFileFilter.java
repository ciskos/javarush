package com.javarush.task.task32.task3209;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class HTMLFileFilter extends FileFilter {
    @Override
    public boolean accept(File f) {
        boolean html = f.getName().toLowerCase().endsWith(".html");
        boolean htm = f.getName().toLowerCase().endsWith(".htm");
        boolean isDir = f.isDirectory();

        return isDir || html || htm;
    }

    @Override
    public String getDescription() {
        return "HTML и HTM файлы";
    }
}
