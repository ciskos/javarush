package com.javarush.task.task30.task3008.client;

import com.javarush.task.task30.task3008.ConsoleHelper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class BotClient extends Client {

    public static void main(String[] args) {
        BotClient botClient = new BotClient();
//        botClient.run();

//        SimpleDateFormat sdf = null;
//        Calendar c = Calendar.getInstance();
//        String userName = "user";
//        String answer = null;
//        answer = String.format("Информация для %s: %s" + userName, sdf.format(c.getTime()));

//        sdf = new SimpleDateFormat("d.MM.YYYY");
//        answer = String.format("Информация для %s: %s", userName, sdf.format(c.getTime()));
//        System.out.println(answer);
//        sdf = new SimpleDateFormat("d");
//        answer = String.format("Информация для %s: %s", userName, sdf.format(c.getTime()));
//        System.out.println(answer);
//        sdf = new SimpleDateFormat("MMMM");
//        answer = String.format("Информация для %s: %s", userName, sdf.format(c.getTime()));
//        System.out.println(answer);
//        sdf = new SimpleDateFormat("YYYY");
//        answer = String.format("Информация для %s: %s", userName, sdf.format(c.getTime()));
//        System.out.println(answer);
//        sdf = new SimpleDateFormat("H:mm:ss");
//        answer = String.format("Информация для %s: %s", userName, sdf.format(c.getTime()));
//        System.out.println(answer);
//        sdf = new SimpleDateFormat("H");
//        answer = String.format("Информация для %s: %s", userName, sdf.format(c.getTime()));
//        System.out.println(answer);
//        sdf = new SimpleDateFormat("m");
//        answer = String.format("Информация для %s: %s", userName, sdf.format(c.getTime()));
//        System.out.println(answer);
//        sdf = new SimpleDateFormat("s");
//        answer = String.format("Информация для %s: %s", userName, sdf.format(c.getTime()));
//        System.out.println(answer);



    }

    @Override
    protected SocketThread getSocketThread() {
        return new BotSocketThread();
    }

    @Override
    protected boolean shouldSendTextFromConsole() {
        return false;
    }

    @Override
    protected String getUserName() {
        return "date_bot_" + (int)(Math.random() * 100);
    }

    public class BotSocketThread extends Client.SocketThread {
        @Override
        protected void clientMainLoop() throws IOException, ClassNotFoundException {
            sendTextMessage("Привет чатику. Я бот. Понимаю команды: дата, день, месяц, год, время, час, минуты, секунды.");
            super.clientMainLoop();
        }

        @Override
        protected void processIncomingMessage(String message) {
            ConsoleHelper.writeMessage(message);
            if (message.contains(":")) {
                String[] userNameAndMessage = message.split(":");
                String userName = userNameAndMessage[0].trim();
                String userMessage = userNameAndMessage[1].trim();
                SimpleDateFormat sdf = null;
                Calendar c = Calendar.getInstance();

                switch (userMessage) {
                    case "дата":
                        sdf = new SimpleDateFormat("d.MM.YYYY");
                        break;
                    case "день":
                        sdf = new SimpleDateFormat("d");
                        break;
                    case "месяц":
                        sdf = new SimpleDateFormat("MMMM");
                        break;
                    case "год":
                        sdf = new SimpleDateFormat("YYYY");
                        break;
                    case "время":
                        sdf = new SimpleDateFormat("H:mm:ss");
                        break;
                    case "час":
                        sdf = new SimpleDateFormat("H");
                        break;
                    case "минуты":
                        sdf = new SimpleDateFormat("m");
                        break;
                    case "секунды":
                        sdf = new SimpleDateFormat("s");
                        break;
                }

                if (sdf != null) {
                    String answer = String.format("Информация для %s: %s"
                            , userName
                            , sdf.format(c.getTime()));
                    sendTextMessage(answer);
                }
            }
        }
    }
}
