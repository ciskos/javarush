package com.javarush.task.task30.task3008;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Server {
    private static Map<String, Connection> connectionMap = new ConcurrentHashMap<>();

    public static void main(String[] args) {
        int port = ConsoleHelper.readInt();

        try (
                ServerSocket serverSocket = new ServerSocket(port);
        ) {
            System.out.println("Server started...");

            while (true) {
                new Handler(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void sendBroadcastMessage(Message message) {
        for (Map.Entry<String, Connection> entry : connectionMap.entrySet()) {
            String key = entry.getKey();
            Connection value = entry.getValue();

            try {
                value.send(message);
            } catch (IOException e) {
                ConsoleHelper.writeMessage("Сообщение пользователю " + key + " не отправлено.");
            }
        }
    }

    private static class Handler extends Thread {
        private Socket socket;

        public Handler(Socket socket) {
            this.socket = socket;
        }

        private String serverHandshake(Connection connection) throws IOException, ClassNotFoundException {
            String happyNewChatter = null;
            boolean userAlreadyInBase = false;
            boolean userNameAnswer = false;
            Message handshakeAnswer = null;

            // Тут хотеть от юзверя правильного имени
            while (true){
                connection.send(new Message(MessageType.NAME_REQUEST));
                handshakeAnswer = connection.receive();
                userNameAnswer = handshakeAnswer.getType().equals(MessageType.USER_NAME);

                if (userNameAnswer) {
                    happyNewChatter = handshakeAnswer.getData();
                    userAlreadyInBase = connectionMap.containsKey(happyNewChatter);

                    if (!happyNewChatter.isEmpty() && !userAlreadyInBase) {
                        connectionMap.put(happyNewChatter, connection);
                        connection.send(new Message(MessageType.NAME_ACCEPTED));
                        break;
                    }
                }
            }

            return happyNewChatter;
        }

        private void notifyUsers(Connection connection, String userName) throws IOException {
            for (Map.Entry<String, Connection> entry : connectionMap.entrySet()) {
                String key = entry.getKey();
                Connection value = entry.getValue();

                if (!key.equals(userName)) {
                    connection.send(new Message(MessageType.USER_ADDED, key));
                }
            }
        }

        private void serverMainLoop(Connection connection, String userName) throws IOException, ClassNotFoundException {
            while (true) {
                Message newMessage = connection.receive();

                if (newMessage.getType() == MessageType.TEXT) {
                    String formattedMessage = String.format("%s: %s", userName, newMessage.getData());
                    sendBroadcastMessage(new Message(MessageType.TEXT, formattedMessage));
                } else {
                    ConsoleHelper.writeMessage("Ошибка - не текстовое сообщение.");
                }
            }
        }

        @Override
        public void run() {
            String userName = null;

            SocketAddress socketAddress = socket.getRemoteSocketAddress();
            ConsoleHelper.writeMessage("Установлено новое соединение с удалённым адресом: " + socketAddress);

            try (Connection connection = new Connection(socket);) {
                userName = serverHandshake(connection);
                sendBroadcastMessage(new Message(MessageType.USER_ADDED, userName));
                notifyUsers(connection, userName);
                serverMainLoop(connection, userName);
            } catch (ClassNotFoundException | IOException e) {
                ConsoleHelper.writeMessage("Произошла ошибка при обмене данными с удалённым сервером.");
            }

            if (userName != null) {
                connectionMap.remove(userName);
                sendBroadcastMessage(new Message(MessageType.USER_REMOVED, userName));
            }
            ConsoleHelper.writeMessage("Соединение с удалённым адресом закрыто.");
        }
    }

}
