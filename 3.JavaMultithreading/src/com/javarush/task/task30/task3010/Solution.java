package com.javarush.task.task30.task3010;

/* 
Минимальное допустимое основание системы счисления
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        int minRadix = 37;
        for (int i = 2; i <= 36; i++) {

            try {
                new BigInteger(args[0], i);
                if (minRadix > i) minRadix = i;
            } catch (Exception e) {
            }
        }

        if (minRadix == 37) {
            System.out.println("incorrect");
        } else {
            System.out.println(minRadix);
        }

    }

}