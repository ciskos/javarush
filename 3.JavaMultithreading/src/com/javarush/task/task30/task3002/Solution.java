package com.javarush.task.task30.task3002;

/* 
Осваиваем методы класса Integer
*/
public class Solution {

    public static void main(String[] args) {
        System.out.println(convertToDecimalSystem("0x16")); //22
        System.out.println(convertToDecimalSystem("012"));  //10
        System.out.println(convertToDecimalSystem("0b10")); //2
        System.out.println(convertToDecimalSystem("62"));   //62
    }

    /*
    * 1. В классе Solution должен существовать метод convertToDecimalSystem(String), возвращающий String.
    * 2. Метод convertToDecimalSystem(String) должен иметь модификаторы доступа: public, static.
    * 4. Метод convertToDecimalSystem(String) должен переводить переданную строку в десятичное число и
    * возвращать его в виде строки.
    * */
    public static String convertToDecimalSystem(String s) {
        //напишите тут ваш код
        /*
        * 3. Метод convertToDecimalSystem(String) должен вызывать метод Integer.parseInt(String, int).
        * */

        if (s.startsWith("0x")) return String.valueOf(Integer.parseInt(s.substring(2), 16));
        if (s.startsWith("0b")) return String.valueOf(Integer.parseInt(s.substring(2), 2));
        if (s.startsWith("0")) return String.valueOf(Integer.parseInt(s.substring(1), 8));

        return String.valueOf(Integer.parseInt(s, 10));
    }
}
