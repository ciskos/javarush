package com.javarush.task.task30.task3009;

/* 
Палиндром?
*/

import java.util.HashSet;
import java.util.Set;

public class Solution {
    public static void main(String[] args) {
        System.out.println(getRadix("112"));        //expected output: [3, 27, 13, 15]
        System.out.println(getRadix("123"));        //expected output: [6]
        System.out.println(getRadix("5321"));       //expected output: []
        System.out.println(getRadix("1A"));         //expected output: []
    }

    /*
    * 1. Необходимо объявить приватный статический метод Set<Integer> getRadix(String number).
    * */
    private static Set<Integer> getRadix(String number) {
        Set<Integer> result = new HashSet<>();
        try {
            Integer num = Integer.parseInt(number);
            for (int i = 2; i <= 36; i++) {
                String numOnRadix = Integer.toString(num, i);
                if (new StringBuilder(numOnRadix).reverse().toString().equals(numOnRadix)) {
                    result.add(i);
                }
            }
        } catch (NumberFormatException e) {
            /*
            * 3. Методе getRadix не должен бросать NumberFormatException.
            * */
        }

        /*
        * 2. Метод getRadix в случае некорректных входных данных должен возвращать пустой HashSet.
        * 4. Метод getRadix не должен ничего выводить в консоль.
        * 5. Метод getRadix должен возвращать множество согласно условию задачи.
        * */
        return result;
    }
}