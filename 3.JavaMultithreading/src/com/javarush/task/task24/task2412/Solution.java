package com.javarush.task.task24.task2412;

import java.text.ChoiceFormat;
import java.text.Format;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/* 
Знания - сила!
*/
public class Solution {
    public static void main(String[] args) {
        List<Stock> stocks = getStocks();

//        int i = 0;
//        for (Stock stock : stocks) {
//            String name = ((String) stock.get("name"));
//            String symbol = (String) stock.get("symbol");
//            double open = !stock.containsKey("open") ? 0 : ((double) stock.get("open"));
//            double last = !stock.containsKey("last") ? 0 : ((double) stock.get("last"));
//            double change = !stock.containsKey("change") ? 0 : ((double) stock.get("change"));
//            Date date = (Date) stock.get("date");
//
//            System.out.println(name + " " + symbol + " " + open + " " + last + " " + change + " " + date);
////            System.out.println("i " + ++i + " " + date);
//        }

        sort(stocks);

//        System.out.println("after");
//        for (Stock stock : stocks) {
//            String name = ((String) stock.get("name"));
//            String symbol = (String) stock.get("symbol");
//            double open = !stock.containsKey("open") ? 0 : ((double) stock.get("open"));
//            double last = !stock.containsKey("last") ? 0 : ((double) stock.get("last"));
//            double change = !stock.containsKey("change") ? 0 : ((double) stock.get("change"));
//            Date date = (Date) stock.get("date");
//
//            System.out.println(name + " " + symbol + " " + open + " " + last + " " + change + " " + date);
//        }

        Date actualDate = new Date();
        printStocks(stocks, actualDate);
    }

    public static void printStocks(List<Stock> stocks, Date actualDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        double[] filelimits = {0d, actualDate.getTime()};
        String[] filepart = {"change {4}", "open {2} and last {3}"};

        // limits = {1, 2, 3, 4, 5, 6, 7}
        // formats = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"}
        // ChoiceFormat(limits, formats)
        ChoiceFormat fileform = new ChoiceFormat(filelimits, filepart);
//        Format[] testFormats = {null, dateFormat, fileform};
        // name, symbol, open, last, change, date, date.getTime()
        Format[] testFormats = {null, null, dateFormat, fileform};
        MessageFormat pattform = new MessageFormat("{0}   {1} | {5} {6}");
//        MessageFormat pattform = new MessageFormat("<0|{0}>   <1|{1}> <2|{2}><3|{3}><4|{4}> | <5|{5}> <6|{6}>");
        pattform.setFormats(testFormats);
//        pattform.setFormatByArgumentIndex(5, dateFormat);
//        pattform.setFormatByArgumentIndex(6, fileform);

        for (Stock stock : stocks) {
            // {0}
            String name = ((String) stock.get("name"));
            // {1}
            String symbol = (String) stock.get("symbol");
            // {2}
            double open = !stock.containsKey("open") ? 0 : ((double) stock.get("open"));
            // {3}
            double last = !stock.containsKey("last") ? 0 : ((double) stock.get("last"));
            // {4}
            double change = !stock.containsKey("change") ? 0 : ((double) stock.get("change"));
            // {5}
            Date date = (Date) stock.get("date");
//            Date dateGetTime = (Date) stock.get("date");
            // MessageFormat - Object ... args
//            String pattform = MessageFormat.format(
////            		"<{0}>   <{1}> | <{5}> <{6}>"
//            		"<0|{0}>   <1|{1}> <2|{2}><3|{3}><4|{4}> | <5|{5}> <6|{6}>"
//            		//0		1		2		3	4		5		6
//            		, name, symbol, open, last, change, date, date.getTime());
//            pattform.setFormats(testFormats);
            // 							null	null	dateFormat	fileform
            // "{0}   {1} | {5} {6}" -> name   symbol | date		date.getTime()
//    		//	0	 			              1		  2	      3	     4		5	           6
            // name, 			              symbol, open,   last,  change, date,          date.getTime());
            // "Fake Apple Inc.", 			  "AAPL", 125.64, 123.43
            // Fake Nokia Corporation (ADR)", "NOK",                 .1,     getRandomDate()
            // 6 -> в зависимости от значения date.getTime() выбирается вставка change или open и last
            // Если значение от 0 до actualDate.getTime(), т.е. до текущего момента, то выбирается первое значение
            // массива filepart - change. Если значение выше текущего момента actualDate.getTime(), то
            // выбирается open и last.
            // MessageFormat формируется рекурсивным образом на основе класса с выбором ChoiceFormat, т.е.
            // "{0}   {1} | {5} {6}" на самом деле должен выглядеть так
            // {0}   {1} | {5} {{4} или {2}{3}} - что собственно видно при выводе паттерна в консоль
            //            4              или                         2            3
            // {6,choice, 0.0#change {4}  |  1.563644657393E12#open {2} and last {3}}
            Object[] testArgs = {name, symbol, open, last, change, date, date.getTime()};
//            System.out.println(actualDate.getTime());
//            System.out.println(pattform.toPattern());
            System.out.println(pattform.format(testArgs));
//            System.out.println(pattform);
//            for (Object o : testArgs) {
//                System.out.println(o);
//            }
        }
    }
    public static void sort(List<Stock> list) {
        list.sort(new Comparator<Stock>() {
            public int compare(Stock stock1, Stock stock2) {
                return stock1.compareTo(stock2);
            }
        });
    }

    public static class Stock extends HashMap<String, Object> implements Comparable {
        private String name = null;
        private String symbol = null;
        private double open = 0.0;
        private double last = 0.0;
        private double change = 0.0;
        private Date date = null;

        public Stock(String name, String symbol, double open, double last) {
            this.name = name;
            this.symbol = symbol;
            this.open = open;
            this.last = last;

            put("name", name);
            put("symbol", symbol);
            put("open", open);
            put("last", last);
//            Date d = getRandomDate(2020);
//            GregorianCalendar calendar = new GregorianCalendar(2019, 7, 20);
            this.date = getRandomDate(2020);
//            this.date = calendar.getTime();
//            put("date", getRandomDate(2020));
            put("date", this.date);
//            System.out.println("cale " + d);
//            put("date", calendar.getTime());
        }

        public Stock(String name, String symbol, double change, Date date) {
            this.name = name;
            this.symbol = symbol;
            this.change = change;
            this.date = date;

            put("name", name);
            put("symbol", symbol);
            put("date", date);
//            System.out.println("date " + date);
            put("change", change);
        }

        @Override
        public int compareTo(Object o) {
            Stock entry = (Stock)o;
            int result;
            double profit;
            double profitEntry;

//            if (name != null) {
                result = name.compareTo(entry.name);
                if (result != 0) {
//                    System.out.println("name " + result);
                    return result;
                }
//            }

//            System.out.println("name " + name + "date " + date + " entry.date " + entry.date);
//            if (date != null) {
                result = entry.date.compareTo(date);
                if (result != 0) {
//                    System.out.println("date " + result);
                    return result;
                }
//            }

//            if (open != 0.0 || last != 0.0) {
                profit = last - open;
                profitEntry = entry.last - entry.open;
                result = (int)(profitEntry - profit);
                if (result != 0) return result;
//            }

            return 0;
        }
    }

    public static List<Stock> getStocks() {
        List<Stock> stocks = new ArrayList<>();

        stocks.add(new Stock("Fake Apple Inc.", "AAPL", 125.64, 123.43));
        stocks.add(new Stock("Fake Cisco Systems, Inc.", "CSCO", 25.84, 26.3));
        stocks.add(new Stock("Fake Google Inc.", "GOOG", 516.2, 512.6));
        stocks.add(new Stock("Fake Intel Corporation", "INTC", 21.36, 21.53));
        stocks.add(new Stock("Fake Level 3 Communications, Inc.", "LVLT", 5.55, 5.54));
        stocks.add(new Stock("Fake Microsoft Corporation", "MSFT", 29.56, 29.72));

        stocks.add(new Stock("Fake Nokia Corporation (ADR)", "NOK", .1, getRandomDate()));
        stocks.add(new Stock("Fake Oracle Corporation", "ORCL", .15, getRandomDate()));
        stocks.add(new Stock("Fake Starbucks Corporation", "SBUX", .03, getRandomDate()));
        stocks.add(new Stock("Fake Yahoo! Inc.", "YHOO", .32, getRandomDate()));
        stocks.add(new Stock("Fake Applied Materials, Inc.", "AMAT", .26, getRandomDate()));
        stocks.add(new Stock("Fake Comcast Corporation", "CMCSA", .5, getRandomDate()));
        stocks.add(new Stock("Fake Sirius Satellite", "SIRI", -.03, getRandomDate()));


//        stocks.add(new Stock("Fake Apple Inc.", "AAPL", 125.64, 129.43));
//        stocks.add(new Stock("Fake Cisco Systems, Inc.", "CSCO", 25.84, 29.3));
//        stocks.add(new Stock("Fake Google Inc.", "GOOG", 516.2, 519.6));
//        stocks.add(new Stock("Fake Intel Corporation", "INTC", 21.36, 25.53));
//        stocks.add(new Stock("Fake Level 3 Communications, Inc.", "LVLT", 5.55, 6.54));
//        stocks.add(new Stock("Fake Microsoft Corporation", "MSFT", 29.56, 20.72));
//
//        stocks.add(new Stock("Fake Nokia Corporation (ADR)", "NOK", .1, getRandomDate()));
//        stocks.add(new Stock("Fake Oracle Corporation", "ORCL", .15, getRandomDate()));
//        stocks.add(new Stock("Fake Starbucks Corporation", "SBUX", .03, getRandomDate()));
//        stocks.add(new Stock("Fake Yahoo! Inc.", "YHOO", .32, getRandomDate()));
//        stocks.add(new Stock("Fake Applied Materials, Inc.", "AMAT", .26, getRandomDate()));
//        stocks.add(new Stock("Fake Comcast Corporation", "CMCSA", .5, getRandomDate()));
//        stocks.add(new Stock("Fake Sirius Satellite", "SIRI", -.03, getRandomDate()));

        return stocks;
    }

    public static Date getRandomDate() {
        return getRandomDate(1970);
    }

    public static Date getRandomDate(int startYear) {
        int year = startYear + (int) (Math.random() * 30);
        int month = (int) (Math.random() * 12);
        int day = (int) (Math.random() * 28);
        GregorianCalendar calendar = new GregorianCalendar(year, month, day);
        return calendar.getTime();
    }
}

