package com.javarush.task.task21.task2113;

import java.util.ArrayList;
import java.util.List;

public class Hippodrome {
    private List<Horse> horses;
    public static Hippodrome game;

    public static void main(String[] args) {
        List<Horse> horseList = new ArrayList<>();

        horseList.add(new Horse("Horse1", 3, 0));
        horseList.add(new Horse("Horse2", 3, 0));
        horseList.add(new Horse("Horse3", 3, 0));

        game = new Hippodrome(horseList);
        game.run();
        game.printWinner();
    }

    public Hippodrome(List<Horse> horses) {
        this.horses = horses;
    }

    public Horse getWinner() {
        Horse winner = null;

        double longestDistance = horses.get(0).getDistance();
        for (int i = 1; i < horses.size(); i++) {
            if (longestDistance < horses.get(i).getDistance()) {
                longestDistance = horses.get(i).getDistance();
                winner = horses.get(i);
            }
        }

        return winner;
    }

    public void printWinner() {
        Horse winner = getWinner();
        System.out.println("Winner is " + winner.getName() + "!");
    }

    public void run() {
        for (int i = 1; i <= 100; i++) {
            move();
            print();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void move() {
        for (int i = 0; i < horses.size(); i++) {
            horses.get(i).move();
        }
    }

    public void print() {
        for (int i = 0; i < horses.size(); i++) {
            horses.get(i).print();
        }
        for (int i = 0; i < 10; i++) {
            System.out.println();
        }
    }
    
    public List<Horse> getHorses() {
        return horses;
    }
}
