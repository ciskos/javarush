package com.javarush.task.task29.task2909.human;

import java.util.ArrayList;
import java.util.List;

public class University {
    private List<Student> students = new ArrayList<>();
    private String name;
    private int age;

    public University(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Student> getStudents() {
        return students;
    }

    public  void setStudents(List<Student> students) {
        this.students = students;
    }

    public Student getStudentWithAverageGrade(double averageGrade) {
        for (Student s : students) {
            if (s.getAverageGrade() == averageGrade) return s;
        }
        return null;
    }

    public Student getStudentWithMaxAverageGrade() {
        double maxGrade = 0.0;
        Student student = null;
        for (Student s : students) {
            if (maxGrade < s.getAverageGrade()) {
                maxGrade = s.getAverageGrade();
                student = s;
            }
        }
        return student;
    }

    public Student getStudentWithMinAverageGrade() {
        double minGrade = students.get(0).getAverageGrade();
        Student student = null;
        for (Student s : students) {
            if (minGrade > s.getAverageGrade()) {
                minGrade = s.getAverageGrade();
                student = s;
            }
        }
        return student;
    }

    public void expel(Student student) {
        students.remove(student);
    }
}