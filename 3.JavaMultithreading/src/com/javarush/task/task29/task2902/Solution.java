package com.javarush.task.task29.task2902;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

/* 
Рефакторинг в соответствии с Naming and Code Convention 2
*/
public class Solution {
    public static void main(String[] args) throws IOException, InterruptedException, URISyntaxException {
        // 1. Переименуй переменную Solution типа Solution в соответствии с Naming and Code Convention.
        Solution solution = new Solution();
        // 2. Переименуй переменную name_of_file_to_be_opened_by_notepad типа String в соответствии с Naming and Code Convention.
        String nameOfFileToBeOpenedByNotepad = solution.getAbsolutePathToDefaultTxtFile().toString();
        // 3. Переименуй переменную NOTEPAD типа Process в соответствии с Naming and Code Convention.
        Process notepad = solution.getStartNotepadProcess(nameOfFileToBeOpenedByNotepad);
        notepad.waitFor();
    }

    // 4. Переименуй метод getstartnotepadprocess() в соответствии с Naming and Code Convention.
    // 5. Переименуй параметр FILE_NAME метода принимающего String в соответствии с Naming and Code Convention.
    public Process getStartNotepadProcess(String fileName) throws IOException {
        // 6. Переименуй переменную cmd_array типа String[] в соответствии с Naming and Code Convention.
        String[] cmdArray = new String[]{"notepad.exe", fileName};
        return Runtime.getRuntime().exec(cmdArray);
    }

    // 7. Переименуй метод Getabsolutepathtodefaulttxtfile() в соответствии с Naming and Code Convention.
    public Path getAbsolutePathToDefaultTxtFile() throws URISyntaxException {
        // 8. Переименуй переменную uRi типа URI в соответствии с Naming and Code Convention.
        URI uri = Solution.class.getResource("file.txt").toURI();
        return Paths.get(uri);
    }
}
