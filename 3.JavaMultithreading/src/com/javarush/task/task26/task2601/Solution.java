package com.javarush.task.task26.task2601;

import java.util.Arrays;
import java.util.Comparator;

/*
Почитать в инете про медиану выборки
*/
public class Solution {

    public static void main(String[] args) {
//        Integer[] array = {13, 8, 15, 5, 17};
//        Integer[] array2 = {1, 3, 5, 7};
//        Integer[] array3 = {1, 3, 5, 7, 2, 6};
//        Integer[] array4 = {13, 8, 15, 5, 17, 14, 18};
//
//        System.out.println(Arrays.toString(sort(array)));
//        System.out.println(Arrays.toString(sort(array2)));
//        System.out.println(Arrays.toString(sort(array3)));
//        System.out.println(Arrays.toString(sort(array4)));
    }

    public static Integer[] sort(Integer[] array) {
        //implement logic here
        final Double median;
        int arrayLength = array.length;
        Double[] arrayCopy = new Double[arrayLength];

        for (int i = 0; i < arrayLength; i++) arrayCopy[i] = Double.valueOf(array[i]);
        Arrays.sort(arrayCopy);

/*
         Найти медиану, для чётного и нечётного количества значений в массиве.
         Для нечётного просто берём число находящееся в середине, обязательно после сортировки.
         Для чётного, после сортировки массива, складываем два соседних числа находящихся в
         середине массива и делим на два
*/
        if (arrayLength % 2 == 0) {
            median = (arrayCopy[(arrayLength / 2) - 1] + arrayCopy[arrayLength / 2]) / 2;
        } else {
            median = arrayCopy[arrayLength / 2];
        }

        Comparator<Double> comparator = new Comparator<Double>() {
            @Override
            public int compare(Double o1, Double o2) {
                return (int)(o1 - o2) == 0 ? 0 : (int)(Math.abs(o1 - median) - Math.abs(o2 - median));
            }
        };

        Arrays.sort(arrayCopy, comparator);
        for (int i = 0; i < arrayLength; i++) array[i] = arrayCopy[i].intValue();

        return array;
    }
}
