package com.javarush.task.task26.task2603;

import java.util.*;

/*
Убежденному убеждать других не трудно
*/
public class Solution {
    public static void main(String[] args) {
        Comparator<Soldier> stringComparator = new Comparator<Soldier>() {
            @Override
            public int compare(Soldier o1, Soldier o2) {
                return o1.name.compareTo(o2.name);
            }
        };
        Comparator<Soldier> integerComparator = new Comparator<Soldier>() {
            @Override
            public int compare(Soldier o1, Soldier o2) {
                return o1.height - o2.height;
            }
        };
        Comparator[] comparators = new Comparator[]{integerComparator, stringComparator};
        CustomizedComparator customizedComparator = new CustomizedComparator(comparators);

        List<Soldier> soldiers = new ArrayList<>();
        soldiers.add(new Soldier("A", 170));
        soldiers.add(new Soldier("B", 170));
        soldiers.add(new Soldier("c", 170));
        soldiers.add(new Soldier("Ivanov", 170));
        soldiers.add(new Soldier("Petrov", 180));
        soldiers.add(new Soldier("Sidorov", 175));
        soldiers.add(new Soldier("Ivanov", 172));
        soldiers.add(new Soldier("Petrov", 182));
        soldiers.add(new Soldier("Sidorov", 177));

        Collections.sort(soldiers, customizedComparator);

        for (Soldier soldier : soldiers) {
            System.out.println(soldier.name + " " + soldier.height);
        }
    }

    public static class Soldier {
        private String name;
        private int height;

        public Soldier(String name, int height) {
            this.name = name;
            this.height = height;
        }
    }

    public static class CustomizedComparator<T> implements Comparator<T> {
        private Comparator<T>[] comparators;

        public CustomizedComparator(Comparator<T> ... comparators) {
            this.comparators = comparators;
        }

        @Override
        public int compare(T o1, T o2) {
            int result = 0;
            for (Comparator<T> c : comparators) {
                result = c.compare(o1, o2);
                if (result != 0) break;
            }
            return result;
        }
    }

}
