package com.javarush.task.task23.task2305;

import java.util.Arrays;

/*
Inner
*/
public class Solution {
    public InnerClass[] innerClasses = new InnerClass[2];

    public class InnerClass {
    }

    public static Solution[] getTwoSolutions() {
        Solution[] result = new Solution[]{new Solution(), new Solution()};

        for (Solution s : result) {
            s.innerClasses[0] = new Solution().new InnerClass();
            s.innerClasses[1] = new Solution().new InnerClass();
        }

        return result;
    }

    public static void main(String[] args) {
//        System.out.println(Arrays.deepToString(getTwoSolutions()));
    }
}
