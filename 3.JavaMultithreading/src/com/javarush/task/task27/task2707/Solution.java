package com.javarush.task.task27.task2707;

import java.lang.reflect.Method;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/*
Определяем порядок захвата монитора
*/
public class Solution {
    public void someMethodWithSynchronizedBlocks(Object obj1, Object obj2) {
//        System.out.println(Thread.currentThread().getName() + " перед syn1");
        synchronized (obj1) {
//            System.out.println(Thread.currentThread().getName() + " перед syn2");
            synchronized (obj2) {
//                System.out.println(Thread.currentThread().getName() + " внутри syn2");
                System.out.println(obj1 + " " + obj2);
            }
//            System.out.println(Thread.currentThread().getName() + " после syn2");
        }
//        System.out.println(Thread.currentThread().getName() + " после syn1");
    }

    public static boolean isLockOrderNormal(final Solution solution, final Object o1, final Object o2) throws Exception {
        //do something here

        Thread t1 = new Thread(){
            @Override
            public void run() {
                synchronized (o1) {
                    while (true) {}
                }
            }
        };

        Thread t2 = new Thread(){
            @Override
            public void run() {
                    solution.someMethodWithSynchronizedBlocks(o1, o2);
            }
        };

        Thread t3 = new Thread(){
            @Override
            public void run() {
                synchronized (o2) {
//                    while (true) {}
                }
            }
        };

        t1.start();
        t2.start();
        t3.start();
        Thread.sleep(1000);

//        System.out.println("t1 state " + t1.getState());
//        System.out.println("t2 state " + t2.getState());
//        System.out.println("t3 state " + t3.getState());
//        System.out.println("t3 bool " + (t3.getState() == Thread.State.TERMINATED));

        return t3.getState() == Thread.State.TERMINATED;
    }

    public static void main(String[] args) throws Exception {
        final Solution solution = new Solution();
        final Object o1 = new Object();
        final Object o2 = new Object();

        System.out.println(isLockOrderNormal(solution, o1, o2));
    }
}
