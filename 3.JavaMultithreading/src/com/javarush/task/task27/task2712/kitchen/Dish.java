package com.javarush.task.task27.task2712.kitchen;

import java.util.Arrays;

public enum Dish {
    Fish
    , Steak
    , Soup
    , Juice
    , Water;

    public static String allDishesToString() {
        StringBuilder result = new StringBuilder();
        for (Dish d : values()) {
            result.append(d + ", ");
        }
        return result.delete(result.length() - 2, result.length() - 1).toString();
    }
}
