package com.javarush.task.task27.task2708;

import java.util.Arrays;
import java.util.Set;

/* 
Убираем deadLock используя открытые вызовы
*/
public class Solution {
    public static void main(String[] args) {
        final long deadline = System.currentTimeMillis() + 5000; //waiting for 5 sec

        final RealEstate realEstate = new RealEstate();
        Set<Apartment> allApartments = realEstate.getAllApartments();

        final Apartment[] apartments = allApartments.toArray(new Apartment[allApartments.size()]);

//        for (Apartment a : realEstate.getAllApartments()) {
//            System.out.println("before set apartment location " + a.getLocation());
//        }
//        for (Apartment a : apartments) {
//            System.out.println("before array apartment location " + a.getLocation());
//        }

        for (int i = 0; i < 20; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < 10; i++) {
                        realEstate.revalidate();
                    }
                }
            }, "RealEstateThread" + i).start();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < apartments.length; i++) {
                        apartments[i].revalidate(true);
                    }
                }
            }, "ApartmentThread" + i).start();
        }

        Thread deamonThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (System.currentTimeMillis() < deadline)
                    try {
                        Thread.sleep(2);
                    } catch (InterruptedException ignored) {
                    }
                System.out.println("Deadlock occurred");
            }
        });
        deamonThread.setDaemon(true);
        deamonThread.start();

//        for (Apartment a : realEstate.getAllApartments()) {
//            System.out.println("after set apartment location " + a.getLocation());
//        }
//        for (Apartment a : apartments) {
//            System.out.println("after array apartment location " + a.getLocation());
//        }
    }
}