package com.javarush.task.task22.task2209;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/*
Составить цепочку слов
*/
public class Solution {
    public static void main(String[] args) {
        //...
        List<String> words = new ArrayList<>();
        String[] wordsArray = null;

        // Считывание имени файла из консоли
        try (
                InputStreamReader isr = new InputStreamReader(System.in);
                BufferedReader brisr = new BufferedReader(isr);
                FileReader fr = new FileReader(brisr.readLine());
                BufferedReader brfr = new BufferedReader(fr);
        ) {
            // Считывание содержимого файла в список
            while (brfr.ready()) {
                String[] w = brfr.readLine().split(" ");
                for (String s : w) words.add(s.trim());
            }

            wordsArray = new String[words.size()];
            words.toArray(wordsArray);
        } catch (IOException e) {
            e.printStackTrace();
        }

        StringBuilder result = getLine(wordsArray);
        System.out.println(result.toString());

//        List<String> words2 = new ArrayList<>();
//        words2.add("Киев");
//        words2.add("Нью-Йорк");
//        words2.add("Амстердам");
//        words2.add("Вена");
//        words2.add("Мельбурн");
//        words2.add("Алмаата");
//        words2.add("Москва");
//        words2.add("Питер");
//        words2.add("Вашингтон");
//        words2.add("Гамбург");
//        words2.add("Венеция");
//        words2.add("Ростов-на-Дону");
//        words2.add("Киров ");
//        words2.add("Варшава");
//        words2.add("Артек");
//        words2.add("Уссурийск");
//        words2.add("колобок");
//
//        String[] wordsArray2 = new String[words2.size()];
//        words2.toArray(wordsArray2);
//        StringBuilder result2 = getLine(wordsArray2);
//        System.out.println(result2.toString());

        // Вход - трам Нет труД муд доМ мандарин март Ноорка арка аа
        // Варианты -
        /*
        * трам муд доМ мандарин Нет труД
        * Нет трам муд доМ мандарин Ноорка арка аа
        * труД доМ муд
        * муд доМ мандарин Нет трам март труД
        * доМ муд
        * мандарин Нет трам муд доМ март труД
        * март трам муд доМ мандарин Нет труД
        * Ноорка арка аа
        * арка аа
        * аа арка
        * */
        // Результат - Нет трам муд доМ мандарин Ноорка арка аа
//        List<String> words3 = new ArrayList<>();
//        words3.add("трам");
//        words3.add("Нет");
//        words3.add("труД");
//        words3.add("муд");
//        words3.add("доМ");
//        words3.add("мандарин");
//        words3.add("март");
//        words3.add("Ноорка");
//        words3.add("арка");
//        words3.add("аа");
//
//        String[] wordsArray3 = new String[words3.size()];
//        words3.toArray(wordsArray3);
//        StringBuilder result3 = getLine(wordsArray3);
//        System.out.println(result3.toString());
    }

    public static StringBuilder getLine(String... words) {
        // Если слов нет, то возврат пустого StringBuilder
        if (words.length == 0 || words == null) return new StringBuilder();

        // Копия входящего массива слов
        List<String> wordsCopy = new ArrayList<>(Arrays.asList(words));
        // Массив хранящий последовательности слов
        List<String> wordSequences = new ArrayList<>();
        // Результат
        StringBuilder sb = new StringBuilder();

//        Collections.sort(wordsCopy);

        // ЦИКЛ 1
        // Пока не будет обработан весь список слов
        for (int i = 0; i < wordsCopy.size(); i++) {
            // Если значение не равно нулю, то
            if (wordsCopy.get(i) != null) {
                // Определяем последний символ у обрабатываемого слова
                String lastSymbol = (wordsCopy.get(i).trim().charAt(wordsCopy.get(i).trim().length() - 1) + "").toLowerCase();
                // Добавляем обрабатываемое слово к результату и пробел
                sb.append(wordsCopy.get(i) + " ");
                // Устанавливаем в списке где было слово значение в null,
                // чтобы больше не возвращаться к нему
                wordsCopy.set(i, null);

                // Счётчик слов
                int wordsCounter = 0;
                // ЦИКЛ 2
                // До тех пор, пока счётчик слов меньше количества слов в списке
                while (wordsCounter++ < wordsCopy.size()) {
                    // ЦИКЛ 3
                    // Пока не будет обработан весь список слов
                    for (int j = 0; j < wordsCopy.size(); j++) {
                        // Если значение не равно нулю, то
                        if (wordsCopy.get(j) != null) {
                            // Определяем первый символ у обрабатываемого слова
                            String startSymbol = (wordsCopy.get(j).trim().charAt(0) + "").toLowerCase();

                            // Если первый символ очередного слова в списке равен последнему
                            // символу в предыдущем слове, то
                            if (startSymbol.equals(lastSymbol)) {
                                // К результату добавляется слово и пробел
                                sb.append(wordsCopy.get(j) + " ");
                                // Определяется новый последний символ взятый из добавленного слова
                                lastSymbol = (wordsCopy.get(j).trim().charAt(wordsCopy.get(j).trim().length() - 1) + "").toLowerCase();
                                // Устанавливаем в списке где было слово значение в null,
                                // чтобы больше не возвращаться к нему
                                wordsCopy.set(j, null);
                                // Обнуляем счётчик для повторного прохождения по списку слов, т.к.
                                // возможно слово которое было пропущено ранее, теперь можешь совпасть
                                // первой буквой с последней буквой текущего слова
                                wordsCounter = 0;
                            }
                        }
                    }
                }
            }

            // По завершении работы циклов 2 и 3, для очередного слова цикла 1 собирается цепочка слов
            // добавляемая в список цепочек слов
            wordSequences.add(sb.toString().trim());
            // StringBuilder обнуляется
            sb.setLength(0);
            // Копия списка слов воссоздаётся заново
            wordsCopy = new ArrayList<>(Arrays.asList(words));
//            Collections.sort(wordsCopy);
        }

//        for (String s : wordsCopy) {
//            System.out.println("copy " + s);
//        }

        // Подсчёт слов
        int longestCountWords = 0;
        // Какой-то индекс
        int indexInList = 0;
//        int longest = 0;
        // Цикл перебирающий готовые цепочки слов
        for (int i = 0; i < wordSequences.size(); i++) {
            // Очередная цепочка
            String s = wordSequences.get(i);
            // Разбитие цепочки на отдельные слова
            String[] splitS = s.split(" ");
//            System.out.println("length " + s.length() + " count words " + splitS.length + " sequence " + s);
            // Если длина текущей цепочки слов больше счётчика слов самой большой последовательности слов, то
            if (longestCountWords < splitS.length) {
                // Самой большой последовательности слов присвивается новое значение
                longestCountWords = splitS.length;
                // И запоминается индекс текущей цепочки слов в списке
                indexInList = i;
            }
        }

        // Получение списка слов из самой длинной последовательности слов
        String[] outWords = wordSequences.get(indexInList).split(" ");
        // Создание списка потерянных слов, на основе входного списка слов
        List<String> lostWords = new ArrayList<>(Arrays.asList(words));

        // Пока не будет обработан весь список слов из самой длинной последовательности слов
        for (int i = 0; i < outWords.length; i++) {
            // Для каждого слов из списка потерянных слов...
            for (int j = 0; j < lostWords.size(); j++) {
//                System.out.println(lostWords.get(j) + " " + outWords[i]);
                // Если слово из потерянного списка не равно null и потерянное слово равно слову из
                // самой длинной цепочки слов, то...
                if (lostWords.get(j) != null && lostWords.get(j).trim().equals(outWords[i].trim()))
                    // Установить значение для потерянного слова в null
                    // Таким образом, все слова входящие в цепочку и в список потерянных слов
                    // будут удалены из списка потерянных слов и в списке потерянных слов
                    // останутся только слова не вошедшие в цепочку, или потерянные слова
                    lostWords.set(j, null);
            }
        }

        // В результат добавляется сама длинная цепочка слов, после чего...
        sb.append(wordSequences.get(indexInList) + " ");

        // В результат добавляются все потерянные слова из списка потерянных слов
        // Вне зависимости от совпадения от первых и последних буквы в словах
        // Идиотизм. Для чего такое условие по совпадению первых и последних букв, если в итоге
        // всё равно всё кашей запихиывается в один результат.
//        for (String s : lostWords) {
////            System.out.println("lost " + s);
//            if (s != null) {
//                sb.append(s + " ");
//            }
//        }

//        System.out.println("longest word sequence " + longestCountWords);

//        for (int i = 0; i < wordSequences.size(); i++) {
//            String s = wordSequences.get(i);
//            if (s.length() > longest) {
//                longest = s.length();
//                indexInList = i;
//            }
//        }

        // Удалить последний символ в результате, который является пробелом
        return sb.deleteCharAt(sb.length() - 1);
    }
}
