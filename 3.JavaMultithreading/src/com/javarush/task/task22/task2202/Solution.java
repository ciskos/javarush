package com.javarush.task.task22.task2202;

/* 
Найти подстроку
*/
public class Solution {
    public static void main(String[] args) {
        System.out.println(getPartOfString("JavaRush - лучший сервис обучения Java."));
        System.out.println(getPartOfString("Амиго и Диего лучшие друзья!"));
    }

    public static String getPartOfString(String string) {
        if (string == null) throw new TooShortStringException();

        StringBuilder sb = null;
        int countSpaces = 0;

        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == ' ') {
                countSpaces++;
            }
        }

        if (countSpaces < 4) throw new TooShortStringException();

        try {
            String[] splitString = string.split(" ");
            sb = new StringBuilder();

            countSpaces = 0;
            for (int i = 1; i < splitString.length && countSpaces <= 3; i++, countSpaces++) {
                sb.append(splitString[i] + " ");
            }
        } catch (TooShortStringException e) {
            e.printStackTrace();
        }

        return sb.toString().trim();
    }

    public static class TooShortStringException extends RuntimeException {
    }
}
