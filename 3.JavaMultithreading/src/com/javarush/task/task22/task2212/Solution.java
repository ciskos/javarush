package com.javarush.task.task22.task2212;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
Проверка номера телефона
*/
public class Solution {
    public static void main(String[] args) {
        System.out.println(checkTelNumber("+380501234567") + " +380501234567");
        System.out.println(checkTelNumber("+38(050)1234567") + " +38(050)1234567");
        System.out.println(checkTelNumber("+38050123-45-67") + " +38050123-45-67");
        System.out.println(checkTelNumber("050123-4567") + " 050123-4567");
        System.out.println(checkTelNumber("+38)050(1234567") + " +38)050(1234567");
        System.out.println(checkTelNumber("+38(050)1-23-45-6-7") + " +38(050)1-23-45-6-7");
        System.out.println(checkTelNumber("050ххх4567") + " 050ххх4567");
        System.out.println(checkTelNumber("050123456") + " 050123456");
        System.out.println(checkTelNumber("(0)501234567") + " (0)501234567");
//        checkTelNumber("+380501234567");
//        checkTelNumber("+38(050)1234567");
//        checkTelNumber("+38050123-45-67");
//        checkTelNumber("050123-4567");
//        checkTelNumber("+38)050(1234567");
//        checkTelNumber("+38(050)1-23-45-6-7");
//        checkTelNumber("050ххх4567");
//        checkTelNumber("050123456");
//        checkTelNumber("(0)501234567");

    }

    public static boolean checkTelNumber(String telNumber) {
        if (telNumber == null) return false;

        Pattern p12 = Pattern.compile(
                        "^(?:\\+\\d\\d)?" +
                        "\\(?" +
                            "\\d{3}" +
                        "\\)?" +
                        "\\d{1,3}" +
                        "(?:-?\\d+){0,2}" +
                        "$"
                , Pattern.COMMENTS);
        Pattern p10 = Pattern.compile(
                        "^" +
                        "\\d+" +
                        "(?:-?\\d+){0,2}" +
                        "$"
                , Pattern.COMMENTS);
        Matcher m12 = p12.matcher(telNumber);
        Matcher m10 = p10.matcher(telNumber);
        Matcher mDigits = Pattern.compile("\\d").matcher(telNumber);
        Matcher mDash = Pattern.compile("-").matcher(telNumber);
        int countDigits = 0;
        int countDashes = 0;
        boolean result = false;

        while (mDigits.find()) {
            countDigits++;
        }

        while (mDash.find()) {
            countDashes++;
        }

        if ((countDigits == 12 || countDigits == 10) && countDashes <=2) {
            if (m12.matches() || m10.matches()) result = true;
        }

        return result;
    }
}
