package com.javarush.task.task22.task2208;

import java.util.HashMap;
import java.util.Map;

/* 
Формируем WHERE
*/
public class Solution {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();

        map.put("name", "Ivanov");
        map.put("country", "Ukraine");
        map.put("city", "Kiev");
        map.put("age", null);

//        map.put("name", null);
//        map.put("country", null);
//        map.put("city", null);
//        map.put("age", null);

        System.out.println(getQuery(map));
    }
    public static String getQuery(Map<String, String> params) {
        int countNulls = 0;
        StringBuilder sb = new StringBuilder();
        String format = "%s = '%s'";

        for (Map.Entry<String, String> entry : params.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            if (value != null) {
                sb.append(String.format(format, key, value) + " and ");
            } else {
                countNulls++;
            }
        }

        if (countNulls == params.size()) {
            return new String();
        }

        sb.delete(sb.length() - 5, sb.length());

        return sb.toString();
    }
}
