package com.javarush.task.task22.task2207;

import java.io.*;
import java.util.*;

/* 
Обращенные слова
*/
public class Solution {
    public static List<Pair> result = new LinkedList<>();

    public static void main(String[] args) {
        try (
                InputStreamReader isr = new InputStreamReader(System.in);
                BufferedReader br = new BufferedReader(isr);
                FileReader fr = new FileReader(new File(br.readLine()));
                BufferedReader brfr = new BufferedReader(fr);
        ) {
            ArrayList<String> words = new ArrayList<>();

            while (brfr.ready()) {
                String[] s = brfr.readLine().split(" ");

                for (String w : s) words.add(w.trim());
            }

            for (int i = 0; i < words.size(); i++) {
                String word = words.get(i);
                for (int j = i + 1; j < words.size(); j++) {
                    StringBuilder wordReverse = new StringBuilder(words.get(j)).reverse();
                    if (word.length() == wordReverse.length()
                            && word.equals(wordReverse.toString())) {
                            Pair p = new Pair(word, wordReverse.reverse().toString());

                            if (!result.contains(p)) {
                                result.add(p);
                            }
                    }
                }
            }

            for (Pair pair : result) {
                System.out.println(pair);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class Pair {
        String first;
        String second;

        public Pair() {
        }

        public Pair(String first, String second) {
            this.first = first;
            this.second = second;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pair pair = (Pair) o;

            if (first != null ? !first.equals(pair.first) : pair.first != null) return false;
            return second != null ? second.equals(pair.second) : pair.second == null;

        }

        @Override
        public int hashCode() {
            int result = first != null ? first.hashCode() : 0;
            result = 31 * result + (second != null ? second.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return  first == null && second == null ? "" :
                    first == null ? second :
                            second == null ? first :
                                    first.compareTo(second) < 0 ? first + " " + second : second + " " + first;

        }
    }

}
