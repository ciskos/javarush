package com.javarush.task.task22.task2211;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Arrays;

/* 
Смена кодировки
*/
public class Solution {
    public static void main(String[] args) throws IOException {
//        args = new String[]{"D:/GIT/JavaRushTasks/task2211/in.txt"
//                , "D:/GIT/JavaRushTasks/task2211/out.txt"};

        String fileIn = args[0];
        String fileOut = args[1];
        Charset windows1251 = Charset.forName("Windows-1251");
        Charset utf8 = Charset.forName("UTF-8");
        byte[] buffer = new byte[1024 * 10];

        FileInputStream fis = new FileInputStream(new File(fileIn));
        FileOutputStream fos = new FileOutputStream(new File(fileOut));
        while (fis.available() > 0) {
            fis.read(buffer);
            String s = new String(buffer, windows1251);
            buffer = s.getBytes(utf8);
            fos.write(buffer);
        }

        fos.close();
        fis.close();
    }
}
